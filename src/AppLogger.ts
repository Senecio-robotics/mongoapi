import { Logger } from '@nestjs/common';
import { PathLike, readFileSync, writeFileSync } from 'fs';

export class AppLogger extends Logger {
    private readonly log_folder_path : PathLike;
    private log_file_version : number;

    private log_file_path = () =>  this.log_folder_path + `/log.${this.log_file_version}.json`

    constructor(log_folder_path : PathLike) {
        super();
        this.log_folder_path = log_folder_path;
        this.log_file_version = 0;
    };

    error(message: any, stack?: string, context?: string) {
        this.add_log_to_file({
            type: 'error',
            timestamp: new Date().toISOString(),
            message: message,
            stack: stack,
            context: context
        });
        super.error(message, stack, context);
    };
    log(message: any, context?: string) {
        this.add_log_to_file({
            type: 'log',
            timestamp: new Date().toISOString(),
            message: message,
            context: context
        });
        super.log(message, context);
    };
    warn(message: any, context?: string){
        this.add_log_to_file({
            type: 'warn',
            timestamp: new Date().toISOString(),
            message: message,
            context: context
        });
        super.warn(message, context);
    };
    debug(message: any, context?: string){
        this.add_log_to_file({
            type: 'debug',
            timestamp: new Date().toISOString(),
            message: message,
            context: context
        });
        super.debug(message, context);
    };
    verbose(message: any, context?: string){
        this.add_log_to_file({
            type: 'verbose',
            timestamp: new Date().toISOString(),
            message: message,
            context: context
        });
        super.verbose(message, context);
    };


    add_log_to_file(log: {type: string, timestamp: String, message: String, context: String, stack?: String}) {
        let parsedData;
        try {
            const data = readFileSync(this.log_file_path());
            parsedData = JSON.parse(data.toString());
        } catch (error) {
            this.log_file_version++;
            console.log(`Failed to load old logs, generating new file: ${this.log_file_version}`)
            parsedData = [];
        }
        parsedData.push(log);
        writeFileSync(this.log_file_path(), JSON.stringify(parsedData, null, 2));
    }
}