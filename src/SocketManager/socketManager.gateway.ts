import {
    WebSocketGateway,
    WebSocketServer,
} from '@nestjs/websockets';
import { Server } from 'socket.io';

const config = require('config');
const cors_origin = config.get('cors_origin');

@WebSocketGateway({
    cors: {
        origin: cors_origin,
    },
})

export class SocketGateway {
    @WebSocketServer()
    server: Server;

    companyUserMap: Map<string, string[]> = new Map();
    socketMap: Map<String, String> = new Map();
    usersSocketMap: Map<String, String> = new Map();

    // add user's socket id to the connected users list (socketMap)
    registerSocket(socket_id, company_id, username) {
        if (this.socketMap[company_id] === undefined) {
            this.socketMap[company_id] = [];
        }
        if (!this.socketMap[company_id].includes(socket_id)) {
            this.usersSocketMap.set(username, socket_id);
            this.socketMap[company_id].push(socket_id);
            console.log(`Registered socket ${socket_id} to company ${company_id}`)
            console.log(this.socketMap);
        }
        if (this.socketMap[company_id].length > 0) {
            const collectionsIds = Object.keys(this.companyUserMap).filter((key) => this.companyUserMap[key].length > 0);
            this.emitCompany(company_id, "userInWork", { ids: collectionsIds });
        }
    }

    registerUserToCollection(company_id, socket, collection_id, userName) {
        console.log(collection_id)
        if (collection_id !== '' && this.companyUserMap[collection_id] === undefined) {
            this.companyUserMap[collection_id] = [];
        }
        if (collection_id !== '' && !this.companyUserMap[collection_id].includes({ userName, socket })) {
            this.companyUserMap[collection_id].push({ userName, socket });
        }
        if (this.companyUserMap[collection_id].length > 1) {
            const userNames = this.companyUserMap[collection_id].map(user => user.userName);
            this.emitUsers(collection_id, "newUser", { userNames: userNames });

        }
        if (this.socketMap[company_id].length > 0) {
            const collectionsIds = Object.keys(this.companyUserMap).filter((key) => this.companyUserMap[key].length > 0);
            this.emitCompany(company_id, "userInWork", { ids: collectionsIds });
        }
    }

    removeUserFromCollection(company_id, socket, collection_id, userName) {
        if (this.companyUserMap[collection_id]) {
            this.companyUserMap[collection_id] = this.companyUserMap[collection_id].filter(item => !(item.userName === userName && item.socket === socket));
        }
        if (this.socketMap[company_id].length > 0) {
            const collectionsIds = Object.keys(this.companyUserMap).filter((key) => this.companyUserMap[key].length > 0);
            this.emitCompany(company_id, "userInWork", { ids: collectionsIds });
        }
        if (this.companyUserMap[collection_id].length <= 1) {
            this.emitUsers(collection_id, "noUsers", { userNames: [] });
        }
        else {
            const userNames = this.companyUserMap[collection_id].map(user => user.userName);
            this.emitUsers(collection_id, "newUser", { userNames: userNames });
        }
    }

    // remove user's socket id from the socketMap
    removeSocket(socket_id, company_id) {
        if (this.socketMap[company_id].includes(socket_id)) {
            this.socketMap[company_id].pop(socket_id);
        }
        console.log(`Removed socket ${socket_id} from company ${company_id}`)
        if (this.socketMap[company_id].length > 0) {
            const collectionsIds = Object.keys(this.companyUserMap).filter((key) => this.companyUserMap[key].length > 0);
            this.emitCompany(company_id, "userInWork", { ids: collectionsIds });
        }
    }

    updateSocketId(company_id, oldId, newId) {
        // Update socketMap
        if (this.socketMap[company_id]) {
            const index = this.socketMap[company_id].indexOf(oldId);
            if (index !== -1) {
                this.socketMap[company_id][index] = newId;
            }
        }

        //update userSocketMap
        this.usersSocketMap.forEach((value, key) => {
            if (value === oldId) {
                this.usersSocketMap.set(key, newId);
            }
        });

        // Update companyUserMap
        Object.keys(this.companyUserMap).forEach(collection_id => {
            this.companyUserMap[collection_id] = this.companyUserMap[collection_id].map(user => {
                if (user.socket === oldId) {
                    user.socket = newId;
                }
                return user;
            });
        });
    }

    // send notification to all the users
    emit(type, val) {
        this.server.emit(type, val);
    }

    // send notification to all the users of the company
    emitCompany(company_id, type, val) {
        if (this.socketMap[company_id] !== undefined) {
            this.server.to(this.socketMap[company_id]).emit(type, val);
        }
    }

    emitUsers(collection_id, type, val) {
        if (this.companyUserMap[collection_id] !== undefined) {
            const sockets = this.companyUserMap[collection_id].map(user => user.socket);
            this.server.to(sockets).emit(type, val);
        }
    }

    emitNewMessage(data) {
        console.log(data)
        const companyId = data["company_id"].toString();
        if (this.socketMap[companyId] !== undefined && this.usersSocketMap.get(data['userName'])) {
            this.server.to(this.usersSocketMap.get(data['userName']).toString()).emit("rule", data['message']);
        }
    }

    emitNewRule(data) {
        const companyId = data["company_id"].toString();
        if (this.socketMap[companyId] !== undefined && this.usersSocketMap.get(data['userName'])) {
            console.log(data)
            this.server.to(this.usersSocketMap.get(data['userName']).toString()).emit("newRule", data['rule']);
        }
    }

}