import { Module } from '@nestjs/common';
import { SocketGateway } from './socketManager.gateway';

@Module({
  providers: [SocketGateway],
  exports: [SocketGateway],
})

export class SocketModule {}