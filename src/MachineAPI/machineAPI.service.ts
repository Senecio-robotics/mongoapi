import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Insect, InsectDocument } from "../Schemas/insect.schema";
import { InsectUsage, InsectUsageDocument } from "../Schemas/insectUsage.schema";
import { Collection, CollectionDocument } from '../Schemas/collection.schema';
import { Company, CompanyDocument } from "../Schemas/company.schema";
import { Kind, KindDocument } from "../Schemas/kind.schema";
import { Site, SiteDocument } from '../Schemas/site.schema';
import { Users, UsersDocument } from "../Schemas/users.schema";
import { OriginImage, OriginImageDocument } from "../Schemas/originImage.schema";
import { FailureCompany, FailureCompanyDocument } from '../Schemas/failureCompany.schema';
import { Failure, FailureDocument } from '../Schemas/failure.schema';
import { TrapEvent, TrapEventDocument } from '../Schemas/trapEvent.schema';

@Injectable()
export class MachineAPIService {
  constructor(

    // allows actions on the collections table
    @InjectModel(Collection.name)
    private readonly collectionModel: Model<CollectionDocument>,

    // allows actions on the company table
    @InjectModel(Company.name)
    private readonly companyModel: Model<CompanyDocument>,

    // allows actions on the insect table
    @InjectModel(Insect.name)
    private readonly insectModel: Model<InsectDocument>,

    // allows actions on the insect usage table
    @InjectModel(InsectUsage.name)
    private readonly insectUsageModel: Model<InsectUsageDocument>,

    // allows actions on the sites table
    @InjectModel(Site.name)
    private readonly siteModel: Model<SiteDocument>,

    // allows actions on the kinds table
    @InjectModel(Kind.name)
    private readonly kindModel: Model<KindDocument>,

    // allows actions on the users table
    @InjectModel(Users.name)
    private readonly usersModel: Model<UsersDocument>,

    // allows actions on the failure company table
    @InjectModel(FailureCompany.name)
    private readonly failureCompanyModel: Model<FailureCompanyDocument>,

    // allows actions on the failure table
    @InjectModel(Failure.name)
    private readonly failureModel: Model<FailureDocument>,

    // allows actions on the originImage table
    @InjectModel(OriginImage.name)
    private readonly originImageModel: Model<OriginImageDocument>,

    // allows actions on the trapEvent table
    @InjectModel(TrapEvent.name)
    private readonly trapEventModel: Model<TrapEventDocument>,
  ) { }

  async getTrapEventInfo(trapEvent_id: String) {
    const trapEvent = await this.trapEventModel.findById(trapEvent_id).exec();

    if (trapEvent === null) {
      return null;
    }

    const collection_obj = await this.collectionModel.findById(trapEvent.collection_id).exec();
    let pickup_date = null;
    let collector = null;
    let trap_failures = null;
    const insectsOfTrapNum = await this.insectModel.countDocuments({ trapEvent_id: trapEvent_id });
    if (insectsOfTrapNum > 0) {
      pickup_date = collection_obj.pickup_date;
      collector = collection_obj.collector_username;
      trap_failures = trapEvent.failures;
    }
    const site_id = collection_obj.site_id;
    const site_obj = await this.siteModel.findById(site_id).exec();
    const company_obj = await this.companyModel.findById(collection_obj.company_id).exec();
    const users = await this.usersModel.find({ company_id: company_obj._id }, { username: 1, _id: 1 }).exec();
    const failures_ids = (await this.failureCompanyModel.find({ company_id: company_obj._id }).exec()).map((f) => f.failure_id);
    const failures = (await this.failureModel.find({ _id: { $in: failures_ids } }).exec()).map((f) => f.failure);
    let pooled_species;
    if(collection_obj.company_id==='65c89d2345878bbb080bd6b1' ||collection_obj.company_id==='65c89b6f45878bbb080bd6b0' || collection_obj.company_id==='65ce34d59a65f4db6b4cf9a5'){
      pooled_species = ['Culex salinarius','Culex quinquefasciatus','Culex nigripalpus','Culex erraticus','Aedes vexans','Aedes albopictus']
    }
    else{
      pooled_species= (await this.kindModel.find({ _id: { $in: company_obj.kinds_ids } }).exec()).map((k) => k.genus + " " + k.species);
    }
    pooled_species = [...new Set(pooled_species)];
    let training_full = false;

    // check if training and full collection
    let trapsEvents = (await this.trapEventModel.find({ collection_id: trapEvent.collection_id }, { _id: 1 }).exec()).map(t => t._id);
    let insects_amount = await this.insectModel.countDocuments({ trapEvent_id: { $in: trapsEvents } }).exec();
    if (collection_obj.purpose === "training" && insects_amount >= 10000) {
      training_full = true;
    }

    let bucket_name = company_obj.name;

    let info = { bucket_name: bucket_name, site_id: site_id, site_name: site_obj.name, longitude: site_obj.longitude, latitude: site_obj.latitude, placement_date: collection_obj.placement_date, pickup_date: pickup_date, collector: collector, trap_failures: trap_failures, users: users.filter(u => u.username !== 'shoval'), failures: failures, pooled_species: pooled_species, training_full: training_full };
    return info;
  }

  async updateCollectionInfo(params: any) {
    const collection_obj = await this.collectionModel.findById(params.collection_id);
    collection_obj.pickup_date = params.pickup_date;
    collection_obj.collector_username = params.collector;
    collection_obj.save();
  }
}