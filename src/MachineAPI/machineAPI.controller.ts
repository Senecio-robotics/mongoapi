import { Controller, Get, Query, Post } from '@nestjs/common';
import { MachineAPIService } from './machineAPI.service';

@Controller('machineapi')
export class MachineAPIController {
  constructor(private readonly machineAPIService: MachineAPIService) { }

  // return info according to trapEvent barcode 
  // params: {trapEvent_id}
  @Get('getTrapEventInfo')
  async getTrapEventInfo(
    @Query('trapEvent_id') trapEvent_id: String){
      return this.machineAPIService.getTrapEventInfo(trapEvent_id);
  }

  // update picked collection info
  // params: {collection_id, pickup_date, collector, failures}
  @Post('updateCollectionInfo')
  async updateCollectionInfo(
    @Query('params') params: any){
      return this.machineAPIService.updateCollectionInfo(params);
  }
}