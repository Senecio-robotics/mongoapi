import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MachineAPIController } from './machineAPI.controller';
import { MachineAPIService } from './machineAPI.service';
import { Collection, CollectionSchema } from '../Schemas/collection.schema';
import { Company, CompanySchema } from '../Schemas/company.schema';
import { Insect, InsectSchema } from '../Schemas/insect.schema';
import { InsectUsage, InsectUsageSchema } from '../Schemas/insectUsage.schema';
import { Site, SiteSchema } from '../Schemas/site.schema';
import { Kind, KindSchema } from '../Schemas/kind.schema';
import { Users, UsersSchema } from '../Schemas/users.schema';
import { OriginImage, OriginImageSchema } from '../Schemas/originImage.schema';
import { FailureCompany, FailureCompanySchema } from '../Schemas/failureCompany.schema';
import { Failure, FailureSchema } from '../Schemas/failure.schema';
import { TrapEvent, TrapEventSchema } from '../Schemas/trapEvent.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Collection.name, schema: CollectionSchema },
    ]),
    MongooseModule.forFeature([
      { name: Company.name, schema: CompanySchema },
    ]),
    MongooseModule.forFeature([
      { name: Insect.name, schema: InsectSchema },
    ]),
    MongooseModule.forFeature([
      { name: InsectUsage.name, schema: InsectUsageSchema },
    ]),
    MongooseModule.forFeature([
      { name: Site.name, schema: SiteSchema },
    ]),
    MongooseModule.forFeature([
      { name: Kind.name, schema: KindSchema },
    ]),
    MongooseModule.forFeature([
      { name: Users.name, schema: UsersSchema },
    ]),
    MongooseModule.forFeature([
      { name: FailureCompany.name, schema: FailureCompanySchema },
    ]),
    MongooseModule.forFeature([
      { name: Failure.name, schema: FailureSchema },
    ]),
    MongooseModule.forFeature([
      { name: OriginImage.name, schema: OriginImageSchema },
    ]),
    MongooseModule.forFeature([
      { name: TrapEvent.name, schema: TrapEventSchema },
    ])
  ],
  controllers: [MachineAPIController],
  providers: [MachineAPIService],
})
export class MachineAPIModule { }