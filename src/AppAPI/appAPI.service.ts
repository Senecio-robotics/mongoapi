import { BadRequestException, Catch, Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import ShortUniqueId from 'short-unique-id';
import { Insect, InsectDocument } from '../Schemas/insect.schema';
import { InsectUsage, InsectUsageDocument } from '../Schemas/insectUsage.schema';
import { Collection, CollectionDocument, CollectionState } from '../Schemas/collection.schema';
import { Company, CompanyDocument } from '../Schemas/company.schema';
import { Kind, KindDocument, KindSchema } from '../Schemas/kind.schema';
import { Identification } from '../Schemas/identification.schema';
import { Rule } from '../Schemas/rule.schema'
import { Site, SiteDocument } from '../Schemas/site.schema';
import { Users, UsersDocument } from '../Schemas/users.schema';
import { UserIdentifiedCollection, UserIdentifiedCollectionDocument } from '../Schemas/userIdentifiedCollection.schema';
import { OriginImage, OriginImageDocument } from '../Schemas/originImage.schema';
import { GuidePictures, GuidePicturesDocument } from '../Schemas/guidepictures.schema';
import { NotValid, NotValidDocument } from '../Schemas/notValid.schema';
import { Run } from '../Schemas/run.schema';
import { Test } from '../Schemas/test.schema';
import { TrapEvent, TrapEventDocument } from '../Schemas/trapEvent.schema';
import { Trap, TrapDocument } from '../Schemas/trap.schema';
import { TrapType, TrapTypeDocument } from '../Schemas/trapType.schema';
import { TypeToTrap, TypeToTrapDocument } from '../Schemas/typeToTrap.schema';
import { LureToTrap, LureToTrapDocument } from '../Schemas/lureToTrap.schema';
import { Lure, LureDocument } from '../Schemas/lure.schema';
import { FilterParamsDto } from '../dto/filterParamsDto.dto';
import { CreateIdentificationDto } from '../dto/createIdentification.dto';
import { GetInsectDto } from '../dto/getInsect.dto';
import { SocketGateway } from '../SocketManager/socketManager.gateway';
import { customAlphabet } from 'nanoid';
import { Landmarks } from '../Schemas/landmarks.schema';
import { SpreadingAreas } from '../Schemas/spreadingAreas.schema';
import { userSpreads } from '../Schemas/userSpreads.schema';
import { Spreads } from 'src/Schemas/spreads.schema';
import { KMEANS } from 'density-clustering';
import { MailerService } from '@nestjs-modules/mailer';
// const axios = require('axios');

@Injectable()
export class AppAPIService {
  constructor(

    // allows actions on the collections table
    @InjectModel(Collection.name)
    private readonly collectionModel: Model<CollectionDocument>,

    // allows actions on the company table
    @InjectModel(Company.name)
    private readonly companyModel: Model<CompanyDocument>,

    // allows actions on the insect table
    @InjectModel(Insect.name)
    private readonly insectModel: Model<InsectDocument>,

    // allows actions on the insectUsage table
    @InjectModel(InsectUsage.name)
    private readonly insectUsageModel: Model<InsectUsageDocument>,

    // allows actions on the sites table
    @InjectModel(Site.name)
    private readonly siteModel: Model<SiteDocument>,

    // allows actions on the kinds table
    @InjectModel(Kind.name)
    private readonly kindModel: Model<KindDocument>,

    // allows actions on the users table
    @InjectModel(Users.name)
    private readonly usersModel: Model<UsersDocument>,

    // allows actions on the originImage table
    @InjectModel(OriginImage.name)
    private readonly originImageModel: Model<OriginImageDocument>,

    // allows actions on the usersIdentifiedCollection table
    @InjectModel(UserIdentifiedCollection.name)
    private readonly userIdentifiedCollectionModel: Model<UserIdentifiedCollectionDocument>,

    // allows actions on the notValid table
    @InjectModel(NotValid.name)
    private readonly notValidModel: Model<NotValidDocument>,

    // allows actions on the trapEvent table
    @InjectModel(TrapEvent.name)
    private readonly trapEventModel: Model<TrapEventDocument>,

    // allows actions on the typeToTrap table
    @InjectModel(TypeToTrap.name)
    private readonly typeToTrapModel: Model<TypeToTrapDocument>,

    // allows actions on the trap table
    @InjectModel(Trap.name)
    private readonly trapModel: Model<TrapDocument>,

    // allows actions on the trap type table
    @InjectModel(TrapType.name)
    private readonly trapTypeModel: Model<TrapTypeDocument>,

    // allows actions on the lure table
    @InjectModel(Lure.name)
    private readonly lureModel: Model<LureDocument>,

    // allows actions on the lureToTrap table
    @InjectModel(LureToTrap.name)
    private readonly lureToTrapModel: Model<LureToTrapDocument>,

    // allows actions on the guidePictures table
    @InjectModel(GuidePictures.name)
    private readonly guidePicturesModel: Model<GuidePicturesDocument>,

    private readonly socketManager: SocketGateway,
    private readonly mailerService: MailerService
  ) { }

  yoloCompanys = ['646cae3c89d8257530a11eda', '62a073'];
  customAlphabetChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
  generateRandomID = customAlphabet(this.customAlphabetChars, 6);

  async getCompanyNameAndUsername(company_id: string, username: string) {
    const company = await this.companyModel.findById(company_id).exec();
    return { companyName: company.name, username: username };
  }

  async expandCollection(collection, kind_ids_identified) {
    const traps = (await this.trapEventModel.find({ collection_id: collection._id }, { _id: 1 }).exec()).map(t => t._id);
    let count_identified_p = await this.insectModel.countDocuments({ trapEvent_id: { $in: traps }, $and: [{ $expr: { $in: [{ $arrayElemAt: ["$identifications.kind_id", -1] }, kind_ids_identified] } }, { $expr: { $eq: [{ $arrayElemAt: ["$identifications.second_opinion", -1] }, false] } }] }).exec();
    const not_valid = await this.insectModel.countDocuments({ $and: [{ trapEvent_id: { $in: traps } }, { $expr: { $eq: [{ $arrayElemAt: ["$identifications.kind_id", -1] }, null] } }, { $expr: { $eq: [{ $arrayElemAt: ["$identifications.second_opinion", -1] }, false] } }] }).exec();
    count_identified_p += not_valid;
    const count_secondOpinion_p = this.insectModel.countDocuments({ trapEvent_id: { $in: traps }, identifications: { $not: { $size: 0 } }, $expr: { $eq: [{ $arrayElemAt: ["$identifications.second_opinion", -1] }, true] } }).exec();
    const collection_site_p = this.siteModel.findById(collection.site_id).exec();
    const collection_verified_p = this.insectModel.countDocuments({ trapEvent_id: { $in: traps }, verifier: { $exists: false } }).exec()
    const collection_insects_number_p = this.insectModel.countDocuments({ trapEvent_id: { $in: traps } }).exec();
    const collection_trapEvents = await this.trapEventModel.find({ collection_id: collection._id }).exec();
    const collection_kind_ids = collection_trapEvents.reduce((arr, te) => arr.concat(te.kindsCounter.map(kc => kc.kind_id)), []);
    const collection_kind_p = this.kindModel.find({ _id: { $in: collection_kind_ids } });

    const [count_identified, count_secondOpinion, collection_site, collection_verified, collection_insects_number, collection_kind] =
      await Promise.all([count_identified_p, count_secondOpinion_p, collection_site_p, collection_verified_p, collection_insects_number_p, collection_kind_p])

    const collection_genuses = collection_kind.map((kind) => kind.genus);
    const collection_species = collection_kind.map((kind) => kind.species);
    const collection_virus = [];
    collection_trapEvents.map(te => {
      for (let i = 0; i < te.vials.length; i++) {
        for (let j = 0; j < te.vials[i].tests.length; j++) {
          if (te.vials[i].tests[j].result === true) {
            collection_virus.push(te.vials[i].tests[j].virus_test);
          }
        }
      }
    });
    return {
      total: collection_insects_number,
      percentage: collection_insects_number > 0 ? 100 * count_identified / collection_insects_number : 0,
      second_opinion_percentage: collection_insects_number > 0 ? 100 * (count_secondOpinion) / collection_insects_number : 0,
      site_name: collection_site ? collection_site.name : null,
      site_id: collection_site ? collection_site.id : null,
      mapVision_id: collection_site ? (collection_site.mapVision_id ? collection_site.mapVision_id : null) : null,
      calServ_id: collection_site ? (collection_site.calServ_id ? collection_site.calServ_id : null) : null,
      site_lng: collection_site ? collection_site.longitude : 0,
      site_lat: collection_site ? collection_site.latitude : 0,
      containsCollection: collection_site ? collection_site.containCollections : true,
      genuses: collection_genuses,
      species: collection_species,
      viruses: collection_virus,
      second_opinion: count_secondOpinion > 0 ? true : false,
      is_verified: collection_insects_number > 0 && (collection_verified) === 0 ? 'Yes' : 'No'
    };
  }

  // returns total number of collections
  async getNumberOfCompanyCollections(company_id: string) {
    const numOfCollections = await this.collectionModel.countDocuments({ company_id: company_id, state: { $in: [0, 1, 2] } }).exec();
    return numOfCollections;
  }

  // returns the compamy collection with site info, insects ids and if there is second opinion on this collection
  async getCompanyCollections(company_id: string, page: number, pageSize: number): Promise<any[]> {
    const skip = (page - 1) * pageSize;
    const companyCollections = [];
    const runs_of_company = (await this.companyModel.findById({ _id: company_id }, { _id: 0, runs: 1 }).exec()).runs;
    let res: any = {};
    const collections = await this.collectionModel.find({ company_id: company_id }).skip(skip).limit(pageSize).exec();
    const kind_ids_identified = (await this.kindModel.find({ $or: [{ $and: [{ sex: { $ne: "Unidentified" } }, { species: { $ne: "Unidentified" } }] }, { genus: "Non mosquito" }] }, { _id: 1 }).exec()).map(kind => kind._id);
    const expandedCollections = await Promise.all(collections.map(collection => this.expandCollection(collection, kind_ids_identified)));
    for (let index = 0; index < collections.length; index++) {
      companyCollections.push(Object.assign(expandedCollections[index], (Object.assign({}, collections[index]) as any)._doc))
    }
    res.company_id = company_id;
    res.runs = runs_of_company;
    res.companyCollections = companyCollections;
    return res;
  }

  async getCompanyTrapsTypes(company_id: string) {
    const types_ids = (await this.typeToTrapModel.find({ company_id: company_id }, { trap_type_id: 1 }).exec()).map(t => t.trap_type_id);
    const types = (await this.trapTypeModel.find({ _id: { $in: types_ids } }).exec()).map(t => t.name);
    return types;
  }

  async getCollectionVirus(collection_id: string) {
    const trapEvents = await this.trapEventModel.find({ $and: [{ collection_id: collection_id }, { "vials.tests.result": true }] }, { _id: 0, "vials.tests.result": 1, "vials.tests.virus_test": 1 }).exec();
    let virusList = new Map();
    if (trapEvents.length > 0) {
      trapEvents.map(te => {
        te.vials.map(vial => {
          vial.tests.map(test => {
            if (test.result === true) {
              virusList.set(test.virus_test, 1);
            }
          })
        })
      })
      return (Array.from(virusList.keys()).toString());
    }
    else return null;
  }

  async getSiteViruses(site_id: string) {
    let coll_ids = [];
    let collection_ids = (await this.collectionModel.find({ site_id: site_id }, { _id: 1 }).exec()).map(col => col._id);
    const trapEvents = await this.trapEventModel.find({ $and: [{ collection_id: { $in: collection_ids } }, { "vials.tests.result": true }] }).exec();
    for (let i = 0; i < trapEvents.length; i++) {
      coll_ids.push(trapEvents[i].collection_id);
    }
    return coll_ids;
  }

  async getCompanyCollectionsIds(company_id: string) {
    return (await this.collectionModel.find({ company_id: company_id }, { _id: 1 }).exec()).map(coll => coll._id);
  }

  async getCollectionEditableInfo(collection_id) {
    const collection = await this.collectionModel.findById(collection_id).exec();
    let data = {
      collector: collection.collector_username,
      pickup_date: collection.pickup_date ? collection.pickup_date : '',
      placement_date: collection.placement_date ? collection.placement_date : '',
      site: collection.site_id,
      purpose: collection.purpose ? collection.purpose : '',
    }
    return data;
  }

  async SaveCollectionChanges(collection_id, site_id, collector, placement_date, pickup_date, purpose) {
    const collection = await this.collectionModel.findById(collection_id).exec();
    if (site_id !== null) {
      collection.site_id = site_id;
    }
    if (collector !== null) {
      collection.collector_username = collector;
    }
    if (placement_date !== null) {
      collection.placement_date = placement_date;
    }
    if (pickup_date !== null) {
      collection.pickup_date = pickup_date;
    }
    if (purpose !== null) {
      collection.purpose = purpose;
    }
    collection.save();
  }

  async getCompanySites(company_id: string): Promise<any[]> {
    return await this.siteModel.find({ company_id: company_id }, { _id: 1, mapVision_id: 1, company_id: 1, name: 1, longitude: 1, latitude: 1 }).exec();
  }

  async getCompanyEmptySites(company_id: string): Promise<any[]> {
    return await this.siteModel.find({ company_id: company_id, containCollections: false }).exec();
  }

  async createSite(company_id: string, name: string, long: number, lat: number, address: string, directions: string, mapVision_id: string) {
    let site = new this.siteModel();
    let site_id = this.generateRandomID();
    let exist = await this.siteModel.findOne({ _id: site_id }).exec();
    while (exist) {
      site_id = this.generateRandomID();
      exist = await this.siteModel.findOne({ _id: site_id }).exec();
    }
    site._id = site_id;
    site.company_id = company_id;
    site.name = name;
    site.longitude = long;
    site.latitude = lat;
    site.address = address;
    site.directions = directions;
    site.containCollections = false;
    mapVision_id !== null ? site.mapVision_id = mapVision_id : '';
    await site.save();
    return site._id;
  }

  async editSite(site_id: string, new_name: string, new_address: string, new_directions: string) {
    const site = await this.siteModel.findById(site_id).exec();
    site.name = new_name;
    site.address = new_address;
    site.directions = new_directions;
    await site.save();
  }

  async getCompanySitesIds(company_id: string): Promise<any[]> {
    return (await this.siteModel.find({ company_id: company_id }, { _id: 1 }).exec()).map(s => s._id);
  }

  async getSiteEditableInfo(site_id: string) {
    const site = await this.siteModel.findById(site_id).exec();
    return { name: site.name, long: site.longitude, lat: site.latitude, address: site.address, directions: site.directions };
  }

  async getCollection(id: string) {
    return await this.collectionModel.findById(id).exec();
  }

  async getRun(company_id, site_id) {
    const company = await this.companyModel.findById(company_id).exec();
    const runs = company.runs;
    for (let i = 0; i < runs.length; i++) {
      if (runs[i].sites_ids.includes(site_id)) {
        return runs[i].name;
      }
    }
    return "Isn't connected to run";
  }

  async putRun(company_id, { old_name, old_type, new_name, sites }) {
    let company = await this.companyModel.findById(company_id).exec();
    if ((old_name === new_name) && company.runs.find(run => run.name === new_name) !== null) {
      return "alreadyExists";
    }
    let r: Run;
    if (old_name && old_type) {
      r = company.runs.find(run => run.name === old_name);
    }
    else {
      r = new Run();
      r._id = (new ShortUniqueId()).randomUUID();
      company.runs.push(r);
    }
    r.name = new_name;
    r.sites_ids = sites;
    company.markModified('runs');
    await company.save();
  }

  async addSiteToRun(company_id, { name, siteId }) {
    let company = await this.companyModel.findById(company_id).exec();
    let r: Run;
    r = company.runs.find(run => run.name === name);
    let sitesIds = r.sites_ids;
    sitesIds.push(siteId)
    r.sites_ids = sitesIds;
    company.markModified('runs');
    await company.save();
  }

  async removeSiteFromRun(company_id, { name, siteId }) {
    let company = await this.companyModel.findById(company_id).exec();
    let r: Run;
    r = company.runs.find(run => run.name === name);
    let sitesIds = [];
    r.sites_ids.forEach(id => {
      if (id !== siteId) {
        sitesIds.push(id);
      }
    });
    r.sites_ids = sitesIds;
    company.markModified('runs');
    await company.save();
  }

  async getPurpose(coll_id) {
    return (await this.collectionModel.findById(coll_id).exec()).purpose;
  }

  async getSiteAddressAndDirections(company_id: string, name: String) {
    const site = await this.siteModel.findOne({ company_id: company_id, name: name }).exec();
    let ans = { address: site.hasOwnProperty("address") ? site.address : "", directions: site.hasOwnProperty("directions") ? site.directions : "" };
    return ans;
  }

  async getCompanyLocation(company_id: string): Promise<any> {
    const company = await this.companyModel.findById(company_id);
    return { lat: company.latitude, lng: company.longitude };
  }

  async createGetUnidentifiedInsectQuery(filter_params) {
    let filterExp: any = {};

    // if jump to, ignore all the other filters
    if (!filter_params.insectID) {

      // if filtered second opinion
      if (filter_params.secondOpinion) {
        filterExp.second_opinion = filter_params.secondOpinion;
      }

      // collection_ids
      filterExp.collection_id = { $in: filter_params.collections_ids };

      // prev_insects
      filterExp.insect_id = { $nin: filter_params.prev_insects };

      // not assigned
      filterExp.username = { $in: [null, ""] };
    }
    else {
      filterExp.$and = [{ insect_id: filter_params.insectID },
      { insect_id: { $nin: filter_params.prev_insects } }]
    }
    return filterExp;
  }

  async createGetInsectQuery(filter_params) {
    let filterExp: any = {};
    let exprAnd: any = [];
    let identificationExpr: any = [];
    // if jump to, ignore all the other filters
    if (!filter_params.insectID) {
      // viewUnidentified
      if (filter_params.viewUnidentified && !filter_params.secondOpinion) {
        identificationExpr.push({ identifications: { $size: 0 } });
        let kindIds = (await this.kindModel.find({ $or: [{ $and: [{ genus: { $ne: "Non mosquito" } }, { sex: "Unidentified" }] }, { species: "Unidentified" }] }, { _id: 1 }).exec()).map(kind => kind._id);
        identificationExpr.push({ $expr: { $in: [{ $arrayElemAt: ["$identifications.kind_id", -1] }, kindIds] } });
        filterExp.$or = identificationExpr;
      }
      else {
        if (filter_params.viewUnidentified && filter_params.secondOpinion) {
          filterExp.identifications = { $not: { $size: 0 } };
          exprAnd.push({ $expr: { $eq: [{ $arrayElemAt: ["$identifications.second_opinion", -1] }, true] } });
        }
        else {
          filterExp.identifications = { $not: { $size: 0 } };
          exprAnd.push({ $expr: { $eq: [{ $arrayElemAt: ["$identifications.second_opinion", -1] }, false] } });
        }
      }

      // species genus
      if (filter_params.genus || filter_params.species) {
        let kindsFilter: any = {};
        if (filter_params.genus) {
          kindsFilter.genus = filter_params.genus;
        }
        if (filter_params.species) {
          kindsFilter.species = filter_params.species;
        }
        const kind_ids = (await this.kindModel.find(kindsFilter).exec()).map(kind => kind._id);
        exprAnd.push({ $expr: { $in: [{ $arrayElemAt: ["$identifications.kind_id", -1] }, kind_ids] } });
      }

      // taggedBy
      if (filter_params.taggedBy) {
        exprAnd.push({ $expr: { $eq: [{ $arrayElemAt: ["$identifications.tagger_username", -1] }, filter_params.taggedBy] } });
      }

      // secondOpinion
      if (filter_params.secondOpinion) {
        exprAnd.push({ $expr: { $eq: [{ $arrayElemAt: ["$identifications.second_opinion", -1] }, true] } });
      }

      // unVerified
      if (filter_params.viewUnverified) {
        exprAnd.push({ verifier: { $exists: false } });
      }

      // collection_ids
      const trapEvents = await this.trapEventModel.find({ collection_id: { $in: filter_params.collections_ids } }).exec();
      filterExp.trapEvent_id = { $in: trapEvents.map(te => te._id) };

      // prev_insects
      filterExp._id = { $nin: filter_params.prev_insects };
    }
    else {
      exprAnd.push({ _id: filter_params.insectID });
      filterExp._id = { $nin: filter_params.prev_insects };
    }

    // $expr $and
    if (exprAnd.length > 0) {
      filterExp.$and = exprAnd;
    }
    return filterExp;
  }

  private async isIdentified(insect_id: String) {
    const insect_obj = await this.insectModel.findById(insect_id);
    let lastElement = insect_obj.identifications.length - 1;
    if (lastElement === 0) {
      if (insect_obj.identifications[0].kind_id !== null) {
        let kind = await this.kindModel.findOne({ _id: insect_obj.identifications[0].kind_id }).exec();
        if ((kind.sex === 'Unidentified' && kind.genus !== "Non mosquito") || kind.species === 'Unidentified') {
          return false;
        }
        else {
          return true;
        }
      }
    }
    if (lastElement < 0) {
      return false;
    }
    return true;
  }

  async getInsect(username: String, filter_params: FilterParamsDto): Promise<GetInsectDto | undefined> {
    let insect: Insect = null;
    // if wanted unidentified/ filtered second opinion- withdraw insects from the insectUsage json - unidentified insect
    if ((filter_params.viewUnidentified) && (!filter_params.insectID || !this.isIdentified(filter_params.insectID))) {
      let insectUsage;
      const filterExp = await this.createGetUnidentifiedInsectQuery(filter_params);
      while (true) {
        try {
          // all the unidentified insects are saved automatically at insectUsage
          insectUsage = await this.insectUsageModel.findOne(filterExp).exec();
          // no insects left to view 
          if (insectUsage == null) {
            return undefined;
          }

          insectUsage.username = username;
          insectUsage.timestamp = new Date();
          insectUsage.save();
          break;
        } catch (e) { }
      }
      insect = await this.insectModel.findById(insectUsage.insect_id).exec();
    }

    else {
      const filterExp = await this.createGetInsectQuery(filter_params);
      insect = await this.insectModel.findOne(filterExp).exec();
    }

    // no insects left to identify
    if (insect === null) {
      return undefined;
    }

    const identifications: CreateIdentificationDto[] = await Promise.all(insect.identifications.map(async (identification) => {
      let kind: Kind = null;
      let not_valid: NotValid = null;
      if (identification.kind_id) {
        kind = await this.kindModel.findById(identification.kind_id).exec();
      }
      if (identification.not_valid_id) {
        not_valid = await this.notValidModel.findById(identification.not_valid_id).exec();
      }
      return new CreateIdentificationDto(identification, kind, not_valid);
    }))

    const trapEvent = await this.trapEventModel.findById(insect.trapEvent_id).exec();
    const collection = await this.collectionModel.findById(trapEvent.collection_id).exec();
    const collection_site: Site = await this.siteModel.findById(collection.site_id).exec();

    // create a new GetInsectDto object and return it
    return new GetInsectDto(insect, identifications, collection_site, collection);
  }

  async addManualInsects(username, insectsList, trapEvent_id, pickupDate) {
    let newKinds: { [id: string]: number } = {};
    for (let i = 0; i < insectsList.length; i++) {
      for (let j = 0; j < insectsList[i].amount; j++) {
        const createdInsect = new this.insectModel({ trapEvent_id: trapEvent_id });
        const insect_id = (new ShortUniqueId()).randomUUID();
        createdInsect._id = insect_id;
        await createdInsect.save();

        const kind = await this.kindModel.findOne({ species: insectsList[i].species, genus: insectsList[i].genus, sex: insectsList[i].sex }).exec();
        const identification_data = {
          "tagger_username": username, "species": insectsList[i].species, "genus": insectsList[i].genus, "sex": insectsList[i].sex,
          "second_opinion": false, "cant_identify": false, "comment": "", "is_fed": false, "not_valid_id": ""
        };
        let identification: Identification = Object.assign({ _id: (new ShortUniqueId()).randomUUID(), timestamp: new Date(), kind_id: kind._id }, identification_data);

        let insect_obj = await this.insectModel.findById(insect_id).exec();
        insect_obj.identifications.push(identification);

        // Alert the system that we updated an internal array
        insect_obj.markModified("identifications");
        await insect_obj.save();

        if (newKinds[kind._id] === undefined) {
          newKinds[kind._id] = 0;
        }
        newKinds[kind._id] = newKinds[kind._id] + 1;
      }
    }

    // update kind counter
    let trapEvent = await this.trapEventModel.findById(trapEvent_id).exec();
    for (const [key, val] of Object.entries(newKinds)) {
      const indx = trapEvent.kindsCounter.findIndex(kindCounter => kindCounter.kind_id === key);
      if (indx === -1) {
        trapEvent.kindsCounter.push({ _id: (new ShortUniqueId()).randomUUID(), kind_id: key, amount: val });
      }
      else {
        trapEvent.kindsCounter[indx].amount += val;
      }
    }
    trapEvent.markModified('kindCounter');
    trapEvent.save();

    // update collection machine id - 00000 (manualy uploaded), pickupDate and state
    const collection_id = trapEvent.collection_id;
    const coll_obj = await this.collectionModel.findById(collection_id).exec();
    coll_obj.machine_id = "00000";
    coll_obj.pickup_date = pickupDate;
    coll_obj.state = 2;
    await coll_obj.save();
  }

  async addIdentification(username: String, insect_id: String, createIdentificationDto: CreateIdentificationDto) {
    let new_kind = null;
    let prev_kind = null;
    let insect_obj = null;
    if (createIdentificationDto.isFed === null) {
      createIdentificationDto.isFed = false;
    }
    while (true) {
      try {
        // get the specific insect
        insect_obj = await this.insectModel.findById(insect_id).exec();

        if (!insect_obj.identifications) {
          insect_obj.identifications = [];
        }

        // create new identification object
        let i: Identification = new Identification();

        // find kind_id
        new_kind = (await this.kindModel.findOne({ genus: createIdentificationDto.genus, species: createIdentificationDto.species, sex: createIdentificationDto.sex }).exec())._id;

        if (new_kind === null) {
          i.kind_id = null;
        }
        else {
          i.kind_id = new_kind._id;
        }

        // partial identification
        if (!createIdentificationDto.cant_identify && !createIdentificationDto.second_opinion && createIdentificationDto.valid && !createIdentificationDto.noise && !createIdentificationDto.multiple && new_kind === null) {
          throw new BadRequestException("Partial identification");
        }

        i._id = (new ShortUniqueId()).randomUUID();
        i.tagger_username = username;
        i.timestamp = new Date();
        i.timestamp.setTime(i.timestamp.getTime() - new Date().getTimezoneOffset() * 60 * 1000);
        i.comment = '';
        i.cant_identify = createIdentificationDto.cant_identify;
        i.is_fed = createIdentificationDto.isFed;
        i.second_opinion = createIdentificationDto.second_opinion;
        i.not_valid_id = null;

        // not valid info
        if (!createIdentificationDto.valid) {
          let not_valid: NotValidDocument = await this.notValidModel.findOne({ head: createIdentificationDto.head, thorax: createIdentificationDto.thorax, abdomen: createIdentificationDto.abdomen, noise: createIdentificationDto.noise, multiple: createIdentificationDto.multiple }, { _id: 1 }).exec();
          if (not_valid === null) {
            not_valid = new this.notValidModel({ _id: (new ShortUniqueId()).randomUUID(), head: createIdentificationDto.head, thorax: createIdentificationDto.thorax, abdomen: createIdentificationDto.abdomen, noise: createIdentificationDto.noise, multiple: createIdentificationDto.multiple });
            await not_valid.save();
          }
          i.not_valid_id = not_valid._id;
        }

        if (insect_obj.identifications.length > 0) {
          // get the kind_id from the previous identification for later
          prev_kind = insect_obj.identifications[insect_obj.identifications.length - 1].kind_id;
        }
        const insect_usage = await this.insectUsageModel.findOne({ insect_id: insect_id }).exec();
        if (insect_obj.identifications.length === 0) {
          // no identification to second opinion- stay at the insect usage table, update second opinion field
          if (createIdentificationDto.second_opinion) {
            insect_usage.second_opinion = true;
            await insect_usage.save();
          }
          // no identification to identification(no second opinion)- remove from insect usage table 
          else {
            await this.insectUsageModel.deleteOne({ _id: insect_usage._id });
          }
        }
        // changing from no second opinion to second opinion - entering the insect back to the insectUsage table 
        else if (!insect_obj.identifications[insect_obj.identifications.length - 1].second_opinion && createIdentificationDto.second_opinion) {
          let trapEvent = await this.trapEventModel.findById(insect_obj.trapEvent_id, { _id: 1, collection_id: 1 }).exec();
          let createdInsectUsage = new this.insectUsageModel({ _id: (new ShortUniqueId()).randomUUID(), collection_id: trapEvent.collection_id, insect_id: insect_id, username: username, timestamp: new Date(), second_opinion: true });
          await createdInsectUsage.save();
        }
        // second opinion identification to full identification- remove from the insect usage table
        else if (insect_obj.identifications[insect_obj.identifications.length - 1].second_opinion && !createIdentificationDto.second_opinion) {
          await this.insectUsageModel.deleteOne({ _id: insect_usage._id });
        }

        // update the userIdentifiedCollection if needed
        if ((await this.userIdentifiedCollectionModel.countDocuments({ collection_id: insect_obj.collection_id, username: username })) === 0) {
          let createdUserIdentifiedCollection = new this.userIdentifiedCollectionModel({ username: username, collection_id: insect_obj.collection_id });
          createdUserIdentifiedCollection._id = (new ShortUniqueId()).randomUUID();
          await createdUserIdentifiedCollection.save();
        }

        insect_obj.identifications.push(i);
        insect_obj.markModified('identifications');
        await insect_obj.save();
        break;
      }
      catch (e) {
        if (e.message === "Partial identification") {
          throw e;
        }
      }
    }

    // asynchronous calls to collecction state and kindCounters update
    if (insect_obj.identifications.length === 1) {
      this.updateCollectionState(insect_obj.collection_id);
    }
    this.updateKindCounters(prev_kind, new_kind, insect_obj);
  }

  async getNotValid(not_valid_id: String) {
    return await this.notValidModel.findById(not_valid_id).exec();
  }

  async addVerification(username: String, insect_id: String) {
    const insect = await this.insectModel.findById(insect_id);
    insect.verifier = username;
    insect.verified_timestamp = new Date();
    await insect.save();
  }

  async addVerificationAll(username: String, filter_params: FilterParamsDto) {
    filter_params.prev_insects = [];
    const filterExp = await this.createGetInsectQuery(filter_params);
    let insects = await this.insectModel.find(filterExp).exec()
    insects.forEach(async insect => {
      if (!insect.hasOwnProperty("verifier")) {
        insect.verifier = username;
        insect.verified_timestamp = new Date();
        await insect.save();
      }
    })
  }

  async collectionsContainsUnverified(collection_ids: String[]) {
    const trapEventIds = (await this.trapEventModel.find({ collection_id: { $in: collection_ids } }, { _id: 1 }).exec()).map(id => id._id);
    const insects = await this.insectModel.countDocuments({ trapEvent_id: { $in: trapEventIds }, verifier: { $exists: false } });
    return insects > 0;
  }

  async AddCollection(company_id, createCollectionDto) {
    const createdCollection = new this.collectionModel(createCollectionDto);
    const site = await this.siteModel.findById({ _id: createdCollection.site_id }).exec();
    console.log(site)
    if (site) {
      site.containCollections = true;
      await site.save();
    }
    let collection_id = this.generateRandomID();
    let exist = await this.collectionModel.findOne({ _id: collection_id }).exec();
    while (exist) {
      collection_id = this.generateRandomID();
      exist = await this.collectionModel.findOne({ _id: collection_id }).exec();
    }
    createdCollection._id = collection_id;
    createdCollection.state = CollectionState.OnField;
    createdCollection.company_id = company_id;

    // new collection- don't need synchronization
    await createdCollection.save();

    // add traps events
    for (let i = 0; i < createCollectionDto.traps.length; i++) {
      if (createCollectionDto.traps[i].trap_check) {
        const createdTrapEvent = new this.trapEventModel(createCollectionDto.traps[i]);
        let trapEvent_id = this.generateRandomID();
        let exist = await this.trapEventModel.findOne({ _id: trapEvent_id }).exec();
        while (exist) {
          trapEvent_id = this.generateRandomID();
          exist = await this.trapEventModel.findOne({ _id: trapEvent_id }).exec();
        }
        createdTrapEvent._id = trapEvent_id;
        const trap_id = (await this.trapModel.findOne({ name: createCollectionDto.traps[i].trap_name }, { _id: 1 }).exec())._id;
        createdTrapEvent.trap_id = trap_id;
        const lures_ids = (await this.lureModel.find({ name: { $in: createCollectionDto.traps[i].lures.map(l => l.name) } }, { _id: 1 }).exec()).map(l => l._id);
        createdTrapEvent.lures_ids = lures_ids;
        createdTrapEvent.collection_id = collection_id;
        const trap_type_id = (await this.trapTypeModel.findOne({ name: createCollectionDto.traps[i].trap_type }).exec())._id;
        createdTrapEvent.trap_type_id = trap_type_id;
        await createdTrapEvent.save();
      }
    }

    return createdCollection._id;
  }

  async getNumberOfSecondOpinions(company_id: string) {
    const collectionsId = (await this.collectionModel.find({ company_id: company_id }, { _id: 1 }).exec()).map(id => id._id);
    const trapEventIds = (await this.trapEventModel.find({ collection_id: { $in: collectionsId } }, { _id: 1 }).exec()).map(id => id._id);

    // query the needed insects
    const query = { trapEvent_id: { $in: trapEventIds }, $expr: { $eq: [{ $arrayElemAt: ["$identifications.second_opinion", -1] }, true] } }
    return this.insectModel.countDocuments(query).exec();
  }

  async getInterestedInsects(company_id: String) {
    const company = await this.companyModel.findById(company_id).exec();
    let values: any = {};
    const interested = await this.kindModel.find({
      species: { $ne: null }, genus: { $ne: null }, _id: { $in: company.kinds_ids }, sex: { $ne: "unidentified" }
    }).exec();
    values.interest_species = interested.reduce((acc: String[], kind: Kind) => acc.indexOf(kind.species) === -1 ? acc.concat(kind.species) : acc, []);
    values.interest_genus = interested.reduce((acc: String[], kind: Kind) => acc.indexOf(kind.genus) === -1 ? acc.concat(kind.genus) : acc, []);
    return values;
  }

  async getNumberOfInsectsToTag(collections_ids: String[]) {
    let te = (await this.trapEventModel.find({ collection_id: { $in: collections_ids } }).exec()).map(te => te._id);
    let identificationExpr: any = [];
    identificationExpr.push({ identifications: { $size: 0 } });
    let kindIds = (await this.kindModel.find({ $or: [{ $and: [{ sex: "Unidentified" }, { genus: { $ne: "Non mosquito" } }] }, { species: "Unidentified" }] }, { _id: 1 }).exec()).map(kind => kind._id);
    identificationExpr.push({ $expr: { $in: [{ $arrayElemAt: ["$identifications.kind_id", -1] }, kindIds] } });
    identificationExpr.push({ $expr: { $eq: [{ $arrayElemAt: ["$identifications.second_opinion", -1] }, true] } });
    return (await this.insectModel.countDocuments({ trapEvent_id: { $in: te }, $or: identificationExpr }).exec());
  }

  async getNumberOfInsects(filter_params: FilterParamsDto) {
    if (filter_params.insectID != "" && filter_params.insectID !== null) {
      return 1;
    }
    const filterExp = await this.createGetInsectQuery(filter_params);
    const insects_number = await this.insectModel.countDocuments(filterExp).exec();
    return insects_number;
  }

  async getNumberOfInsectsLeftToIdentify(company_id: String) {
    let collections_ids = (await this.collectionModel.find({ company_id: company_id }).exec()).map(col => col._id);
    let te = (await this.trapEventModel.find({ collection_id: { $in: collections_ids } }).exec()).map(te => te._id);
    let identificationExpr: any = [];
    identificationExpr.push({ identifications: { $size: 0 } });
    let kindIds = (await this.kindModel.find({ $or: [{ $and: [{ sex: "Unidentified" }, { genus: { $ne: "Non mosquito" } }] }, { species: "Unidentified" }] }, { _id: 1 }).exec()).map(kind => kind._id);
    identificationExpr.push({ $expr: { $in: [{ $arrayElemAt: ["$identifications.kind_id", -1] }, kindIds] } });
    identificationExpr.push({ $expr: { $eq: [{ $arrayElemAt: ["$identifications.second_opinion", -1] }, true] } });
    return (await this.insectModel.countDocuments({ trapEvent_id: { $in: te }, $or: identificationExpr }).exec());
  }

  async getHomePDropdownValues(company_id: String, filter_params: any, page: number, pageSize: number) {
    let values: any = {};
    const unique = (arr: any[]) => [...new Set(arr)];
    let runsName = ((await this.companyModel.findById({ _id: company_id }, { _id: 0, runs: 1 }).exec()).runs).map(run => run.name);
    const skip = (page - 1) * pageSize;
    const company = await this.companyModel.findById(company_id).select('kinds_ids').exec();
    const collection_ids = (await this.collectionModel.find({ company_id: company_id }, { _id: 1 }).skip(skip).limit(pageSize).exec()).map(coll => coll._id);
    let sitesExpr: any = [{ company_id: company_id }];
    let kindsExpr: any = [{ _id: { $in: company.kinds_ids } }];
    let collectionsExpr: any = [{ company_id: company_id }];
    const traps = await this.trapEventModel.find({ collection_id: { $in: collection_ids } }).exec();
    let insectsExpr: any = [{ trapEvent_id: { $in: traps } }];

    // jump to
    if (filter_params.insectID !== null) {
      const insect_obj = await this.insectModel.findById(filter_params.insectID).exec();
      const trapEvent_obj = await this.trapEventModel.findById(insect_obj.trapEvent_id).select('collection_id').exec();
      const coll_obj = await this.collectionModel.findById(trapEvent_obj.collection_id).select('site_id _id').exec();
      sitesExpr.push({ _id: coll_obj.site_id });
      kindsExpr.push({ _id: insect_obj.identifications[insect_obj.identifications.length - 1].kind_id });
      collectionsExpr.push({ _id: coll_obj._id });
      insectsExpr.push({ _id: insect_obj._id });
    }

    // barcode
    if (filter_params.barcode !== null) {
      const coll_obj = await this.collectionModel.findById(filter_params.barcode).select('site_id _id').exec();
      const trapsEvents = await this.trapEventModel.find({ collection_id: coll_obj._id }, { kindsCounter: 1 }).exec();
      sitesExpr.push({ _id: coll_obj.site_id });
      const kindsCountersArray = trapsEvents.map((trapEvent) => trapEvent.kindsCounter.reduce((prev, kindCounter) => kindCounter.amount === 0 || prev.includes(kindCounter._id) ? prev : prev.concat(kindCounter._id), []));
      kindsExpr.push({ _id: { $in: [].concat.apply([], kindsCountersArray) } });
      collectionsExpr.push({ _id: filter_params.barcode });
      insectsExpr.push({ collection_id: filter_params.barcode });
    }

    // site name
    if (filter_params.site_name !== null) {
      const site_obj = await this.siteModel.findOne({ company_id: company_id, name: filter_params.site_name }, { _id: 1, mapVision_id: 1 }).exec();
      if (company_id == '646cae3c89d8257530a11eda' || company_id == '62a073') {
        filter_params.site_id = (site_obj.mapVision_id ? site_obj.mapVision_id : site_obj._id);
      }
      else {
        filter_params.site_id = site_obj._id;
      }
    }

    // site id
    if (filter_params.site_id !== null) {
      let site = await this.siteModel.findOne({ company_id: company_id, _id: filter_params.site_id }).exec();
      if ((company_id == '646cae3c89d8257530a11eda' || company_id == '62a073') && site == null) {
        site = await this.siteModel.findOne({ company_id: company_id, mapVision_id: filter_params.site_id }, { _id: 1 }).exec();
      }
      filter_params.site_id = site._id;
      const collections_obj = await this.collectionModel.find({ site_id: filter_params.site_id }, { _id: 1 }).exec();
      const kindIdsArray = Promise.all(collections_obj.map(async (coll_obj) => {
        const trapsEvents = await this.trapEventModel.find({ collection_id: coll_obj._id }).exec();
        const kindsCountersArray = trapsEvents.map((trapEvent) => trapEvent.kindsCounter.reduce((prev, kindCounter) => kindCounter.amount === 0 || prev.includes(kindCounter._id) ? prev : prev.concat(kindCounter._id), []));
        return [].concat.apply([], kindsCountersArray);
      }))
      const name = await this.companyModel.aggregate([
        {
          $match: {
            _id: company_id
          }
        },
        {
          $unwind: "$runs"
        },
        {
          $match: {
            "runs.sites_ids": filter_params.site_id
          }
        },
        {
          $project: {
            _id: 0,
            runName: "$runs.name",
            siteIds: {
              $filter: {
                input: "$runs.sites_ids",
                cond: { $eq: ["$$this", filter_params.site_id] }
              }
            }
          }
        }
      ]).exec();
      if (name > 0) {
        runsName.push(name[0].runName);
      }
      else {
        runsName = [];
      }
      sitesExpr.push({ _id: filter_params.site_id });
      kindsExpr.push({ _id: { $in: [].concat.apply([], kindIdsArray) } });
      collectionsExpr.push({ site_id: filter_params.site_id });
      insectsExpr.push({ collection_id: { $in: collections_obj.map((coll_obj) => coll_obj._id) } });
    }

    // position type
    if (filter_params.position !== null) {
      const sites = (await this.siteModel.find({ position_type: filter_params.position }, { _id: 1 }).exec()).map(site => site._id);
      const collections_obj = await this.collectionModel.find({ site_id: { $in: sites } }).exec();
      const kindIdsArray = Promise.all(collections_obj.map(async (coll_obj) => {
        const trapsEvents = await this.trapEventModel.find({ collection_id: coll_obj._id }, { kindsCounter: 1 }).exec();
        const kindsCountersArray = trapsEvents.map((trapEvent) => trapEvent.kindsCounter.reduce((prev, kindCounter) => kindCounter.amount === 0 || prev.includes(kindCounter._id) ? prev : prev.concat(kindCounter._id), []));
        return [].concat.apply([], kindsCountersArray);
      }))
      sitesExpr.push({ position_type: filter_params.position });
      kindsExpr.push({ _id: { $in: [].concat.apply([], kindIdsArray) } });
      collectionsExpr.push({ site_id: { $in: sites } });
      insectsExpr.push({ collection_id: { $in: collections_obj.map((coll_obj) => coll_obj._id) } });
    }

    // genus and species
    if (filter_params.genus !== null || filter_params.species !== null) {
      let query: any = {};
      if (filter_params.genus) {
        query.genus = filter_params.genus;
      }
      if (filter_params.species) {
        query.genus = filter_params.species;
      }
      const kinds = (await this.kindModel.find(query).exec()).map((kind) => kind._id);
      const trapevent_objects = (await this.trapEventModel.find({ "kindsCounter.kind_id": { $in: kinds } }, { collection_id: 1 }).exec()).map(te => te.collection_id);
      const collections_obj = await this.collectionModel.find({ _id: { $in: trapevent_objects } }, { site_id: 1, _id: 1 }).exec();
      sitesExpr.push({ _id: { $in: collections_obj.map(coll => coll.site_id) } });
      kindsExpr.push({ _id: kinds });
      collectionsExpr.push({ _id: { $in: collections_obj.map(coll => coll._id) } });
      insectsExpr.push({ $expr: { $in: [{ $arrayElemAt: ["$identifications.kind_id", -1] }, kinds] } });
    }

    // tagged by
    if (filter_params.taggedBy !== null) {
      const insects = await this.insectModel.find({ "identifications.tagger_username": filter_params.taggedBy }, { trapEvent_id: 1, identifications: 1, _id: 1 }).exec();
      const trapEvents = (await this.trapEventModel.find({ _id: { $in: insects.map(ins => ins.trapEvent_id) } }, { collection_id: 1 }).exec()).map(te => te.collection_id);
      const collections = await this.collectionModel.find({ _id: { $in: trapEvents } }, { site_id: 1, _id: 1 }).exec();
      sitesExpr.push({ _id: { $in: collections.map(coll => coll.site_id) } });
      kindsExpr.push({ _id: { $in: insects.map(ins => ins.identifications[ins.identifications.length - 1].kind_id) } });
      collectionsExpr.push({ _id: { $in: collections.map(coll => coll._id) } });
      insectsExpr.push({ _id: { $in: insects.map(ins => ins._id) } });
    }

    // runs
    if (filter_params.run) {
      const sites = await this.companyModel.findOne(
        {
          _id: company_id,
          "runs.name": filter_params.run
        },
        {
          "runs.$": 1 // Project only the matched run from the array
        }
      ).exec();
      sitesExpr.push({ _id: { $in: sites.runs[0].sites_ids } });
    }

    const sites = await this.siteModel.find({ $and: sitesExpr }, { _id: 1, mapVision_id: 1, name: 1 }).exec();
    if (company_id == '646cae3c89d8257530a11eda' || company_id == '62a073') {
      values.sitesIds = unique(sites.map(site => (site.mapVision_id ? site.mapVision_id : site._id)));
    }
    else {
      values.sitesIds = unique(sites.map(site => site._id));
    }
    values.sitesNames = unique(sites.map(site => site.name));
    const collections = (await this.collectionModel.find({ $and: collectionsExpr }, { _id: 1 }).exec()).map(coll => coll._id);
    values.collectionsIds = unique(collections);
    const kinds = await this.kindModel.find({ $and: kindsExpr }, { species: 1, genus: 1 }).exec();
    let species = kinds.map(k => k.species).filter(k => k !== null);
    values.interest_species = unique(species);
    let genus = kinds.map(k => k.genus).filter(k => k !== null);
    values.interest_genus = unique(genus);
    const insects = await this.insectModel.find({ $and: insectsExpr }, { _id: 1, "identifications.tagger_username": 1 }).exec();
    values.insectsIds = unique(insects.map(insect => insect._id));
    values.usersNames = unique(insects.reduce((prev, insect) => prev.concat(insect.identifications.map(iden => iden.tagger_username)), []));
    values.runsName = unique(runsName);
    return values;
  }

  async getCompanyRuns(company_id: String) {
    const company = await this.companyModel.findById(company_id).exec();
    return company.runs.map((run) => run.name);
  }

  async getAllCompanyRuns(company_id: String) {
    const company = await this.companyModel.findById(company_id).exec();
    return company.runs;
  }

  async getCompanyPooling(company_id: String) {
    const collectionsId = (await this.collectionModel.find({ company_id: company_id }).exec()).map(collection => collection._id);
    let poolings = [];
    let vialsId = [];
    let vialsWithResult = new Map();
    await Promise.all(collectionsId.map(async collection => {
      const trapEvents = await this.trapEventModel.find({ collection_id: collection }).exec();
      await Promise.all(trapEvents.map(async te => {
        let vials = te.vials;
        if (vials.length > 0) {
          await Promise.all(vials.map(async vial => {
            if (vial._id !== undefined) {
              vialsId.push(vial._id);
            }
            let kind = await this.kindModel.findById({ _id: vial.kind_id });
            let name = kind.genus + " " + kind.species + " " + kind.sex;
            if (vial.tests.length > 0) {
              vialsWithResult.set(vial._id, 1);
              let tests = vial.tests;
              for (let n = 0; n < tests.length; n++) {
                poolings.push({ _id: vial._id, collection_barcode: collection, species: kind.species, genus: kind.genus, sex: kind.sex, name: name, virus: tests[n].virus_test, result: tests[n].result === false ? "Negative" : "Positive" });
              }
            }
          }))
        }
      }))
    }))
    return [poolings, vialsId, Array.from(vialsWithResult.keys())];
  }

  async getPositivePooling(company_id: String) {
    const collectionsId = (await this.collectionModel.find({ company_id: company_id }).exec()).map(collection => collection._id);
    let poolings = [];
    await Promise.all(collectionsId.map(async collection => {
      const trapEvents = await this.trapEventModel.find({ collection_id: collection }).exec();
      await Promise.all(trapEvents.map(async te => {
        let vials = te.vials;
        if (vials.length > 0) {
          await Promise.all(vials.map(async vial => {
            if (vial.tests.length > 0) {
              let tests = vial.tests;
              for (let n = 0; n < tests.length; n++) {
                if (tests[n].result === true) {
                  poolings.push(collection);
                }
              }
            }

          }))
        }
      }))
    }))
    return poolings;
  }

  async createRule(company_id, userName, kindName, ruleName, siteId, amount) {
    let user = await this.usersModel.findOne({ company_id: company_id, username: userName }).exec();
    let ruleExists = false;
    if (user.rules) {
      for (const rule of user.rules) {
        let areEqual = JSON.stringify(rule.site_id.sort()) === JSON.stringify(siteId.sort());
        if (areEqual && rule.amount === Number(amount) && kindName === rule.kindName) {
          ruleExists = true;
          break; // Exit the loop when a match is found
        }
      }
    } else {
      user.rules = [];
    }
    if (ruleExists) {
      return "alreadyExists";
    }
    let data = {};
    data['company_id'] = company_id;
    data['userName'] = userName;
    const rule = new Rule();
    rule.amount = Number(amount);
    rule.kindName = kindName;
    rule.name = ruleName;
    rule.site_id = siteId;
    user.rules.push(rule);
    user.markModified('rules');
    data['rule'] = rule.name;
    this.socketManager.emitNewRule(data)
    await user.save();
  }

  async getKinds(company_id) {
    const company = await this.companyModel.findById(company_id);
    const kinds_ids = company.kinds_ids;
    return this.kindModel.find({ _id: { $in: kinds_ids } }).exec();
  }

  async getGenusOfSpecies(species) {
    return (await (this.kindModel.findOne({ species: species }).exec())).genus;
  }

  async getCollectionInsectsInfo(collection_id) {
    let data = {};
    let traps = [];
    const trapsEvents = await this.trapEventModel.find({ collection_id: collection_id }).exec();
    await Promise.all(trapsEvents.map(async trapEvent => {
      const trap_name: any = (await this.trapModel.findById(trapEvent.trap_id).exec()).name;
      if (!traps.includes(trap_name)) {
        traps.push(trap_name);
      }
      await Promise.all(trapEvent.kindsCounter.map(async kindCounter => {
        let kind = await this.kindModel.findById(kindCounter.kind_id).exec();
        let full_name = kind.species + " " + kind.genus;
        if (!(full_name in data)) {
          data[full_name] = {};
        }
        if (!(trap_name in data[full_name])) {
          data[full_name][trap_name] = {};
        }
        data[full_name][trap_name][kind.sex] = kindCounter.amount;
      }))
    }))
    const output = { data, traps }
    return output;
  }

  async getAllKinds() {
    return this.kindModel.find({ sex: "unidentified" }).exec();
  }

  async getAllKindsNames() {
    const kinds = (await this.kindModel.find({ sex: "Female" }).exec()).map(kind => {
      return kind.genus + " " + kind.species;
    });
    return kinds;
  }

  async getKindsAmount(collection_id, interestedKinds: [{ _id: string, genus: string, species: string, sex: string }]) {
    const interestedKindsIds = interestedKinds.map(kind => kind._id);
    var trapEvents = (await this.trapEventModel.find({ collection_id: collection_id }, { _id: 1 }).exec()).map(te => te._id);
    var insects = await this.insectModel.find({ "trapEvent_id": { $in: trapEvents } }, { "identifications.kind_id": 1 }).exec();
    var result = {};
    await Promise.all(insects.map(async insect => {
      let lastElement = insect.identifications.length - 1;
      if (insect.identifications.length > 0 && insect.identifications[lastElement].kind_id !== null) {
        let kind_id = insect.identifications[lastElement].kind_id.toString();
        if (!result[kind_id]) {
          result[kind_id] = 1;
        }
        else { result[kind_id] += 1; }
      }
    }));
    var kinds = [];
    for (var i in result) {
      var kind = (await this.kindModel.findById(i));
      kinds.push({ "species": kind.species, "genus": kind.genus, "sex": kind.sex, "amount": result[i] });
    }
    return kinds;
  }

  async getAdjustedSpeciesList(company_id: String, genus: String) {
    const company = await this.companyModel.findById(company_id).exec();
    const interested = await this.kindModel.find({ _id: { $in: company.kinds_ids }, genus: genus });
    return interested.reduce((acc: String[], interested: KindDocument) => acc.indexOf(interested.species) === -1 ? acc.concat(interested.species) : acc, []);
  }

  async getAdjustedGenusList(company_id: String, species: String) {
    const company = await this.companyModel.findById(company_id).exec();
    const interested = await this.kindModel.find({ _id: { $in: company.kinds_ids }, species: species });
    return interested.reduce((acc: String[], interested: KindDocument) => acc.indexOf(interested.genus) === -1 ? acc.concat(interested.genus) : acc, []);
  }

  // async getAdjustedRunsList(company_id: String, type: String) {
  //   const company = await this.companyModel.findById(company_id).exec();
  //   return company.runs.reduce((acc: String[], run: Run) => run.type === type ? acc.concat(run.name) : acc, []);
  // }

  async updateCollectionState(collection_id: String) {
    while (true) {
      try {
        let collection = await this.collectionModel.findById(collection_id).exec();
        let filterExp: any = [];
        filterExp.push({ identifications: { $size: 0 } });
        let kindIds = (await this.kindModel.find({ $or: [{ $and: [{ sex: "Unidentified" }, { genus: { $ne: "Non mosquito" } }] }, , { species: "Unidentified" }] }, { _id: 1 }).exec()).map(kind => kind._id);
        filterExp.push({ $expr: { $in: [{ $arrayElemAt: ["$identifications.kind_id", -1] }, kindIds] } });
        filterExp.push({ $expr: { $eq: [{ $arrayElemAt: ["$identifications.second_opinion", -1] }, true] } });
        const query = { collection_id: collection_id, $or: filterExp }
        if (await this.insectModel.countDocuments(query).exec() > 0) {
          if (collection.state !== 1) {
            collection.state = 1;
            await collection.save();
          }
        }
        else {
          if (collection.state !== 2) {
            collection.state = 2;
            await collection.save();
          }
        }
      }
      catch (e) { }
    }
  }

  async updateKindCounters(prev_kind, new_kind, insect_obj) {
    // update the trap event's kindsCounter
    // synchronization because of save
    while (true) {
      try {
        let trapevent_obj = await this.trapEventModel.findById(insect_obj.trapEvent_id).exec();

        if (prev_kind !== null) {
          // return KindCounter index according to kind_id
          const former_kind_counter_index = trapevent_obj.kindsCounter.findIndex(kC => kC.kind_id === prev_kind);

          trapevent_obj.kindsCounter[former_kind_counter_index].amount -= 1;
        }

        if (new_kind !== null) {
          // return KindCounter id according to kind_id
          const new_kind_counter_index = trapevent_obj.kindsCounter.findIndex(kC => kC.kind_id === new_kind);

          // check if the new kindCounter exists 
          if (new_kind_counter_index != -1) {
            trapevent_obj.kindsCounter[new_kind_counter_index].amount += 1;
          }
          else {
            trapevent_obj.kindsCounter.push({ _id: (new ShortUniqueId()).randomUUID(), kind_id: new_kind, amount: 1 });
          }
        }

        // if it's the last insect- change collection's state
        const allTrapEvents = await this.trapEventModel.find({ collection_id: trapevent_obj.collection_id }).exec();
        let filterExp: any = [];
        filterExp.push({ identifications: { $size: 0 } });
        let kindIds = (await this.kindModel.find({ $or: [{ $and: [{ sex: "Unidentified" }, { genus: { $ne: "Non mosquito" } }] }, { species: "Unidentified" }] }, { _id: 1 }).exec()).map(kind => kind._id);
        filterExp.push({ $expr: { $in: [{ $arrayElemAt: ["$identifications.kind_id", -1] }, kindIds] } });
        filterExp.push({ $expr: { $eq: [{ $arrayElemAt: ["$identifications.second_opinion", -1] }, true] } });
        if ((await this.insectModel.find({ trapEvent_id: { $in: allTrapEvents.map(te => te._id) }, $or: filterExp }).exec()).length === 0) {
          let collection_obj = await this.collectionModel.findById(trapevent_obj.collection_id).exec();
          collection_obj.state = CollectionState.Completed;
          await collection_obj.save();
        }

        trapevent_obj.markModified('kindsCounter');
        await trapevent_obj.save();
        break;
      }
      catch (e) { }
    }
  }

  async freeUserAssigned(username: String) {
    let insectUsages = await this.insectUsageModel.find({ username: username }).exec();
    insectUsages.forEach(async insect_usage => {
      try {
        insect_usage.username = "";
        await insect_usage.save();
      } catch { }
    })
  }

  async connectUserToCollection(company_id: String, socket, collection_id, userName) {
    this.socketManager.registerUserToCollection(company_id, socket, collection_id, userName);
  }

  async removeUserFromCollection(company_id: String, socket, collection_id, userName) {
    this.socketManager.removeUserFromCollection(company_id, socket, collection_id, userName);
  }

  async getRunSites(company_id: String, run_name: String) {
    const company = await this.companyModel.findById(company_id).exec();
    const runs = company.runs;
    for (let i = 0; i < runs.length; i++) {
      if (runs[i].name === run_name) {
        const runs_ids = Object.values(runs[i].sites_ids);
        const sites = await this.siteModel.find({ _id: { $in: runs_ids } });
        return sites;
      }
    }
    return null;
  }

  async addTest(company_id, vial_barcode, test, result) {
    const testAdded = [];
    await Promise.all(vial_barcode.map(async barcode => {
      const te = await this.trapEventModel.findOne({ "vials._id": { $eq: barcode } }).exec();
      let collection_id = te.collection_id;
      for (let j = 0; j < te.vials.length; j++) {
        if (te.vials[j]._id === barcode) {
          let kind = await this.kindModel.findById({ _id: te.vials[j].kind_id }).exec();
          let name = kind.genus + " " + kind.species + " " + kind.sex;
          for (let i = 0; i < test.length; i++) {
            let t: Test = new Test();
            t._id = (new ShortUniqueId()).randomUUID();
            t.virus_test = test[i];
            t.result = result;
            te.vials[j].tests.push(t);
            te.markModified('vials');
            await te.save();
            testAdded.push({ _id: barcode, collection_barcode: collection_id, species: kind.species, genus: kind.genus, sex: kind.sex, name: name, virus: test[i], result: result === true ? "Positive" : "Negative" });
          }
        }
      }
    }))

    // if the new test has a virus in it, send notification to the company users
    if (result) {
      this.socketManager.emitCompany(company_id, "newVirus", { vial_barcodes: vial_barcode, test_name: test });
    }

    return testAdded;
  }

  async deleteTestFromVial(vial_id, virus, result) {
    const te = await this.trapEventModel.findOne({ $and: [{ "vials._id": vial_id }, { "vials.tests.result": result }, { "vials.tests.virus_test": virus }] }).exec();
    let newTests = [];
    te.vials.map(vial => {
      if (vial._id === vial_id) {
        vial.tests.map(test => {
          if (!(test.virus_test === virus)) {
            newTests.push(test);
          }
        });
        vial.tests = newTests;
      }
    });
    te.markModified("vials");
    await te.save();
  }

  async deleteSite(company_id, site_id) {
    const result = await this.siteModel.deleteOne({ company_id: company_id, _id: site_id });
  }

  async getTests(company_id, checked_pooling_obj) {
    const kind = await this.kindModel.findOne({ species: checked_pooling_obj.species, genus: checked_pooling_obj.genus, sex: checked_pooling_obj.sex });
    const company = await this.companyModel.findById({ _id: company_id });
    const insects_tests = company.insects_tests;
    for (let i = 0; i < insects_tests.length; i++) {
      if (insects_tests[i].kind_id === kind._id) {
        return insects_tests[i].viruses_tests;
      }
    }
  }

  async insectAtCollection(collections_ids, insect_id) {
    const insect = await this.insectModel.findById(insect_id).exec();
    const trapEvent = await this.trapEventModel.findById(insect.trapEvent_id);
    return collections_ids.filter(value => value === trapEvent.collection_id);
  }

  async getCompanyTrapsAndLures(company_id) {
    let company = await this.companyModel.findById(company_id).exec();
    let traps = [];
    for (let i = 0; i < company.traps_ids.length; i++) {
      const t = company.traps_ids[i];
      let trap_name = await this.trapModel.findOne({ _id: t }, { name: 1 }).exec();
      let trap_type_id = await this.typeToTrapModel.findOne({ company_id: company_id, trap_id: t }, { trap_type_id: 1 }).exec();
      console.log(t, trap_name, trap_type_id)
      let trap_type_name = await this.trapTypeModel.findById(trap_type_id.trap_type_id).exec();

      let trap_lures = [];
      for (let j = 0; j < company.lures_ids.length; j++) {
        const l = company.lures_ids[j];
        let lure_name = await this.lureModel.findById(l);
        let lure_check = await this.lureToTrapModel.countDocuments({ company_id: company_id, trap_id: t, lure_id: l }).exec();
        trap_lures.push({ lure_name: lure_name.name, lure_check: lure_check === 1 });
      }
      traps.push({ trap_name: trap_name.name, trap_check: false, lures: trap_lures, trap_type: trap_type_name.name });
    }
    return traps;
  }

  // Return an Array linking the collections trap names to trapEvent ids
  async getTrapsEventsIdsOfCollection(collection_id) {
    // get all of the collections trapEvents
    const trap_events = await this.trapEventModel.find({ collection_id: collection_id }, { _id: 1, trap_id: 1 }).exec();
    // create the output Array
    return await Promise.all(trap_events.map(async (trap_event): Promise<readonly [unknown, unknown]> =>
      // get the trapEvent's trap name and link it to the trapEvent id
      [(await this.trapModel.findById(trap_event.trap_id, { name: 1 }).exec()).name, trap_event._id]
    ));
  }

  async getTrapsEventsIdsOfCompany(company_id) {
    const collections_ids = (await this.collectionModel.find({ company_id: company_id }, { _id: 1 }).exec()).map(coll => coll._id);
    return (await this.trapEventModel.find({ collection_id: { $in: collections_ids } }, { _id: 1 }).exec()).map(trapEvent => trapEvent._id);
  }

  async asyncFilter(arr, predicate) {
    const results = await Promise.all(arr.map(predicate));
    return arr.filter((_v, index) => results[index]);
  }

  async userIdentifiedCollection(collections_ids, user) {
    return await this.asyncFilter(collections_ids, async collection_id => (
      (await this.userIdentifiedCollectionModel.countDocuments({ collection_id: collection_id, username: user })) > 0
    ));
  }

  async getGuidePictures() {
    return (await this.guidePicturesModel.find({}).exec());
  }

  async getCollectionIdFromVial(vial_id) {
    let collection_id = (await this.trapEventModel.findOne({ "vials._id": vial_id[0] }).exec()).collection_id;
    return [collection_id];
  }

  async getCollectionOfInsect(insect_id) {
    const trapEvent_id = (await this.insectModel.findById(insect_id).exec()).trapEvent_id;
    return [(await this.trapEventModel.findById(trapEvent_id).exec()).collection_id];
  }

  async getFilteredCollectionData(collections, filter_params) {
    let query: any = {};
    let collectionsFiltered = {};
    let kindIds;
    if (filter_params.genus) {
      query.genus = filter_params.genus;
    }
    if (filter_params.species) {
      query.species = filter_params.species;
    }
    if (filter_params.genus || filter_params.species) {
      kindIds = (await this.kindModel.find(query).exec()).map(kind => kind._id);
    }
    for (let i = 0; i < collections.length; i++) {
      let siteId = collections[i][0].site_id.toString();
      if (collections[i][1] !== null) {
        let prevNum, currNum;
        let trapEventIds_prev = (await this.trapEventModel.find({ collection_id: collections[i][0] }).exec()).map(te => te._id);
        let trapEventIds_curr = (await this.trapEventModel.find({ collection_id: collections[i][1] }).exec()).map(te => te._id);
        if (filter_params.genus || filter_params.species) {
          prevNum = await this.insectModel.countDocuments({ trapEvent_id: { $in: trapEventIds_prev }, "identifications.kind_id": { $in: kindIds } });
          currNum = await this.insectModel.countDocuments({ trapEvent_id: { $in: trapEventIds_curr }, "identifications.kind_id": { $in: kindIds } });
        }
        else {
          prevNum = collections[i][0].total;
          currNum = collections[i][1].total;
        }
        collectionsFiltered[siteId] = [prevNum, currNum]
      }
      else {
        collectionsFiltered[siteId] = [0, 0]
      }
    }

    return collectionsFiltered;
  }

  async updateSocket(company_id, oldId, newId) {
    this.socketManager.updateSocketId(company_id, oldId, newId);
  }

  async getUserMessages(company_id, userName) {
    let res: any = {};
    let messages = (await this.usersModel.findOne({ company_id: company_id, username: userName }, { _id: 0, messages: 1 }).exec()).messages;
    let rulesName = await this.usersModel.find({ company_id: company_id, username: userName }, { _id: 0, "rules.name": 1 }).exec();
    res.messages = messages;
    if (rulesName[0].rules !== undefined) {
      res.rulesName = rulesName[0].rules.map(rule => rule.name);
    }
    else res.rulesName = [];
    return res;
  }

  async deleteUserMessages(company_id, userName) {
    let user = (await this.usersModel.findOne({ company_id: company_id, username: userName }).exec());
    user.messages = [];
    user.markModified("messages");
    await user.save();
  }

  async deleteRule(company_id, ruleName, userName) {
    const user = await this.usersModel.findOneAndUpdate(
      { company_id: company_id, username: userName },
      { $pull: { 'rules': { name: ruleName } } },
      { new: true }
    ).exec();
    user.markModified("rules");
    await user.save();
  }

  haversine(lat1, lon1, lat2, lon2) {
    const R = 6371; // Radius of the Earth in kilometers
    const dLat = (lat2 - lat1) * (Math.PI / 180);
    const dLon = (lon2 - lon1) * (Math.PI / 180);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(lat1 * (Math.PI / 180)) *
      Math.cos(lat2 * (Math.PI / 180)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const distance = R * c; // Distance in kilometers
    return distance;
  }

  async devideLandmarks(landmarkes, users) {
    const resultArrays = [];
    const coordinates = landmarkes.map(landmark => [landmark.lat, landmark.lng]);
    const kmeans = new KMEANS();
    const clusters = kmeans.run(coordinates, users.length);

    // Create an array of 2 arrays with the landmarks
    clusters.forEach((cluster, index) => {
      const clusterLandmarks = cluster.map(dataIndex => landmarkes[dataIndex]);
      resultArrays.push(clusterLandmarks);
    });
    return resultArrays;
  }

  calculateCentroid(landmarks) {
    const sumLat = landmarks.reduce((acc, curr) => acc + curr.lat, 0);
    const sumLng = landmarks.reduce((acc, curr) => acc + curr.lng, 0);
    const centroidLat = sumLat / landmarks.length;
    const centroidLng = sumLng / landmarks.length;
    return { lat: centroidLat, lng: centroidLng };
  }

  async createUserSpread(company_id, areaId, landmarkes) {
    const company = await this.companyModel.findById(company_id).exec();
    const spreadingAreas = company.spreading_areas;
    const area = spreadingAreas.find(area => area._id === areaId);

    if (!area) {
      throw new Error('Area not found');
    }

    const userSpreads = area.userSpreads;

    landmarkes.forEach(async landmark => {
      let closestUserSpread;
      let closestDistance = Infinity;

      // Find the closest userSpread for this landmark
      userSpreads.forEach(userSpread => {
        const userCoordinates = userSpread.spreadsArray.map(spreadId => {
          const matchedLandmark = area.landMarks.find(landmark => landmark._id === spreadId);
          return [Number(matchedLandmark.lat), Number(matchedLandmark.lng)];
        });

        // Calculate distance between the current userSpread and the landmark
        const distances = userCoordinates.map(coord => {
          const dx = Number(coord[0]) - landmark.lat;
          const dy = Number(coord[1]) - landmark.lng;
          return Math.sqrt(dx * dx + dy * dy);
        });

        const minDistance = Math.min(...distances);
        console.log(minDistance)
        if (minDistance < closestDistance) {
          closestUserSpread = userSpread;
          closestDistance = minDistance;
        }
      });

      // Add the landmark to the closest userSpread
      if (closestUserSpread) {
        let userSpread = closestUserSpread.spreadsArray;
        userSpread.push(landmark._id);
        company.spreading_areas.find(area => area._id === areaId).userSpreads.find(spread => spread._id === closestUserSpread._id).spreadsArray = userSpread;
        company.markModified('spreading_areas');

      }
    });
    await company.save();
    // Save the changes to the company

  }

  // Function to calculate the distance between two points
  calculateDistance(point1, point2) {
    const lat1 = point1.lat;
    const lon1 = point1.lng;
    const lat2 = point2.lat;
    const lon2 = point2.lng;

    const R = 6371e3; // Radius of the earth in meters
    const φ1 = lat1 * Math.PI / 180; // φ, λ in radians
    const φ2 = lat2 * Math.PI / 180;
    const Δφ = (lat2 - lat1) * Math.PI / 180;
    const Δλ = (lon2 - lon1) * Math.PI / 180;

    const a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
      Math.cos(φ1) * Math.cos(φ2) *
      Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    const distance = R * c; // in meters
    return distance;
  }

  checkDistance(landmark1, landmark2) {
    const distance = this.haversine(
      landmark1.lat,
      landmark1.lng,
      landmark2.lat,
      landmark2.lng
    );

    if (distance <= 2) return true;
    else return false;
  }

  async createLandmarks(company_id, areaName, users, landmarkes, day) {
    const company = await this.companyModel.findById(company_id).exec();
    console.log(landmarkes)
    const spreadingAreas = company.spreading_areas;
    let existArea = company.spreading_areas.find(area => area.name === areaName);
    let areaId, name;
    let landmarksArray = [];
    if (existArea) {
      landmarkes.forEach(async latLng => {
        let exist = existArea.landMarks.find(l => l.address === latLng[0]);
        if (exist === undefined) {
          let i = 8;
          let landmark = new Landmarks();
          landmark._id = this.generateRandomID();
          landmark.active = true;
          landmark.address = latLng[0];
          landmark.clientName = latLng[1];
          landmark.rollsNumber = latLng[2];
          landmark.email = latLng[3];
          landmark.cellPhone = latLng[4];
          landmark.inside = latLng[5] === 'פנימי' ? true : false;
          let totalMinutes = latLng[6] * 24 * 60;
          let hours = Math.floor(totalMinutes / 60);
          let minutes = totalMinutes % 60;
          let timeString = `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}`;
          landmark.startTime = timeString;
          totalMinutes = latLng[7] * 24 * 60;
          hours = Math.floor(totalMinutes / 60);
          minutes = totalMinutes % 60;
          timeString = `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}`;
          landmark.endTime = timeString;
          if (latLng.length === 11) {
            landmark.notes = latLng[i];
            i++;
          }
          landmark.lat = latLng[i];
          landmark.lng = latLng[i + 1];
          landmarksArray.push(landmark);
        }
      });
      let allLandmarks = existArea.landMarks;
      existArea.landMarks = allLandmarks.concat(landmarksArray);
      areaId = existArea._id;
    }
    else {
      landmarkes.forEach(async latLng => {
        let i = 8;
        let landmark = new Landmarks();
        landmark._id = this.generateRandomID();
        landmark.active = true;
        landmark.address = latLng[0];
        landmark.clientName = latLng[1];
        landmark.rollsNumber = latLng[2];
        landmark.email = latLng[3];
        landmark.cellPhone = latLng[4]
        landmark.inside = latLng[5] === 'פנימי' ? true : false;
        let totalMinutes = latLng[6] * 24 * 60;
        let hours = Math.floor(totalMinutes / 60);
        let minutes = totalMinutes % 60;
        let timeString = `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}`;
        landmark.startTime = timeString;
        totalMinutes = latLng[7] * 24 * 60;
        hours = Math.floor(totalMinutes / 60);
        minutes = totalMinutes % 60;
        timeString = `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}`;
        landmark.endTime = timeString;
        if (latLng.length === 11) {
          landmark.notes = latLng[i];
          i++;
        }

        landmark.lat = latLng[i];
        landmark.lng = latLng[i + 1];
        landmarksArray.push(landmark);
      });
      areaId = this.generateRandomID();
      name = areaName;
      let area = new SpreadingAreas();
      area._id = areaId;
      area.name = name;
      area.landMarks = landmarksArray;
      area.userSpreads = [];
      spreadingAreas.push(area);
    }
    company.markModified('spreading_areas');
    await company.save();
    landmarksArray.map(l => { l._id, l.lat, l.lng });
    if (existArea) {
      this.createUserSpread(company_id, areaId, landmarksArray);
    }
    else {
      const resultArray = await this.devideLandmarks(landmarksArray, users)
      let userIndex = 0;
      const datesArray = [];
      const today = new Date();
      const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
      day.forEach(d => {
        let prevDate = days.indexOf(d) - today.getDay();
        if (prevDate <= 0) {
          prevDate += 7;
        }
        let millisecondsInADay = 24 * 60 * 60 * 1000;
        let millisecondsUntilNextDay = prevDate * millisecondsInADay;

        // Calculate the next day's date
        let nextDayDate = new Date(today.getTime() + millisecondsUntilNextDay);
        let date = nextDayDate.toLocaleDateString('en-US', {
          month: '2-digit',
          day: '2-digit',
          year: 'numeric'
        });
        datesArray.push(date)
      });

      userIndex = 0;
      resultArray.forEach(async res => {
        let numArray = [];
        res.forEach(async latLng => {
          numArray.push(latLng._id);
        });
        let user = new userSpreads();
        user.date = datesArray[userIndex].toString();
        user._id = this.generateRandomID();
        user.userName = users[userIndex];
        user.areaName = 'area';
        user.spreadsArray = numArray;
        spreadingAreas.find(area => area._id === areaId).userSpreads.push(user);
        userIndex++;
      });
      company.markModified('spreading_areas');
      await company.save();
    }
    return 'success';
  }

  async getAreas(company_id) {
    const areas = (await this.companyModel.findById(company_id).exec()).spreading_areas.map(a => a.name);
    areas.push('All Areas')
    return areas;
  }

  async getLandmarks(company_id, areaName) {
    const company = await this.companyModel.findOne({ _id: company_id }).exec();
    const spreading_areas = company.spreading_areas;

    if (areaName === 'All Areas') {
      let allAreasData = {
        landMarks: [],
        userSpreads: []
      };

      spreading_areas.forEach(area => {
        area.landMarks.forEach(mark => {
          let existingMark = allAreasData.landMarks.find(l => l._id === mark._id);
          if (existingMark) {
            existingMark.amount += mark.spreads ? mark.spreads.reduce((sum, spread) => sum + Number(spread.amount), 0) : 0;
          } else {
            allAreasData.landMarks.push({
              ...mark,
              amount: mark.spreads ? mark.spreads.reduce((sum, spread) => sum + Number(spread.amount), 0) : 0,
              dates: []
            });
          }
        });

        area.userSpreads.forEach(userSpread => {
          userSpread.spreadsArray.forEach(spreadId => {
            let mark = allAreasData.landMarks.find(l => l._id === spreadId);
            if (mark) {
              mark.dates.push(userSpread.date);
            }
          });
        });
      });
      return allAreasData;
    } else {
      let area = spreading_areas.find(o => o.name === areaName);
      let data = area;

      for (let i = 0; i < area.landMarks.length; i++) {
        let count = 0;
        if (area.landMarks[i].spreads) {
          for (let j = 0; j < area.landMarks[i].spreads.length; j++) {
            count += Number(area.landMarks[i].spreads[j].amount);
          }
        }
        data.landMarks[i]['amount'] = count;
        data.landMarks[i]['dates'] = [];
      }

      for (let i = 0; i < area.userSpreads.length; i++) {
        for (let j = 0; j < area.userSpreads[i].spreadsArray.length; j++) {
          let index = data.landMarks.findIndex(l => l._id === area.userSpreads[i].spreadsArray[j]);
          data.landMarks[index]['dates'].push(area.userSpreads[i].date);
        }
      }
      return data;
    }
  }


  async editAreaName(company_id, prevName, newName) {
    const company = await this.companyModel.findById(company_id).exec();
    let areaIndex = company.spreading_areas.findIndex(area => area.name === prevName);
    company.spreading_areas[areaIndex].name = newName;
    company.markModified('spreading_areas');
    await company.save();
  }

  async addSpreadData(company_id, checkedLandmarks, date) {
    let response;
    const company = await this.companyModel.findById(company_id).exec();
    for (const checkedLandmark of checkedLandmarks) {
      const spreadingArea = company.spreading_areas.find(area =>
        area.landMarks.some(landmark => landmark._id === checkedLandmark._id)
      );

      if (spreadingArea) {
        // Find the landmark within the spreading area
        const landmark = spreadingArea.landMarks.find(landmark =>
          landmark._id === checkedLandmark._id
        );

        if (landmark) {
          // Create a new Spread object with the provided date and amount
          let newSpread = new Spreads();
          newSpread.amount = landmark.rollsNumber;
          newSpread.date = date;
          landmark.spreads ? landmark.spreads.push(newSpread) : landmark.spreads = [newSpread];
        }
      }
    }
    company.markModified('spreading_areas');
    await company.save();
  }

  async addNotesToClient(company_id, landmark, notes) {
    const company = await this.companyModel.findById(company_id).exec();
    const spreadingArea = company.spreading_areas.find(area =>
      area.landMarks.some(landmark => landmark._id === landmark._id)
    );

    if (spreadingArea) {
      // Find the landmark within the spreading area
      const currLandmark = spreadingArea.landMarks.find(l => l._id === landmark._id);
      if (currLandmark) {
        currLandmark.notes = notes;
      }
    }
    company.markModified('spreading_areas');
    await company.save();
  }


  async getCompanyUsers(company_id) {
    let users = (await this.usersModel.find({ company_id: company_id }, { username: 1 }).exec()).map(user => user.username);
    return users;
  }

  async createNewSpread(compamy_id, areaId, spreadsArray, areaName, date, userName) {
    const company = await this.companyModel.findById(compamy_id).exec();
    const index = company.spreading_areas.findIndex(spread => spread._id === areaId);
    let newSpread = new userSpreads();
    newSpread._id = this.generateRandomID();
    newSpread.areaName = areaName;
    newSpread.date = date;
    newSpread.spreadsArray = spreadsArray;
    newSpread.userName = userName;
    company.spreading_areas[index].userSpreads.push(newSpread);
    company.markModified('spreading_areas');
    await company.save();
  }

  async createNewLandmark(company_id, areaName, lat, lng, address, clientName, rollsNumber, daySelected, email, cellPhone, notes, courier, inside, startTime, endTime) {
    const company = await this.companyModel.findById(company_id).exec();
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    if (courier !== null) {
      let landmark = new Landmarks();
      landmark._id = this.generateRandomID();
      landmark.address = address;
      landmark.clientName = clientName;
      landmark.rollsNumber = rollsNumber;
      landmark.email = email;
      landmark.cellPhone = cellPhone;
      landmark.active = true;
      landmark.lat = lat;
      landmark.lng = lng;
      landmark.startTime = startTime;
      landmark.endTime = endTime;
      landmark.inside = inside;
      if (notes !== null) {
        landmark.notes = notes;
      }
      let user = new userSpreads();
      const today = new Date();
      let prevDate = days.indexOf(daySelected) - today.getDay();
      if (prevDate <= 0) {
        prevDate += 7;
      }
      let millisecondsInADay = 24 * 60 * 60 * 1000;
      let millisecondsUntilNextDay = prevDate * millisecondsInADay;

      // Calculate the next day's date
      let nextDayDate = new Date(today.getTime() + millisecondsUntilNextDay);
      let date = nextDayDate.toLocaleDateString('en-US', {
        month: '2-digit',
        day: '2-digit',
        year: 'numeric'
      });
      user.date = date.toString();
      user._id = this.generateRandomID();
      user.userName = courier;
      user.areaName = 'area';
      user.spreadsArray = [landmark._id];
      let areaId = this.generateRandomID();
      let area = new SpreadingAreas();
      area._id = areaId;
      area.name = areaName;
      area.userSpreads = [user];
      area.landMarks = [landmark]
      company.spreading_areas.push(area);
    }
    else {
      const index = company.spreading_areas.findIndex(spread => spread.name === areaName);
      let allLandmarks = company.spreading_areas[index].landMarks;
      let exist = allLandmarks.find(l => (l.lat === lat && l.lng === lng));
      if (exist) {
        return 'landmark already exists';
      }
      let landmark = new Landmarks();
      landmark._id = this.generateRandomID();
      landmark.address = address;
      landmark.clientName = clientName;
      landmark.rollsNumber = rollsNumber;
      landmark.email = email;
      landmark.cellPhone = cellPhone;
      landmark.active = true;
      landmark.lat = lat;
      landmark.lng = lng;
      landmark.startTime = startTime;
      landmark.endTime = endTime;
      if (notes !== null) {
        landmark.notes = notes;
      }
      allLandmarks.push(landmark);
      company.spreading_areas[index].landMarks = allLandmarks;
      let today = new Date();
      let spreadsAvailable = company.spreading_areas[index].userSpreads.filter(user => {
        const parts = user.date.split('/');
        const day = parseInt(parts[1], 10);
        const month = parseInt(parts[0], 10) - 1; // Month is 0-based
        const year = parseInt(parts[2], 10);
        const targetDate = new Date(year, month, day);
        if (targetDate > today && days[targetDate.getDay()] === daySelected) return true
        else return false
      });
      let newLandmarks = spreadsAvailable[0].spreadsArray;
      newLandmarks.push(landmark._id);
      spreadsAvailable[0].spreadsArray = newLandmarks;
    }
    company.markModified('spreading_areas');
    await company.save();
  }

  async showAllDays(company_id, areaName, lat, lng) {
    const company = await this.companyModel.findById(company_id).exec();
    const index = company.spreading_areas.findIndex(spread => spread.name === areaName);
    let allLandmarks = company.spreading_areas[index].landMarks;
    let exist = allLandmarks.find(l => (l.lat === lat && l.lng === lng));
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    if (exist) {
      return 'landmark already exists';
    }
    let today = new Date();
    let spreadsAvailable = [];
    company.spreading_areas[index].userSpreads.forEach(user => {
      const parts = user.date.split('/');
      const day = parseInt(parts[1], 10);
      const month = parseInt(parts[0], 10) - 1; // Month is 0-based
      const year = parseInt(parts[2], 10);
      const targetDate = new Date(year, month, day);
      if (targetDate > today) {
        spreadsAvailable.push(days[targetDate.getDay()]);
      }
    });
    return [...new Set(spreadsAvailable)];;
  }

  async addLandmarkToSpread(company_id, spreadId, landmark, areaName) {
    const company = await this.companyModel.findById(company_id).exec();
    const areaIndex = company.spreading_areas.findIndex(a => a.name === areaName);
    const landmarkId = company.spreading_areas[areaIndex].landMarks.find(l => l.lat === landmark.lat && l.lng === landmark.lng)._id;
    const index = company.spreading_areas[areaIndex].userSpreads.findIndex(user => user._id === spreadId);
    let spreadArray = company.spreading_areas[areaIndex].userSpreads[index].spreadsArray;
    spreadArray.push(landmarkId);
    company.spreading_areas[areaIndex].userSpreads[index].spreadsArray = spreadArray;
    company.markModified('spreading_areas');
    await company.save();
  }

  async disActiveLandmarks(company_id, areaId, landmarks) {
    const today = new Date();
    const company = await this.companyModel.findById(company_id).exec();
    const allLandmarks = company.spreading_areas.find(area => area._id === areaId).landMarks;
    const userSpread = company.spreading_areas.find(area => area._id === areaId).userSpreads.filter(user => {
      const parts = user.date.split('/');
      const day = parseInt(parts[1], 10);
      const month = parseInt(parts[0], 10) - 1; // Month is 0-based
      const year = parseInt(parts[2], 10);
      const targetDate = new Date(year, month, day);
      if (targetDate > today) return true;
      else return false
    });
    for (let i = 0; i < landmarks.length; i++) {
      let landmarkToDisActivate = allLandmarks.find(l => l._id === landmarks[i]);
      landmarkToDisActivate.active = false;
    }
    for (let i = 0; i < userSpread.length; i++) {
      let newLandmarksIds = userSpread[i].spreadsArray.filter(l => !(landmarks.includes(l)));
      userSpread[i].spreadsArray = newLandmarksIds;
    }
    company.markModified('spreading_areas');
    await company.save();
  }

  async getDataOnAllLandmarks(company_id, startDate, endDate) {
    const part1 = startDate.split('/');
    const day1 = parseInt(part1[1], 10);
    const month1 = parseInt(part1[0], 10) - 1; 
    const year1 = parseInt(part1[2], 10);
    const stratD = new Date(Date.UTC(year1, month1, day1));

    const part2 = endDate.split('/');
    const day2 = parseInt(part2[1], 10);
    const month2 = parseInt(part2[0], 10) - 1; 
    const year2 = parseInt(part2[2], 10);
    const endD = new Date(Date.UTC(year2, month2, day2));

    let company = await this.companyModel.findById(company_id).exec();
    let data = {};

    await Promise.all(company.spreading_areas.map(async (area) => {
      let landmarksArray = {};
      await Promise.all(area.landMarks.map(async (landmark) => {
        if (landmark.spreads && landmark.spreads.length > 0) {
          landmark.spreads.forEach((spread, index) => {
            const spreadDate = new Date(spread.date.toString());

            if (spreadDate >= stratD && spreadDate <= endD) {
              landmarksArray[index + landmark._id.toString()] = [
                landmark.address,
                landmark.clientName,
                landmark.cellPhone,
                landmark.email,
                landmark.active,
                landmark.rollsNumber,
                spread.date,
                spread.amount
              ];
            }
          });
        }
      }));
      data[area.name.toString()] = landmarksArray;
    }));

    return data;
  }



  company_id = "6617ace9a8d3180e43112890";

  async sendEmailToMultipleRecipients() {
    let subject = " פיזור יתושים שבועי";
    const company = await this.companyModel.findById(this.company_id).exec();
    const allLandmarks = [];
    for (let i = 0; i < company.spreading_areas.length; i++) {
      company.spreading_areas[i].landMarks.filter(l => l.active).forEach(l => {
        allLandmarks.push({
          _id: l._id,
          email: l.email,
          clientName: l.clientName
        })
      });
    }
    for (let j = 0; j < allLandmarks.length; j++) {
      const urlSurvey1 = `http://localhost/survey?userId=${allLandmarks[j]._id}`;
      const urlSurvey = `https://www.senecio-robotics-app.com/survey?userId=${allLandmarks[j]._id}`;
      // const urlSurvey2 = `https://docs.google.com/forms/d/e/1FAIpQLSfFaBHMcX-muaZmAmnPFgX91ht-1yBOvZlAF8h9dyS5oi0UbA/viewform?pli=1`;
      const htmlContentWithImage = `
    <html>
    <head>
      <title>שלום רב, ${allLandmarks[j].clientName}</title>
    </head>
    <body>
      <table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="2" style="text-align:right;">
        <p> ,${allLandmarks[j].clientName} שלום רב</p>
        <p> .בשבוע שעבר בוצע פיזור של יתושים זכרים עקרים בחצר ביתך</p>
        <p>. להצלחת התהליך, נודה לשיתוף הפעולה שלכם במילוי שאלון קצר
        <a href="${urlSurvey}" style="color:#007bff; text-decoration:none;">בקישור זה</a>
        </p>
        <p>למידע נוסף תוכלו להיכנס ל<a href="https://www.rentokil-initial.com/il" style="color:#007bff; text-decoration:none;">אתר האינטרנט שלנו</a> או לפנות למוקד החברה: 1800-877777</p>
        <tr>
        <td align="left" width="50%">
          <img src="cid:renotkilImg" style="display:block; margin:auto;">
        </td>
        <td align="right" width="50%">
          <img src="cid:ipmImage" style="display:block; margin:auto;">
        </td>
      </tr>
      </td>
        </tr>
      </table>
    </body>
    </html>
  `;
      try {
        await this.mailerService.sendMail({
          to: allLandmarks[j].email,
          subject,
          html: htmlContentWithImage,
          attachments: [{
            filename: 'ipm.png',
            path: __dirname + '/ipm.png',
            cid: 'ipmImage'
          },
          {
            filename: 'rentokil_logo.png',
            path: __dirname + '/rentokil_logo.png',
            cid: 'renotkilImg'
          }],
          from: 'emma.qa@rentokil-initial.com',
        });
        console.log(`email sent `)
      } catch{
        console.log(`was not send to ${allLandmarks[j].email}`)
      }
    }

  }

  async fillSurvey(userId, rating, comments) {
    const company = await this.companyModel.findById(this.company_id).exec();
    const today = new Date();
    const date = today.toLocaleDateString('en-US', {
      month: '2-digit',
      day: '2-digit',
      year: 'numeric'
    });
    const spreadingArea = company.spreading_areas.find(area =>
      area.landMarks.find(landmark => landmark._id === userId)
    );
    const landmark = spreadingArea.landMarks.find(landmark => landmark._id === userId);
    landmark.surveyResult ? landmark.surveyResult.push({ date: date, rate: rating, comments: comments }) : landmark.surveyResult = [{ date: date, rate: rating, comments: comments }];
    company.markModified('spreading_areas');
    await company.save();
  }

  async getSurveyResult(company_id) {
    const company = await this.companyModel.findById(company_id).exec();
    let allRes = []
    company.spreading_areas.forEach(s => {
      let res = s.landMarks.map(l => [l.clientName, l.address, l.surveyResult]);
      allRes.push(res);
    });
    return allRes;
  }

  async moveLandmarks(company_id, checkedLandmarks, prevArea, area, user, date) {
    const newArea = (await this.companyModel.findById(company_id).exec())
    const prev = newArea.spreading_areas.filter(a => a._id === prevArea);
    const newSpread = newArea.spreading_areas.filter(a => a.name === area);
    for (let i = 0; i < checkedLandmarks.length; i++) {
      newSpread[0].userSpreads.map(spread => {
        if (spread.date === date && spread.userName === user) {
          let allIds = newSpread[0].userSpreads[newSpread[0].userSpreads.length - 1].spreadsArray;
          allIds.push(checkedLandmarks[i]);
          const landmark = prev[0].landMarks.find(l => l._id === checkedLandmarks[i]);
          const allLandmarks = newSpread[0].landMarks;
          allLandmarks.push(landmark);
          newSpread[0].userSpreads[newSpread[0].userSpreads.length - 1].spreadsArray = allIds;
          newSpread[0].landMarks = allLandmarks;
        }
      })
      prev[0].landMarks = prev[0].landMarks.filter(l => l._id !== checkedLandmarks[i]);
      prev[0].userSpreads.forEach(spread => {
        spread.spreadsArray = spread.spreadsArray.filter(id => id !== checkedLandmarks[i]);
      })
    }
    newArea.markModified('spreading_areas');
    await newArea.save();
  }

  async getLandmarkById(company_id, landmarkId) {
    const spread = (await this.companyModel.findById(company_id).exec()).spreading_areas;
    for (let i = 0; i < spread.length; i++) {
      const landmark = spread[i].landMarks.find(l => l._id === landmarkId);
      if (landmark) {
        console.log(landmark)
        return landmark;
      }
    }
    return 'no landmark';
  }

  async editClient(company_id, landmarkId, clientName, rollsNumber, email, cellPhone, notes, inside, startTime, endTime) {
    const company = (await this.companyModel.findById(company_id).exec());
    for (let i = 0; i < company.spreading_areas.length; i++) {
      const landmark = company.spreading_areas[i].landMarks.findIndex(l => l._id === landmarkId);
      if (landmark !== -1) {
        company.spreading_areas[i].landMarks[landmark].clientName = clientName;
        company.spreading_areas[i].landMarks[landmark].rollsNumber = rollsNumber;
        company.spreading_areas[i].landMarks[landmark].email = email;
        company.spreading_areas[i].landMarks[landmark].cellPhone = cellPhone;
        company.spreading_areas[i].landMarks[landmark].notes = notes;
        company.spreading_areas[i].landMarks[landmark].startTime = startTime;
        company.spreading_areas[i].landMarks[landmark].endTime = endTime;
        company.spreading_areas[i].landMarks[landmark].inside = inside;
        company.markModified(`spreading_areas.${i}.landMarks.${landmark}`);
        break;
      }
    }
    await company.save();
  }


  async solveVRPTW(body) {
    try {
      const landmarks = await this.companyModel.findById('65c89b6f45878bbb080bd6b0').exec();
      console.log(landmarks.spreading_areas[0].landMarks);
      // const response = await axios.post('http://localhost:5000/solve_vrptw', landmarks.spreading_areas[0].landMarks);
      // return response.data;
    } catch (error) {
      throw error;
    }
  }

  @Cron(CronExpression.EVERY_DAY_AT_2AM, {
    timeZone: 'Asia/Jerusalem' // Israel timezone
  })
  async createWeeklyUserSpreads() {
    const company = await this.companyModel.findById(this.company_id).exec();
    for (let j = 0; j < company.spreading_areas.length; j++) {
      const area = company.spreading_areas[j];

      if (!area) {
        throw new Error('Area not found.');
      }

      const today = new Date();
      const date = today.toLocaleDateString('en-US', {
        month: '2-digit',
        day: '2-digit',
        year: 'numeric'
      });

      const yesterdayParts = date.split('/');
      const yesterdayDay = parseInt(yesterdayParts[1], 10);
      const yesterdayMonth = parseInt(yesterdayParts[0], 10) - 1; // Month is 0-based
      const yesterdayYear = parseInt(yesterdayParts[2], 10);

      let user_spreads = area.userSpreads.filter(user => {
        const parts = user.date.split('/');
        const day = parseInt(parts[1], 10);
        const month = parseInt(parts[0], 10) - 1; // Month is 0-based
        const year = parseInt(parts[2], 10);

        return day === yesterdayDay && month === yesterdayMonth && year === yesterdayYear;
      });

      for (let i = 0; i < user_spreads.length; i++) {
        let landmarksId = user_spreads[i].spreadsArray
          .map(spreadId => area.landMarks.find(landmark => landmark._id === spreadId))
          .filter(l => l && l.active)
          .map(l => l._id);

        if (landmarksId.length === 0) {
          continue;
        }

        const prevDate = user_spreads[i].date;
        const parts = prevDate.split('/');
        const day = parseInt(parts[1], 10);
        const month = parseInt(parts[0], 10) - 1; // Month is 0-based
        const year = parseInt(parts[2], 10);

        const prevDateObj = new Date(year, month, day);
        prevDateObj.setDate(prevDateObj.getDate() + 7);

        const newDate = prevDateObj.toLocaleDateString('en-US', {
          month: '2-digit',
          day: '2-digit',
          year: 'numeric'
        });

        let user = {
          date: newDate,
          _id: this.generateRandomID(),
          userName: user_spreads[i].userName,
          areaName: user_spreads[i].areaName, // Use the area name here
          spreadsArray: landmarksId
        };
        area.userSpreads.push(user);
      }
    }
    // Save the changes to the company document
    company.markModified('spreading_areas');
    await company.save();
  }

  @Cron(CronExpression.EVERY_HOUR)
  async disconnectNonActive() {
    console.log("Sched started");
    let d = new Date();
    d.setHours(d.getHours() - 1);
    let usagess = await this.insectUsageModel.find({ username: { $ne: "" }, timestamp: { $lt: d } });
    for (let i = 0; i < usagess.length; i++) {
      const insect_usage = usagess[i];
      insect_usage.username = "";
      await insect_usage.save();
    }
    console.log("Sched finished");
  }
}