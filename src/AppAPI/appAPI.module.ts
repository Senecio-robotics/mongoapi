import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppAPIController } from './appAPI.controller';
import { AppAPIService } from './appAPI.service';
import { Collection, CollectionSchema } from '../Schemas/collection.schema';
import { Company, CompanySchema } from '../Schemas/company.schema';
import { Insect, InsectSchema } from '../Schemas/insect.schema';
import { InsectUsage, InsectUsageSchema } from '../Schemas/insectUsage.schema';
import { Site, SiteSchema } from '../Schemas/site.schema';
import { Kind, KindSchema } from '../Schemas/kind.schema';
import { Users, UsersSchema } from '../Schemas/users.schema';
import { OriginImage, OriginImageSchema } from '../Schemas/originImage.schema';
import { GuidePictures, GuidePicturesSchema } from '../Schemas/guidepictures.schema';
import { UserIdentifiedCollection, UserIdentifiedCollectionSchema } from '../Schemas/userIdentifiedCollection.schema';
import { NotValid, NotValidSchema } from '../Schemas/notValid.schema';
import { TrapEvent, TrapEventSchema } from '../Schemas/trapEvent.schema';
import { Trap, TrapSchema } from '../Schemas/trap.schema';
import { TrapType, TrapTypeSchema } from '../Schemas/trapType.schema';
import { TypeToTrap, TypeToTrapSchema } from '../Schemas/typeToTrap.schema';
import { Lure, LureSchema } from '../Schemas/lure.schema';
import { LureToTrap, LureToTrapSchema } from '../Schemas/lureToTrap.schema';
import { SocketModule } from '../SocketManager/socketManager.module';
import { MailerModule } from '@nestjs-modules/mailer';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Collection.name, schema: CollectionSchema },
    ]),
    MongooseModule.forFeature([
      { name: Company.name, schema: CompanySchema },
    ]),
    MongooseModule.forFeature([
      { name: Insect.name, schema: InsectSchema },
    ]),
    MongooseModule.forFeature([
      { name: InsectUsage.name, schema: InsectUsageSchema },
    ]),
    MongooseModule.forFeature([
      { name: Site.name, schema: SiteSchema },
    ]),
    MongooseModule.forFeature([
      { name: Kind.name, schema: KindSchema },
    ]),
    MongooseModule.forFeature([
      { name: Users.name, schema: UsersSchema },
    ]),
    MongooseModule.forFeature([
      { name: OriginImage.name, schema: OriginImageSchema },
    ]),
    MongooseModule.forFeature([
      { name: UserIdentifiedCollection.name, schema: UserIdentifiedCollectionSchema },
    ]),
    MongooseModule.forFeature([
      { name: NotValid.name, schema: NotValidSchema },
    ]),
    MongooseModule.forFeature([
      { name: TrapEvent.name, schema: TrapEventSchema },
    ]),
    MongooseModule.forFeature([
      { name: Trap.name, schema: TrapSchema },
    ]),
    MongooseModule.forFeature([
      { name: TrapType.name, schema: TrapTypeSchema },
    ]),
    MongooseModule.forFeature([
      { name: TypeToTrap.name, schema: TypeToTrapSchema },
    ]),
    MongooseModule.forFeature([
      { name: Lure.name, schema: LureSchema },
    ]),
    MongooseModule.forFeature([
      { name: LureToTrap.name, schema: LureToTrapSchema },
    ]),
    MongooseModule.forFeature([
      { name: GuidePictures.name, schema: GuidePicturesSchema },
    ]),
    MailerModule.forRoot({
      transport: {
        host: 'smtp.gmail.com',
        port: 587, // or your SMTP port
        secure: false,
        auth: {
          user: 'emma.qa@rentokil-initial.com',
          pass: 'kppjjnlguczffdyy',
        },
      },
    }),
    SocketModule,
  ],
  controllers: [AppAPIController],
  providers: [AppAPIService],

})
export class AppAPIModule { }