import { Controller, Get, Post, Body, Headers, Request, UseGuards, Param, Query, Put, Delete, Logger } from '@nestjs/common';
import { AppAPIService } from './appAPI.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { CollectionDto } from '../dto/collection.dto';
import { CreateIdentificationDto } from '../dto/createIdentification.dto';
import { FilterParamsDto } from '../dto/filterParamsDto.dto';
import { GetInsectDto } from '../dto/getInsect.dto';

@Controller('api')
export class AppAPIController {
  private readonly logger = new Logger(AppAPIController.name);
  constructor(
    private readonly appAPIService: AppAPIService,
  ) { }

  // return the logged in username and company name 
  @UseGuards(JwtAuthGuard)
  @Get('getCompanyNameAndUsername')
  async getCompanyNameAndUsername(
    @Request() req) {
    this.logger.log('getCompanyNameAndUsername');
    return this.appAPIService.getCompanyNameAndUsername(req.user.company_id, req.user.username);
  }

  // return all the collections of the user's company, site info and insects ids
  @UseGuards(JwtAuthGuard)
  @Post('getCompanyCollections')
  async getCompanyCollections(
    @Request() req,
    @Body() body: any) {
    this.logger.log('getCompanyCollections');
    return this.appAPIService.getCompanyCollections(req.user.company_id, body.page, body.pageSize);
  }

  // return all the types of traps that exists
  @UseGuards(JwtAuthGuard)
  @Get('getCompanyTrapsTypes')
  async getCompanyTrapsTypes(
    @Request() req) {
    this.logger.log('getCompanyTrapsTypes');
    return this.appAPIService.getCompanyTrapsTypes(req.user.company_id);
  }

  // return all the collections ids of the user's company
  @UseGuards(JwtAuthGuard)
  @Get('getNumberOfCompanyCollections')
  async getNumberOfCompanyCollections(
    @Request() req) {
    this.logger.log('getNumberOfCompanyCollections');
    return this.appAPIService.getNumberOfCompanyCollections(req.user.company_id);
  }

  // return all the collections ids of the user's company
  @UseGuards(JwtAuthGuard)
  @Get('getCompanyCollectionsIds')
  async getCompanyCollectionsIds(
    @Request() req): Promise<any[]> {
    this.logger.log('getCompanyCollectionsIds');
    return this.appAPIService.getCompanyCollectionsIds(req.user.company_id);
  }

  // return collection editable info
  @UseGuards(JwtAuthGuard)
  @Post('getCollectionEditableInfo')
  async getCollectionEditableInfo(
    @Body() body) {
    this.logger.log('getCollectionEditableInfo');
    return this.appAPIService.getCollectionEditableInfo(body.collection_id);
  }

  // edit collection info
  @UseGuards(JwtAuthGuard)
  @Post('SaveCollectionChanges')
  async SaveCollectionChanges(
    @Body() body) {
    this.logger.log('SaveCollectionChanges');
    return this.appAPIService.SaveCollectionChanges(body.collection_id, body.site_id, body.collector, body.placement_date, body.pickup_date, body.purpose);
  }

  // return all the sites of the user's company
  @UseGuards(JwtAuthGuard)
  @Get('getCompanySites')
  async getCompanySites(
    @Request() req): Promise<any[]> {
    this.logger.log('getCompanySites');
    return this.appAPIService.getCompanySites(req.user.company_id);
  }

  // return all the company's sites that don't have collections related to them
  @UseGuards(JwtAuthGuard)
  @Get('getCompanyEmptySites')
  async getCompanyEmptySites(
    @Request() req): Promise<any[]> {
    this.logger.log('getCompanyEmptySites');
    return this.appAPIService.getCompanyEmptySites(req.user.company_id);
  }

  // return all the sites ids of the user's company
  @UseGuards(JwtAuthGuard)
  @Get('getCompanySitesIds')
  async getCompanySitesIds(
    @Request() req): Promise<any[]> {
    this.logger.log('getCompanySitesIds');
    return this.appAPIService.getCompanySitesIds(req.user.company_id);
  }

  // create new site
  @UseGuards(JwtAuthGuard)
  @Put('createSite')
  async createSite(
    @Request() req,
    @Body() body) {
    this.logger.log('createSite');
    return this.appAPIService.createSite(req.user.company_id, body.name, body.long, body.lat, body.address, body.directions, body.mapVision_id);
  }

  // edit site's info
  @UseGuards(JwtAuthGuard)
  @Put('editSite')
  async editSite(
    @Body() body) {
    this.logger.log('editSite');
    return this.appAPIService.editSite(body.site_id, body.new_name, body.new_address, body.new_directions);
  }

  // return site editable info
  @UseGuards(JwtAuthGuard)
  @Post('getSiteEditableInfo')
  async getSiteEditableInfo(
    @Body() body) {
    this.logger.log('getSiteEditableInfo');
    return this.appAPIService.getSiteEditableInfo(body.site_id);
  }

  // return collection obj according to id
  @UseGuards(JwtAuthGuard)
  @Post('getCollection')
  async getCollection(
    @Body() body: any) {
    this.logger.log('getCollection');
    return this.appAPIService.getCollection(body.id);
  }

  // return run name according to site id
  @UseGuards(JwtAuthGuard)
  @Post('getRun')
  async getRun(
    @Request() req,
    @Body() body: any) {
    this.logger.log('getRun');
    return this.appAPIService.getRun(req.user.company_id, body.site_id);
  }

  // create or update run
  @UseGuards(JwtAuthGuard)
  @Put('putRun')
  async putRun(
    @Request() req,
    @Body() body: any) {
    this.logger.log('putRun');
    return this.appAPIService.putRun(req.user.company_id, body);
  }

  // create or update run
  @UseGuards(JwtAuthGuard)
  @Put('createRule')
  async createRule(
    @Request() req,
    @Body() body: any) {
    this.logger.log('createRule');
    return this.appAPIService.createRule(req.user.company_id, body.userName, body.kindName, body.ruleName, body.siteId, body.amount);
  }

  //update runs sites
  @UseGuards(JwtAuthGuard)
  @Put('addSiteToRun')
  async addSiteToRun(
    @Request() req,
    @Body() body: any) {
    this.logger.log('addSiteToRun');
    return this.appAPIService.addSiteToRun(req.user.company_id, body);
  }

  //update runs sites
  @UseGuards(JwtAuthGuard)
  @Put('deleteSite')
  async deleteSite(
    @Request() req,
    @Body() body: any) {
    this.logger.log('deleteSite');
    return this.appAPIService.deleteSite(req.user.company_id, body.site_id);
  }


  //update runs sites
  @UseGuards(JwtAuthGuard)
  @Put('removeSiteFromRun')
  async removeSiteFromRun(
    @Request() req,
    @Body() body: any) {
    this.logger.log('removeSiteFromRun');
    return this.appAPIService.removeSiteFromRun(req.user.company_id, body);
  }

  // return purpose according to collection id
  @UseGuards(JwtAuthGuard)
  @Post('getPurpose')
  async getPurpose(
    @Body() body: any) {
    this.logger.log('getPurpose');
    return this.appAPIService.getPurpose(body.coll_id);
  }

  // return site address and directions according to name
  @UseGuards(JwtAuthGuard)
  @Post('getSiteAddressAndDirections')
  async getSiteAddressAndDirections(
    @Request() req,
    @Body() body: any) {
    this.logger.log('getSiteAddressAndDirections');
    return this.appAPIService.getSiteAddressAndDirections(req.user.company_id, body.name);
  }

  // return the location of the company
  @UseGuards(JwtAuthGuard)
  @Get('getCompanyLocation')
  async getCompanyLocation(
    @Request() req): Promise<any> {
    this.logger.log('getCompanyLocation');
    return this.appAPIService.getCompanyLocation(req.user.company_id);
  }

  // return interested insects of the company
  @UseGuards(JwtAuthGuard)
  @Get('InterestedInsects')
  async getInterestedInsects(
    @Request() req) {
    this.logger.log('InterestedInsects');
    return this.appAPIService.getInterestedInsects(req.user.company_id);
  }

  // return insect from the collections according to the filter params
  @UseGuards(JwtAuthGuard)
  @Post('GetInsect')
  async getInsect(
    @Request() req,
    @Body() filterParamsTotal: FilterParamsDto): Promise<GetInsectDto | undefined> {
    this.logger.log('GetInsect');
    return this.appAPIService.getInsect(req.user.username, filterParamsTotal);
  }

  // Add manually uploaded insects to trapEvent
  @UseGuards(JwtAuthGuard)
  @Post('AddManualInsects')
  async addManualInsects(
    @Request() req,
    @Body() body: any) {
    this.logger.log('AddManualInsects');
    return this.appAPIService.addManualInsects(req.user.username, body.insectsList, body.trapEvent_id, body.pickupDate);
  }

  // add a new insect identification
  @UseGuards(JwtAuthGuard)
  @Post('AddIdentification')
  async addIdentification(
    @Request() req,
    @Query('insect_id') insect_id: String,
    @Body() createIdentificationDto: CreateIdentificationDto) {
    this.logger.log('AddIdentification');
    return this.appAPIService.addIdentification(req.user.username, insect_id, createIdentificationDto);
  }

  // return the notValid object according to id
  @UseGuards(JwtAuthGuard)
  @Post('getNotValid')
  async getNotValid(
    @Body() not_valid_id: String) {
    this.logger.log('getNotValid');
    return this.appAPIService.getNotValid(not_valid_id);
  }

  // add a new insect verification
  @UseGuards(JwtAuthGuard)
  @Post('AddVerification')
  async addVerification(
    @Request() req,
    @Query('insect_id') insect_id: String) {
    this.logger.log('AddVerification');
    return this.appAPIService.addVerification(req.user.username, insect_id);
  }

  // add verifications to all the isnects at the given collections 
  @UseGuards(JwtAuthGuard)
  @Post('AddVerificationAll')
  async addVerificationAll(
    @Request() req,
    @Body() filterParamsTotal: FilterParamsDto) {
    this.logger.log('AddVerificationAll');
    return this.appAPIService.addVerificationAll(req.user.username, filterParamsTotal);
  }

  // check if ther are unverified at the chosen collections 
  @UseGuards(JwtAuthGuard)
  @Post('CollectionsContainsUnverified')
  async collectionsContainsUnverified(
    @Body() body: any) {
    this.logger.log('CollectionsContainsUnverified');
    return this.appAPIService.collectionsContainsUnverified(body.collections_ids);
  }

  // add colection
  @UseGuards(JwtAuthGuard)
  @Post('AddCollection')
  async addCollection(
    @Request() req,
    @Body() createCollectionDto: CollectionDto) {
    this.logger.log('AddCollection');
    return this.appAPIService.AddCollection(req.user.company_id, createCollectionDto);
  }

  // return total number of insects left to identify
  @UseGuards(JwtAuthGuard)
  @Get('LeftToIdentify')
  async getNumberOfInsectsLeftToIdentify(
    @Request() req) {
    this.logger.log('LeftToIdentify');
    return this.appAPIService.getNumberOfInsectsLeftToIdentify(req.user.company_id);
  }

  // return total number of insects with second opinion identification
  @UseGuards(JwtAuthGuard)
  @Get('SecondOpinions')
  async getNumberOfSecondOpinions(
    @Request() req) {
    this.logger.log('SecondOpinions');
    return this.appAPIService.getNumberOfSecondOpinions(req.user.company_id);
  }

  // return according to collections ids list of the number of insects left to identify
  @UseGuards(JwtAuthGuard)
  @Post('NumberOfInsectsToTag')
  async getNumberOfInsectsToTag(
    @Body() body: any) {
    this.logger.log('NumberOfInsectsToTag');
    return this.appAPIService.getNumberOfInsectsToTag(body.collections_ids);
  }

  // return according to collections ids list and filter params the number of insects at the chosen collections
  @UseGuards(JwtAuthGuard)
  @Post('NumberOfInsects')
  async getNumberOfInsects(
    @Body() filterParams: FilterParamsDto) {
    this.logger.log('NumberOfInsects');
    return this.appAPIService.getNumberOfInsects(filterParams);
  }

  // return home page drop down lists values
  @UseGuards(JwtAuthGuard)
  @Post('HomePDropdownValues')
  async getHomePDropdownValues(
    @Request() req,
    @Body() body: any) {
    this.logger.log('HomePDropdownValues');
    return this.appAPIService.getHomePDropdownValues(req.user.company_id, body.filterParams, body.page, body.pageSize);
  }

  // return tha company's runs
  @UseGuards(JwtAuthGuard)
  @Get('getCompanyRuns')
  async getCompanyRuns(
    @Request() req) {
    this.logger.log('getCompanyRuns');
    return this.appAPIService.getCompanyRuns(req.user.company_id);
  }

  // return tha company's runs
  @UseGuards(JwtAuthGuard)
  @Get('getAllCompanyRuns')
  async getAllCompanyRuns(
    @Request() req) {
    this.logger.log('getAllCompanyRuns');
    return this.appAPIService.getAllCompanyRuns(req.user.company_id);
  }

  // return the company's pooling and vials Ids
  @UseGuards(JwtAuthGuard)
  @Get('getCompanyPooling')
  async getCompanyPooling(
    @Request() req) {
    this.logger.log('getCompanyPooling');
    return this.appAPIService.getCompanyPooling(req.user.company_id);
  }
  // return the company's positive vials
  @UseGuards(JwtAuthGuard)
  @Get('getPositivePooling')
  async getPositivePooling(
    @Request() req) {
    this.logger.log('getPositivePooling');
    return this.appAPIService.getPositivePooling(req.user.company_id);
  }

  // return all kinds
  @UseGuards(JwtAuthGuard)
  @Get('getKinds')
  async getKinds(
    @Request() req) {
    this.logger.log('getKinds');
    return this.appAPIService.getKinds(req.user.company_id);
  }

  // return genus of a given species
  @UseGuards(JwtAuthGuard)
  @Post('getGenusOfSpecies')
  async getGenusOfSpecies(
    @Body() body: any) {
    this.logger.log('getGenusOfSpecies');
    return this.appAPIService.getGenusOfSpecies(body.species);
  }

  // return all kinds of collection devided according to traps and their amount
  @UseGuards(JwtAuthGuard)
  @Post('getCollectionInsectsInfo')
  async getCollectionInsectsInfo(
    @Body() body: any) {
    this.logger.log('getCollectionInsectsInfo');
    return this.appAPIService.getCollectionInsectsInfo(body.collection_id);
  }

  //returns the virus detected in the collection
  @UseGuards(JwtAuthGuard)
  @Post('getCollectionVirus')
  async getCollectionVirus(
    @Body() body: any) {
    this.logger.log('getCollectionVirus');
    return this.appAPIService.getCollectionVirus(body.collection_id);
  }

  //returns the virus and collection id detected in the site
  @UseGuards(JwtAuthGuard)
  @Post('getSiteViruses')
  async getSiteViruses(
    @Body() body: any) {
    this.logger.log('getSiteViruses');
    return this.appAPIService.getSiteViruses(body.site_id);
  }

  // return all kinds exits
  @UseGuards(JwtAuthGuard)
  @Get('getAllKinds')
  async getAllKinds() {
    this.logger.log('getAllKinds');
    return this.appAPIService.getAllKinds();
  }

  // return all kinds names
  @UseGuards(JwtAuthGuard)
  @Get('getAllKindsNames')
  async getAllKindsNames() {
    this.logger.log('getAllKindsNames');
    return this.appAPIService.getAllKindsNames();
  }

  // return guide pictures
  @UseGuards(JwtAuthGuard)
  @Get('getGuidePictures')
  async getGuidePictures() {
    this.logger.log('getGuidePictures');
    return this.appAPIService.getGuidePictures();
  }

  // return the amount of kind at the given collection
  @UseGuards(JwtAuthGuard)
  @Post('getKindsAmount')
  async getKindsAmount(
    @Body() body: any) {
    this.logger.log('getKindsAmount');
    return this.appAPIService.getKindsAmount(body.collection_id, body.interestedKinds);
  }

  // get list of species matching the genus 
  @UseGuards(JwtAuthGuard)
  @Post('getAdjustedSpeciesList')
  async getAdjustedSpeciesList(
    @Request() req,
    @Body() body: any) {
    this.logger.log('getAdjustedSpeciesList');
    return this.appAPIService.getAdjustedSpeciesList(req.user.company_id, body.genus);
  }

  // get list of genus matching the species 
  @UseGuards(JwtAuthGuard)
  @Post('getAdjustedGenusList')
  async getAdjustedGenusList(
    @Request() req,
    @Body() body: any) {
    this.logger.log('getAdjustedGenusList');
    return this.appAPIService.getAdjustedGenusList(req.user.company_id, body.species);
  }

  // get list of the company runs according to type 
  // @UseGuards(JwtAuthGuard)
  // @Post('getAdjustedRunsList')
  // async getAdjustedRunsList(
  //   @Request() req,
  //   @Body() body: any) {
  //   this.logger.log('getAdjustedRunsList');
  //   return this.appAPIService.getAdjustedRunsList(req.user.company_id, body.type);
  // }

  // free the assigned insects of the logged in user
  @UseGuards(JwtAuthGuard)
  @Post('FreeUserAssigned')
  async freeUserAssigned(
    @Request() req) {
    this.logger.log('FreeUserAssigned');
    return this.appAPIService.freeUserAssigned(req.user.username);
  }

  @UseGuards(JwtAuthGuard)
  @Post('connectUserToCollection')
  async connectUserToCollection(
    @Request() req,
    @Body() body: any) {
    this.logger.log('connectUserToCollection');
    return this.appAPIService.connectUserToCollection(req.user.company_id, body.socket, body.collection_id, body.userName);
  }

  @UseGuards(JwtAuthGuard)
  @Post('removeUserFromCollection')
  async removeUserFromCollection(
    @Request() req,
    @Body() body: any) {
    this.logger.log('removeUserFromCollection');
    return this.appAPIService.removeUserFromCollection(req.user.company_id, body.socket, body.collection_id, body.userName);
  }

  // return the givven run's sites ids list
  @UseGuards(JwtAuthGuard)
  @Post('getRunSites')
  async getRunSites(
    @Request() req,
    @Body() body: any) {
    this.logger.log('getRunSites');
    return this.appAPIService.getRunSites(req.user.company_id, body.run_name);
  }

  // add test to given vial
  @UseGuards(JwtAuthGuard)
  @Post('addTest')
  async addTest(
    @Request() req,
    @Body() body: any) {
    this.logger.log('addTest');
    return this.appAPIService.addTest(req.user.company_id, body.vial_barcode, body.test, body.result);
  }

  // get all insect's tests
  @UseGuards(JwtAuthGuard)
  @Post('getTests')
  async getTests(
    @Request() req,
    @Body() body: any) {
    this.logger.log('getTests');
    return this.appAPIService.getTests(req.user.company_id, body.pool_obj);
  }

  // return true if the collection contain the insect
  @UseGuards(JwtAuthGuard)
  @Post('insectAtCollection')
  async insectAtCollection(
    @Body() body: any) {
    this.logger.log('insectAtCollection');
    return this.appAPIService.insectAtCollection(body.collections_ids, body.insect_id);
  }

  // returns traps according to type
  @UseGuards(JwtAuthGuard)
  @Get('getCompanyTrapsAndLures')
  async getCompanyTrapsAndLures(
    @Request() req) {
    this.logger.log('getCompanyTrapsAndLures');
    return this.appAPIService.getCompanyTrapsAndLures(req.user.company_id);
  }

  // returns traps event IDs according to collection_id
  @UseGuards(JwtAuthGuard)
  @Post('getTrapsEventsIdsOfCollection')
  async getTrapsEventsIdsOfCollection(
    @Body() body: any) {
    this.logger.log('getTrapsEventsIdsOfCollection');
    return this.appAPIService.getTrapsEventsIdsOfCollection(body.collection_id);
  }

  // returns traps event IDs of company
  @UseGuards(JwtAuthGuard)
  @Get('getTrapsEventsIdsOfCompany')
  async getTrapsEventsIdsOfCompany(
    @Request() req) {
    this.logger.log('getTrapsEventsIdsOfCompany');
    return this.appAPIService.getTrapsEventsIdsOfCompany(req.user.company_id);
  }

  // return true if the user identified insect/s at the collection
  @UseGuards(JwtAuthGuard)
  @Post('userIdentifiedCollection')
  async userIdentifiedCollection(
    @Body() body: any) {
    this.logger.log('userIdentifiedCollection');
    return this.appAPIService.userIdentifiedCollection(body.collections_ids, body.user);
  }

  // updates the tests array and deletes the specific vial
  @UseGuards(JwtAuthGuard)
  @Put('deleteTestFromVial')
  async deleteTestFromVial(
    @Body() body: any) {
    this.logger.log('deleteTestFromVial');
    return this.appAPIService.deleteTestFromVial(body.vial_id, body.virus, body.result);
  }

  // return collection id of the virus detected
  @UseGuards(JwtAuthGuard)
  @Post('getCollectionIdFromVial')
  async getCollectionIdFromVial(
    @Body() body: any) {
    this.logger.log('getCollectionIdFromVial');
    return this.appAPIService.getCollectionIdFromVial(body.vial_id);
  }

  // return collection id according to insect id
  @UseGuards(JwtAuthGuard)
  @Post('GetCollectionOfInsect')
  async getCollectionOfInsect(
    @Body() body: any) {
    this.logger.log('GetCollectionOfInsect');
    return this.appAPIService.getCollectionOfInsect(body.insect_id);
  }

  @UseGuards(JwtAuthGuard)
  @Post('getFilteredCollectionData')
  async getFilteredCollectionData(
    @Body() body: any) {
    this.logger.log('getFilteredCollectionData');
    return this.appAPIService.getFilteredCollectionData(body.collections, body.filter_params);
  }

  @UseGuards(JwtAuthGuard)
  @Post('updateSocket')
  async UpdateSocket(
    @Request() req,
    @Body() body: any) {
    this.logger.log('updateSocket');
    return this.appAPIService.updateSocket(req.user.company_id, body.oldId, body.newId);
  }

  // get users messages
  @UseGuards(JwtAuthGuard)
  @Post('getUserMessages')
  async getUserMessages(
    @Request() req,
    @Body() body: any) {
    this.logger.log('getUserMessages');
    return this.appAPIService.getUserMessages(req.user.company_id, body.userName);
  }

  // delete users messages
  @UseGuards(JwtAuthGuard)
  @Post('deleteUserMessages')
  async deleteUserMessages(
    @Request() req,
    @Body() body: any) {
    this.logger.log('deleteUserMessages');
    return this.appAPIService.deleteUserMessages(req.user.company_id, body.userName);
  }

  // delete rule
  @UseGuards(JwtAuthGuard)
  @Put('deleteRule')
  async deleteRule(
    @Request() req,
    @Body() body: any) {
    this.logger.log('deleteRule');
    return this.appAPIService.deleteRule(req.user.company_id, body.ruleName, body.userName);
  }

  // upload csv with landmarks
  @UseGuards(JwtAuthGuard)
  @Post('createLandmarks')
  async createLandmarks(
    @Request() req,
    @Body() body: any) {
    this.logger.log('createLandmarks');
    return this.appAPIService.createLandmarks(req.user.company_id, body.areaName, body.users, body.landmarkes, body.day);
  }

  // get areas of spreading
  @UseGuards(JwtAuthGuard)
  @Get('getAreas')
  async getAreas(
    @Request() req) {
    this.logger.log('getAreas');
    return this.appAPIService.getAreas(req.user.company_id);
  }

  // get landmarks  from area
  @UseGuards(JwtAuthGuard)
  @Post('getLandmarks')
  async getLandmarks(
    @Request() req,
    @Body() body: any) {
    this.logger.log('getLandmarks');
    return this.appAPIService.getLandmarks(req.user.company_id, body.areaName);
  }

  // set Area Name
  @UseGuards(JwtAuthGuard)
  @Post('editAreaName')
  async editAreaName(
    @Request() req,
    @Body() body: any) {
    this.logger.log('editAreaName');
    return this.appAPIService.editAreaName(req.user.company_id, body.prevName, body.newName);
  }

  // add spread data
  @UseGuards(JwtAuthGuard)
  @Post('addSpreadData')
  async addSpreadData(
    @Request() req,
    @Body() body: any) {
    this.logger.log('addSpreadData');
    return this.appAPIService.addSpreadData(req.user.company_id, body.checkedLandmarks, body.date);
  }

  // add notes data
  @UseGuards(JwtAuthGuard)
  @Post('addNotesToClient')
  async addNotesToClient(
    @Request() req,
    @Body() body: any) {
    this.logger.log('addNotesToClient');
    return this.appAPIService.addNotesToClient(req.user.company_id, body.landmark, body.notes);
  }

  // get company users
  @UseGuards(JwtAuthGuard)
  @Get('getCompanyUsers')
  async getCompanyUsers(
    @Request() req) {
    this.logger.log('getCompanyUsers');
    return this.appAPIService.getCompanyUsers(req.user.company_id);
  }

  // create spread
  @UseGuards(JwtAuthGuard)
  @Post('createNewSpread')
  async createNewSpread(
    @Request() req,
    @Body() body: any) {
    this.logger.log('createNewSpread');
    return this.appAPIService.createNewSpread(req.user.company_id, body.areaId, body.spreadsArray, body.areaName, body.date, body.userName);
  }

  // create new landmark
  @UseGuards(JwtAuthGuard)
  @Post('createNewLandmark')
  async createNewLandmark(
    @Request() req,
    @Body() body: any) {
    this.logger.log('createNewLandmark');
    return this.appAPIService.createNewLandmark(req.user.company_id, body.areaName, body.lat, body.lng, body.address, body.clientName, body.rollsNumber, body.daySelected, body.email, body.cellPhone, body.notes, body.courier, body.inside, body.startTime, body.endTime);
  }

  // return days availble for landmark
  @UseGuards(JwtAuthGuard)
  @Post('showAllDays')
  async showAllDays(
    @Request() req,
    @Body() body: any) {
    this.logger.log('showAllDays');
    return this.appAPIService.showAllDays(req.user.company_id, body.areaName, body.lat, body.lng);
  }

  // add landmark to user spread
  @UseGuards(JwtAuthGuard)
  @Post('addLandmarkToSpread')
  async addLandmarkToSpread(
    @Request() req,
    @Body() body: any) {
    this.logger.log('addLandmarkToSpread');
    return this.appAPIService.addLandmarkToSpread(req.user.company_id, body.spreadId, body.landmark, body.areaName);
  }

  // disactivate selected landmarks
  @UseGuards(JwtAuthGuard)
  @Post('disActiveLandmarks')
  async disActiveLandmarks(
    @Request() req,
    @Body() body: any) {
    this.logger.log('disActiveLandmarks');
    return this.appAPIService.disActiveLandmarks(req.user.company_id, body.areaId, body.landmarks);
  }
  // get all landmarks data
  @UseGuards(JwtAuthGuard)
  @Post('getDataOnAllLandmarks')
  async getDataOnAllLandmarks(
    @Request() req,
    @Body() body: any) {
    this.logger.log('getDataOnAllLandmarks');
    return this.appAPIService.getDataOnAllLandmarks(req.user.company_id, body.startDate, body.endDate);
  }

  @Post('sendEmail')
  async sendEmail(
    @Body() body: any) {
    return await this.appAPIService.sendEmailToMultipleRecipients();
  }

  // move selected landmarks
  @UseGuards(JwtAuthGuard)
  @Post('moveLandmarks')
  async moveLandmarks(
    @Request() req,
    @Body() body: any) {
    this.logger.log('moveLandmarks');
    return this.appAPIService.moveLandmarks(req.user.company_id, body.checkedLandmarks, body.prevArea, body.area, body.user, body.date);
  }

  // get landmark by id
  @UseGuards(JwtAuthGuard)
  @Post('getLandmarkById')
  async getLandmarkById(
    @Request() req,
    @Body() body: any) {
    this.logger.log('getLandmarkById');
    return this.appAPIService.getLandmarkById(req.user.company_id, body.landmarkId);
  }

  // edit client
  @UseGuards(JwtAuthGuard)
  @Post('editClient')
  async editClient(
    @Request() req,
    @Body() body: any) {
    this.logger.log('editClient');
    return this.appAPIService.editClient(req.user.company_id, body.landmarkId, body.clientName, body.rollsNumber, body.email, body.cellPhone, body.notes, body.inside, body.startTime, body.endTime);
  }

  @Post('fillSurvey')
  async fillSurvey(
    @Body() body: any) {
    this.logger.log('fillSurvey');
    return this.appAPIService.fillSurvey(body.userId, body.rating, body.comments);
  }

  @UseGuards(JwtAuthGuard)
  @Get('getSurveyResult')
  async getSurveyResult(
    @Request() req) {
    this.logger.log('getSurveyResult');
    return this.appAPIService.getSurveyResult(req.user.company_id);
  }

  @Post('solveVRPTW')
  async solveVRPTW(
    @Body() body: any) {
    this.logger.log('solveVRPTW');
    return this.appAPIService.solveVRPTW(body);
  }

}