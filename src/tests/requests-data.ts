export const filter_params1 = {
    filterParams: {
      barcode: null,
      insectID: "F2EzlC",
      site_name: null,
      site_id: null,
      position: null,
      genus: null,
      species: null,
      taggedBy: null
    }
  };
  export const filter_params2 = {
    filterParams: {
      barcode: null,
      insectID: null,
      site_name: null,
      site_id: null,
      position: null,
      genus: "Culiseta",
      species: null,
      taggedBy: "machine"
    }
  };
  export const filter_params3 = {
    filterParams: {
      barcode: null,
      insectID: null,
      site_name: "Coyote pond",
      site_id: null,
      position: null,
      genus: null,
      species: null,
      taggedBy: null
    }
  };
  export const filter_params_insects1 = {
    insectID: null,
    viewUnidentified: true,
    species: null,
    genus: null,
    taggedBy: "",
    secondOpinion: false,
    collections_ids: ["U6KOHp"],
    prev_insects: [],
  };
  export const filter_params_insects2 = {
    insectID: null,
    viewUnidentified: false,
    species: null,
    genus: "Culiseta",
    taggedBy: "",
    secondOpinion: false,
    collections_ids: ["U6KOHp", "3jVz7O", "8C9x0H"],
    prev_insects: [],
  };
  export const filter_params_insects3 = {
    insectID: null,
    viewUnidentified: false,
    species: null,
    genus: "Culiseta",
    taggedBy: "",
    secondOpinion: false,
    collections_ids: ["U6KOHp", "3jVz7O", "8C9x0H"],
    prev_insects: ['F2EzlC'],
  };
  export const afterCahnges={
    collection_id:"8C9x0H", 
    site_id:"test",
    collector:null, 
    placement_date:null, 
    pickup_date:null, 
    purpose:null
  };
  