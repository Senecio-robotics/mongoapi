import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { AppAPIModule } from '../AppAPI/appAPI.module';
import { AuthModule } from '../auth/auth.module';
import { MongooseModule } from '@nestjs/mongoose';
import { updateIfCurrentPlugin } from 'mongoose-update-if-current';
import { UsersModule } from '../users/users.module';
import * as response from './expected-response';
import * as data from './requests-data';

describe('AppAPITests', () => {
  let app: INestApplication;
  let token = 'Bearer ';

  beforeAll(async () => {
    jest.useFakeTimers()
    jest.setTimeout(100000)
    const moduleRef = await Test.createTestingModule({
      imports: [
        AppAPIModule,
        AuthModule,
        UsersModule,
        MongooseModule.forRoot(
          'mongodb+srv://app_user:12345@senecioroboticsdb.dizra.mongodb.net/TestingDataBase?retryWrites=true&w=majority', {
          connectionFactory: (connection) => {
            connection.plugin(updateIfCurrentPlugin);
            return connection;
          }
        })
      ],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
    token += (await request(app.getHttpServer())
      .post('/auth/login')
      .send({
        "username": "admin", "password": "shenhav2021",
        "company_id": "60dec715df975161f49f848e", "socket_id": "0"
      }))
      .body.access_token;
  });

  // gets the sites of specific company
  it(`/GET getCompanySites`, () => {
    return request(app.getHttpServer())
      .get('/api/getCompanySites')
      .set({ Authorization: token })
      .expect(200)
      .expect(response.resuletCompanySites);
  });

  // gets the company collections according to the user who is logged in(admin)
  it('/GET getCompanyCollections', () => {
    return request(app.getHttpServer())
      .get('/api/getCompanyCollections')
      .set({ Authorization: token })
      .expect(200)
      .expect(response.resultCompanyCollections);
  });

  // gets the values according to spesific insect id
  it('/POST getHomePDropdownValues by insect id', () => {
    return request(app.getHttpServer())
      .post('/api/HomePDropdownValues')
      .send(data.filter_params1)
      .set({ Authorization: token })
      .expect(201)
      .expect(response.values1);
  });

  // gets the values according to spesific genus and insects who were tagged by the machine
  it('/POST getHomePDropdownValues by genus, tagged by', () => {
    return request(app.getHttpServer())
      .post('/api/HomePDropdownValues')
      .send(data.filter_params2)
      .set({ Authorization: token })
      .expect(201)
      .expect(response.values2);
  });

  // gets the values according to spesific site name
  it('/POST getHomePDropdownValues site name', () => {
    return request(app.getHttpServer())
      .post('/api/HomePDropdownValues')
      .send(data.filter_params3)
      .set({ Authorization: token })
      .expect(201)
      .expect(response.values3);
  });

  // gets insects that does not have tagger user name and returns one of them from collection id: "U6KOHp"
  it('/POST GetInsect from specific collection', () => {
    return request(app.getHttpServer())
      .post('/api/GetInsect')
      .send(data.filter_params_insects1)
      .set({ Authorization: token })
      .expect(201)
      .expect(response.getInsectResult1);
  });

  // // gets a insect from all collections with genus=culiseta 
  // it('/POST GetInsect', () => {
  //   return request(app.getHttpServer())
  //     .post('/api/GetInsect')
  //     .send(data.filter_params_insects2)
  //     .set({ Authorization: token })
  //     .expect(201)
  //     .expect(response.getInsectResult1);
  // });

  // // get insect that is unidentified and not the one we alredy identified : different id from "F2EzlC"
  // it('/POST GetInsect', () => {
  //   return request(app.getHttpServer())
  //     .post('/api/GetInsect')
  //     .send(data.filter_params_insects3)
  //     .set({ Authorization: token })
  //     .expect(201)
  //     .expect(response.getInsectResult2);
  // });

  // // a request to free the username, after changing it to admin
  // it('/POST free username', () => {
  //   return request(app.getHttpServer())
  //     .post('/api/FreeUserAssigned')
  //     .set({ Authorization: token })
  //     .expect(201);
  // });

  afterAll(async () => {
    jest.clearAllTimers();
    await app.close();
  });
});
