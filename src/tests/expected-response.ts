export const values1 = {
  sitesIds: ['uXGHju'],
  sitesNames: ['Sacramento zoo'],
  collectionsIds: ['U6KOHp'],
  interest_species: ['incidens'],
  interest_genus: ['Culiseta'],
  insectsIds: ['F2EzlC'],
  usersNames: ['machine'],
  runsName: ['Run 1', 'Run 2', 'Run 3']
};
export const values2 = {
  sitesIds: [ 'uXGHju' ],
  sitesNames: [ 'Sacramento zoo' ],
  collectionsIds: [ 'U6KOHp' ],
  interest_species: [ 'incidens' ],
  interest_genus: [ 'Culiseta' ],
  insectsIds: [ 'F2EzlC', 'bh8YIj' ],
  usersNames: [ 'machine' ],
  runsName: [ 'Run 1', 'Run 2', 'Run 3' ]
};
export const values3 = {
  sitesIds: [ '02wrUs' ],
  sitesNames: [ 'Coyote pond' ],
  collectionsIds: [],
  interest_species: [],
  interest_genus: [],
  insectsIds: [],
  usersNames: [],
  runsName: [ 'Run 1', 'Run 2', 'Run 3' ]
};
export const getInsectResult1 = {
  insect_id: 'bh8YIj',
  site_name: 'Sacramento zoo',
  site_lat: 38.53985378785777,
  site_long: -121.50503938002358,
  placement_date: '2022-04-27T00:00:00.000Z',
  pickup_date: '2022-04-27T00:00:00.000Z',
  collection_id: 'U6KOHp',
  urls: [
    {
      path: 'https://storage.googleapis.com/arik_upload/ZDg1M2JhYjlmNzFiMDMzMQ_22_02_04__11_46_43_3.jpg',
      focus_num: 1,
      images_scale: 1019
    }
  ],
  identifications: [
    {
      second_opinion: false,
      cant_identify: false,
      tagger_username: 'machine',
      valid: true,
      species: 'incidens',
      genus: 'Culiseta',
      sex: 'Female',
      head: false,
      thorax: false,
      abdomen: false,
      noise: false,
      multiple: false
    }
  ],
  verified: false
};
export const getInsectResult2 = {
  insect_id: 'bh8YIj',
  site_name: 'Sacramento zoo',
  site_lat: 38.53985378785777,
  site_long: -121.50503938002358,
  placement_date: '2022-04-27T00:00:00.000Z',
  pickup_date: '2022-04-27T00:00:00.000Z',
  collection_id: 'U6KOHp',
  urls: [
    {
      path: 'https://storage.googleapis.com/arik_upload/ZDg1M2JhYjlmNzFiMDMzMQ_22_02_04__11_46_43_3.jpg',
      focus_num: 1,
      images_scale: 1019
    }
  ],
  identifications: [
    {
      second_opinion: false,
      cant_identify: false,
      tagger_username: 'machine',
      valid: true,
      species: 'incidens',
      genus: 'Culiseta',
      sex: 'Female',
      head: false,
      thorax: false,
      abdomen: false,
      noise: false
    }
  ],
  verified: false
}

export const resultCompanyNameAndUsername = {
  companyName: 'Senecio robotics',
  username: 'admin'
};
export const resultCompanyCollections = {
  runs: [
    { _id: '1', type: 'weekly', name: 'Run 1', sites_ids:[
      {
        _id: 'BodcEh',
        name: 'Asia mart parking, Santa Rosa',
        longitude: -122.76851992256326,
        latitude: 38.453270874744014,
        address: '2481 Guerneville Rd, Santa Rosa',
        directions: 'Enter the Asia Mart parking, drive to the most north point, near the households',
        company_id: '60dec715df975161f49f848e',
        __v: 0
      }
    ] },
    { _id: '2', type: 'weekly', name: 'Run 2', sites_ids: [ '02wrUs' ] },
    { _id: '3', type: 'daily', name: 'Run 1', sites_ids:     [
      {
        _id: 'BodcEh',
        name: 'Asia mart parking, Santa Rosa',
        longitude: -122.76851992256326,
        latitude: 38.453270874744014,
        address: '2481 Guerneville Rd, Santa Rosa',
        directions: 'Enter the Asia Mart parking, drive to the most north point, near the households',
        company_id: '60dec715df975161f49f848e',
        __v: 0
      }
    ] },
    { _id: '4', type: 'daily', name: 'Run 2', sites_ids: [] },
    { _id: '5', type: 'daily', name: 'Run 3', sites_ids: [] }
  ],
  companyCollections: [
    {
      total: 2,
      percentage: 100,
      second_opinion_percentage: 0,
      site_name: 'Sacramento zoo',
      site_id: 'uXGHju',
      site_lng: -121.50503938002358,
      site_lat: 38.53985378785777,
      genuses: [ 'Culiseta' ],
      species: [ 'incidens' ],
      viruses: [],
      second_opinion: false,
      is_verified: 'No',
      _id: 'U6KOHp',
      company_id: '60dec715df975161f49f848e',
      type: 'weekly',
      state: 2,
      __v: 94,
      collector_username: 'Rebecca F.',
      pickup_date: '2022-04-27T00:00:00.000Z',
      placement_date: '2022-04-27T00:00:00.000Z'
    }
  ]
}

export const resuletCompanySites = [
  {
    _id: 'BodcEh',
    name: 'Asia mart parking, Santa Rosa',
    longitude: -122.76851992256326,
    latitude: 38.453270874744014,
    address: '2481 Guerneville Rd, Santa Rosa',
    directions: 'Enter the Asia Mart parking, drive to the most north point, near the households',
    company_id: '60dec715df975161f49f848e',
    __v: 0
  },
  {
    _id: 'uXGHju',
    name: 'Sacramento zoo',
    longitude: -121.50503938002358,
    latitude: 38.53985378785777,
    address: '15th Ave, western to the zoo',
    directions: null,
    company_id: '60dec715df975161f49f848e',
    __v: 0
  },
  {
    _id: '02wrUs',
    name: 'Coyote pond',
    longitude: -121.25887810398793,
    latitude: 38.84741855838237,
    address: '2543 Old Kenmare Drive, Lincoln',
    directions: null,
    company_id: '60dec715df975161f49f848e',
    __v: 0
  }
];
export const changedCompany = {
  _id: '8C9x0H',
  company_id: '60dec715df975161f49f848e',
  site_id: "test",
  state: 0,
  __v: 0,
  placement_date: '2022-04-12T06:44:46.804Z'
}