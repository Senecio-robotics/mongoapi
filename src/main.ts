import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AppLogger } from './AppLogger';

const config = require('config');
const server_port = config.get('port');

async function bootstrap() {
  console.log("Port = " + server_port);
  let cors = require('cors')
  const app = await NestFactory.create(AppModule, {
    logger: new AppLogger(config.get('log_folder_path'))
  });
  app.use(cors())
  await app.listen(server_port);
}
bootstrap();