import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Insect, InsectDocument } from "../Schemas/insect.schema";
import { InsectUsage, InsectUsageDocument } from "../Schemas/insectUsage.schema";
import { Collection, CollectionDocument } from '../Schemas/collection.schema';
import { Company, CompanyDocument } from "../Schemas/company.schema";
import { Kind, KindDocument } from "../Schemas/kind.schema";
import { Site, SiteDocument } from '../Schemas/site.schema';
import { Users, UsersDocument } from "../Schemas/users.schema";
import { OriginImage, OriginImageDocument } from "../Schemas/originImage.schema";
import { Identification } from "../Schemas/identification.schema";
import ShortUniqueId from "short-unique-id";
import { UserIdentifiedCollection, UserIdentifiedCollectionDocument } from "../Schemas/userIdentifiedCollection.schema";
import { KindCounter } from "../Schemas/kindCounter.schema";
import { TrapEvent, TrapEventDocument } from "../Schemas/trapEvent.schema";
import { customAlphabet } from 'nanoid';

@Injectable()
export class AutomationAPIService {
  constructor(

    // allows actions on the collections table
    @InjectModel(Collection.name)
    private readonly collectionModel: Model<CollectionDocument>,

    // allows actions on the company table
    @InjectModel(Company.name)
    private readonly companyModel: Model<CompanyDocument>,

    // allows actions on the insect table
    @InjectModel(Insect.name)
    private readonly insectModel: Model<InsectDocument>,

    // allows actions on the insect usage table
    @InjectModel(InsectUsage.name)
    private readonly insectUsageModel: Model<InsectUsageDocument>,

    // allows actions on the sites table
    @InjectModel(Site.name)
    private readonly siteModel: Model<SiteDocument>,

    // allows actions on the kinds table
    @InjectModel(Kind.name)
    private readonly kindModel: Model<KindDocument>,

    // allows actions on the users table
    @InjectModel(Users.name)
    private readonly usersModel: Model<UsersDocument>,

    // allows actions on the originImage table
    @InjectModel(OriginImage.name)
    private readonly originImageModel: Model<OriginImageDocument>,

    // allows actions on the usersIdentifiedCollection table
    @InjectModel(UserIdentifiedCollection.name)
    private readonly userIdentifiedCollectionModel: Model<UserIdentifiedCollectionDocument>,

    // allows actions on the trapEvent table
    @InjectModel(TrapEvent.name)
    private readonly trapEventModel: Model<TrapEventDocument>,
  ) { }

  customAlphabetChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
  generateRandomID = customAlphabet(this.customAlphabetChars, 6);

  async updateKindCounters() {
    let trapEvents = await this.trapEventModel.find().exec();
    for (let i = 0; i < trapEvents.length; i++) {
      console.log(`${i + 1}/${trapEvents.length}`);
      const insects: Insect[] = await this.insectModel.find({ trapEvent_id: trapEvents[i]._id }).exec();
      let counters: KindCounter[] = [];
      for (let j = 0; j < insects.length; j++) {
        if (!insects[j].cant_identify && insects[j].identifications.length > 0) {
          const kind_id = insects[j].identifications[insects[j].identifications.length - 1].kind_id;
          if (kind_id === null)
            continue;
          // return KindCounter id according to kind_id
          const new_kind_counter_index = counters.findIndex((kC) => kC.kind_id === kind_id);

          // check if the new kindCounter exists 
          if (new_kind_counter_index != -1)
            counters[new_kind_counter_index].amount += 1;
          else
            counters.push({ _id: (new ShortUniqueId()).randomUUID(), kind_id: kind_id, amount: 1 });
        }
      }
      trapEvents[i].kindsCounter = counters;
      trapEvents[i].markModified("kindsCounter");
      await trapEvents[i].save();
    }
  }

  async updateIdentificaions() {
    let insects = await this.insectModel.find().exec();
    for (let i = 0; i < insects.length; i++) {
      try {
        console.log(`${i + 1}/${insects.length}`);
        let insect = insects[i];
        let identification: Identification = null;
        for (let j = 0; j < insect.identifications.length; j++) {
          identification = insect.identifications[j];
          identification.cant_identify = false;
          identification.second_opinion = false;
          identification.is_fed = false;
        }
        if (insect.identifications.length > 0) {
          if (insect.second_opinion)
            identification.second_opinion = insect.second_opinion;
          if (insect.is_fed)
            identification.is_fed = insect.is_fed;
        }
        else if (insect.second_opinion) {
          let new_identification: Identification = {
            _id: (new ShortUniqueId()).randomUUID(),
            timestamp: new Date(),
            tagger_username: 'arik',
            kind_id: null,
            comment: "",
            second_opinion: true,
            cant_identify: false,
            not_valid_id: null,
            is_fed: false,
          };
          insect.identifications.push(new_identification);
        }
        if (insect.cant_identify) {
          let new_identification: Identification = {
            _id: (new ShortUniqueId()).randomUUID(),
            timestamp: new Date(),
            tagger_username: 'arik',
            kind_id: null,
            comment: "",
            second_opinion: false,
            cant_identify: true,
            not_valid_id: null,
            is_fed: false,
          };
          insect.markModified("identifications");
          await insect.save();
          insect.identifications.push(new_identification);
        }
        insect.markModified("identifications");
        await insect.save();
      }
      catch (e) {
        console.log(`failed modifying insect ${insects[i]._id} with error:`);
        console.log(e);
      }
    }
  }

  async fixInsectusage() {
    const insectUsages = await this.insectUsageModel.find().exec();
    for (let i = 0; i < insectUsages.length; i++) {
      console.log(`${i + 1}/${insectUsages.length}`);
      const insectUsage = insectUsages[i];
      const insect = await this.insectModel.findById(insectUsage.insect_id).exec();
      if (insect == null)
        insectUsage.delete();
    }
  }

  async insertToInsectUsage() {

    // retrieve only unidentified/ second opinions
    let filterExp = {
      $or: [{ $expr: { $eq: [{ $arrayElemAt: ["$identifications.second_opinion", -1] }, true] } }, { identifications: { $size: 0 } },
      { $and: [{ identifications: { $size: 1 } }, { $or: [{ "identifications.sex": "Unidentified" }, { "identifications.species": "Unidentified" }] }] }]
    }
    const insects = await this.insectModel.find(filterExp).exec();
    for (let i = 0; i < insects.length; i++) {
      console.log(`${i + 1}/${insects.length}`);
      let insect = insects[i];
      try {
        const trapEvent = await this.trapEventModel.findById(insect.trapEvent_id).exec();
        // check if this insect already exists at the insect usage modle
        const check = await this.insectUsageModel.findOne({ insect_id: insect._id }).exec();
        if (check === null) {
          let createdInsectUsage;
          if (insect.identifications.length > 0 && insect.identifications[insect.identifications.length - 1].second_opinion) {
            createdInsectUsage = new this.insectUsageModel({ collection_id: trapEvent.collection_id, insect_id: insect._id, username: null, timestamp: null, second_opinion: true });
          }
          else {
            createdInsectUsage = new this.insectUsageModel({ collection_id: trapEvent.collection_id, insect_id: insect._id, username: null, timestamp: null, second_opinion: false });
          }
          createdInsectUsage._id = (new ShortUniqueId()).randomUUID();
          await createdInsectUsage.save();
        }
      } catch {
        console.log(`Failed to insert insect to insectUsage:\n${insect}`);
      }
    }
  }

  async addSecondOpinionToInsectUsage() {
    const insectsUsages = await this.insectUsageModel.find().exec();
    for (let i = 0; i < insectsUsages.length; i++) {
      console.log(`${i + 1}/${insectsUsages.length}`);
      let insectUsage = insectsUsages[i];
      const insect = await this.insectModel.findOne({ _id: insectUsage.insect_id }).exec();
      if (insect.identifications.length > 0 && insect.identifications[insect.identifications.length - 1].second_opinion)
        insectUsage.second_opinion = true;
      else
        insectUsage.second_opinion = false;
      await insectUsage.save();
    }
  }

  async deleteInsects(collection_id) {
    const insectsUsages = await this.insectUsageModel.find({ collection_id: collection_id, second_opinion: false }).exec();
    for (let i = 0; i < insectsUsages.length; i++) {
      console.log(`${i + 1}/${insectsUsages.length}`);
      await this.insectUsageModel.deleteOne({ _id: insectsUsages[i]._id });
    }
  }

  async deleteInsectsOfCollection(collection_id) {
    const insects = await this.insectModel.find({ collection_id: collection_id }).exec();
    for (let i = 0; i < insects.length; i++) {
      console.log(`${i + 1}/${insects.length}`);
      await this.insectModel.deleteOne({ _id: insects[i]._id });
    }
  }

  async addFocusNumber() {
    let insects = await this.insectModel.find().exec();
    for (let i = 0; i < insects.length; i++) {
      console.log(`${i + 1}/${insects.length}`);
      let cropped_images = insects[i].cropped_images;
      for (let j = 0; j < cropped_images.length; j++) {
        if (!cropped_images[j].hasOwnProperty("focuses_number")) {
          cropped_images[j].focuses_number = 1;
          insects[i].markModified('cropped_images');
        }
      }
      insects[i].save();
    }
  }

  async insertToUserIdentifiedCollection() {
    const collections = await this.collectionModel.find().exec();
    for (let i = 0; i < collections.length; i++) {
      console.log(`${i + 1}/${collections.length}`);
      let insects = await this.insectModel.find({ $and: [{ collection_id: collections[i]._id }, { identifications: { $not: { $size: 0 } } }] });
      for (let j = 0; j < insects.length; j++) {
        const trapEvent = await this.trapEventModel.findById(insects[j].trapEvent_id).exec();
        for (let k = 0; k < insects[j].identifications.length; k++) {
          if (await this.userIdentifiedCollectionModel.countDocuments({ username: insects[j].identifications[k].tagger_username, collection_id: trapEvent.collection_id }) === 0) {
            let createdUserIdentifiedCollection = new this.userIdentifiedCollectionModel({ username: insects[j].identifications[k].tagger_username, collection_id: trapEvent.collection_id });
            createdUserIdentifiedCollection._id = (new ShortUniqueId()).randomUUID();
            await createdUserIdentifiedCollection.save();
          }
        }
      }
    }
  }

  async addNotValid() {
    let insects = await this.insectModel.find().exec();
    for (let i = 0; i < insects.length; i++) {
      console.log(`${i + 1}/${insects.length}`);
      let identifications = insects[i].identifications;
      for (let j = 0; j < identifications.length; j++) {
        identifications[j].not_valid_id = null;
        insects[i].markModified('identifications');
      }
      insects[i].save();
    }
  }

  async updateSiteContainCollection() {
    const collections = (await this.collectionModel.find({}, { site_id: 1 }).exec()).map(s => s.site_id);
    console.log(collections)
    for (let i = 0; i < collections.length; i++) {
      console.log(`${i + 1}/${collections.length}`);
      let site = await this.siteModel.findById(collections[i]).exec();
      if (site) {
        site.containCollections = true;
        await site.save();
      }
    }
  }

  async copySites(copy_company_id, new_company_id) {
    const sites = await this.siteModel.find({ company_id: copy_company_id }).exec();
    console.log(sites)
    for (let i = 0; i < sites.length; i++) {
      console.log(`${i + 1}/${sites.length}`);
      let site = new this.siteModel();
      site._id = (new ShortUniqueId()).randomUUID();
      site.company_id = new_company_id;
      site.name = sites[i].name;
      site.longitude = sites[i].longitude;
      site.latitude = sites[i].latitude;
      site.address = sites[i].address;
      site.directions = sites[i].directions;
      site.containCollections = false;
      await site.save();
    }
  }

  async deleteSites() {
    const sites = (await this.siteModel.find({ containCollections: { $exists: false } }).exec()).map(s => s._id);
    console.log(sites)
    for (let i = 0; i < sites.length; i++) {
      console.log(`${i + 1}/${sites.length}`);
      await this.siteModel.deleteOne({ _id: sites[i] })
    }
  }

  async addType() {
    const trapEvents = await this.trapEventModel.find().exec();
    console.log(trapEvents)
    for (let i = 0; i < trapEvents.length; i++) {
      console.log(`${i + 1}/${trapEvents.length}`);
      trapEvents[i].trap_type_id = "646f23ac88d006dd0ad40113";
      trapEvents[i].save();
    }
  }

  async addSiteIdOfMapVision(MapVisionIds: any) {
    for (const siteName in MapVisionIds) {
      const mapVisionId = MapVisionIds[siteName];
      console.log(mapVisionId, siteName)
      const matchingSite = await this.siteModel.find({ name: siteName }).exec();
      for (let i = 0; i < matchingSite.length; i++) {
        matchingSite[i].mapVision_id = mapVisionId;
        matchingSite[i].save();
      }
    }
  }

  async deleteData() {
    const collections = await this.collectionModel.find({ company_id: "65c89d2345878bbb080bd6b1" }).exec();

    for (let i = 0; i < collections.length; i++) {
      const collection = collections[i];
      if (collection._id !== "3D2QK6") {

        // Find trapEventModel documents for the current collection
        const trapEvents = await this.trapEventModel.find({ collection_id: collection._id }).exec();

        // Iterate through trapEventModel documents
        for (let j = 0; j < trapEvents.length; j++) {
          const trapEvent = trapEvents[j];

          // Delete insectModel documents associated with this trapEvent
          await this.insectModel.deleteMany({ trapEvent_id: trapEvent._id }).exec();

          // Delete the current trapEvent
          await this.trapEventModel.deleteOne({ _id: trapEvent._id }).exec();
        }

        // Delete the current collection
        await this.collectionModel.deleteOne({ _id: collection._id }).exec();
      }
    }

  }

  async createSites(sitesList) {
    let company1 = "65c89d2345878bbb080bd6b1";
    let company2 = "65c89b6f45878bbb080bd6b0";
    let allsites1 = [], allsites2 = [];
    for (let i = 0; i < sitesList.length; i++) {
      let site1 = new this.siteModel();
      let site2 = new this.siteModel();
      site1._id = this.generateRandomID();
      site2._id = this.generateRandomID();
      site1.company_id = company1;
      site2.company_id = company2;
      site1.name = sitesList[i][0];
      site2.name = sitesList[i][0];
      site1.latitude = sitesList[i][1];
      site1.longitude = sitesList[i][2];
      site2.latitude = sitesList[i][1];
      site2.longitude = sitesList[i][2];
      site1.address = "";
      site2.address = "";
      site1.containCollections = false;
      site2.containCollections = false;
      allsites1.push(site1._id);
      allsites2.push(site2._id);
      await site1.save();
      await site2.save();
    }
    return { allsites1, allsites2 }
  }

  async addKinds(kindsList: any) {
    const kinds = await this.kindModel.find({}).exec();
    let newkindsId = [];
    let oldKindsId = [];
    for (let i = 0; i < kindsList.length; i++) {
      let insect = kindsList[i].split(' ');
      let exist = kinds.find(k => k.species === insect[1] && k.genus === insect[0]);
      if (exist === undefined) {
        let newKind1 = new this.kindModel(), newKind2 = new this.kindModel(), newKind3 = new this.kindModel();
        newKind1._id = (new ShortUniqueId()).randomUUID();
        newKind1.genus = insect[0];
        newKind1.species = insect[1];
        newKind1.sex = "Male";
        await newKind1.save();
        newKind2._id = (new ShortUniqueId()).randomUUID();
        newKind2.genus = insect[0];
        newKind2.species = insect[1];
        newKind2.sex = "Female";
        await newKind2.save();
        newKind3._id = (new ShortUniqueId()).randomUUID();
        newKind3.genus = insect[0];
        newKind3.species = insect[1];
        newKind3.sex = "Unidentified";
        await newKind3.save();
        newkindsId.push(newKind1._id, newKind2._id, newKind3._id);
      }
      else {
        let prev = oldKindsId;
        let old = kinds.filter(k => k.species === insect[1] && k.genus === insect[0]);
        let mappedId = old.map(k => k._id);
        oldKindsId = prev.concat(mappedId);
      }
    }
    return [...newkindsId, oldKindsId];
  }
}
