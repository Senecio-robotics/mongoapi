import { Body, Controller, Post, Put, Query } from '@nestjs/common';
import { AutomationAPIService } from './AutomationAPI.service';

@Controller('autoapi')
export class AutomationAPIController {
  constructor(private readonly automationAPIService: AutomationAPIService) { }

  // update kind counters
  @Post('updateKindCounters')
  async updateKindCounters() {
    this.automationAPIService.updateKindCounters();
  }

  // update Identificaions
  @Post('updateIdentificaions')
  async updateIdentificaions() {
    this.automationAPIService.updateIdentificaions();
  }

  @Post('fixInsectusage')
  async fixInsectusage() {
    this.automationAPIService.fixInsectusage();
  }

  // add unidentified insects to insect usage 
  @Post('InsertToInsectUsage')
  async insertToInsectUsage() {
    this.automationAPIService.insertToInsectUsage();
  }

  // add second opinion to insects at the insect usage collection
  @Post('AddSecondOpinionToInsectUsage')
  async addSecondOpinionToInsectUsage() {
    this.automationAPIService.addSecondOpinionToInsectUsage();
  }

  // delete insects from insect usage
  @Post('deleteInsectsFromInsectUsage')
  async deleteInsects(
    @Query('collection_id') collection_id: String) {
    return this.automationAPIService.deleteInsects(collection_id);
  }

  // delete collection and the insects inside
  @Post('deleteInsectsOfCollection')
  async deleteCollectionAndInsects(
    @Query('collection_id') collection_id: String) {
    return this.automationAPIService.deleteInsectsOfCollection(collection_id);
  }

  // add number of focuses
  @Post('addFocusNumber')
  async addFocusNumber() {
    this.automationAPIService.addFocusNumber();
  }

  // add identifiers field to collection
  @Post('insertToUserIdentifiedCollection')
  async insertToUserIdentifiedCollection() {
    this.automationAPIService.insertToUserIdentifiedCollection();
  }

  // delete noise and add not_valid_id
  @Post('addNotValid')
  async addNotValid() {
    this.automationAPIService.addNotValid();
  }

  // add containCollections field to sites
  @Post('updateSiteContainCollection')
  async updateSiteContainCollection() {
    this.automationAPIService.updateSiteContainCollection();
  }

  // copy company sites for creating site demo
  @Post('copySites')
  async copySites(
    @Query('copy_company_id') copy_company_id: String,
    @Query('new_company_id') new_company_id: String) {
    return this.automationAPIService.copySites(copy_company_id, new_company_id);
  }

  // delete all sites that doesn't contain containCollections field
  @Post('deleteSites')
  async deleteSites() {
    return this.automationAPIService.deleteSites();
  }

  // add type field to trapEvent
  @Post('addType')
  async addType() {
    return this.automationAPIService.addType();
  }

  // add site id of map vision
  @Put('addSiteIdOfMapVision')
  async addSiteIdOfMapVision(
    @Body() body) {
    return this.automationAPIService.addSiteIdOfMapVision(body);
  }

  @Post('deleteData')
  async deleteData() {
    return this.automationAPIService.deleteData();
  }

  @Post('createSites')
  async createSites(
    @Body() body) {
    return this.automationAPIService.createSites(body);
  }


  // add kinds
  @Post('addKinds')
  async addKinds(
    @Body() body) {
    return this.automationAPIService.addKinds(body);
  }
}
