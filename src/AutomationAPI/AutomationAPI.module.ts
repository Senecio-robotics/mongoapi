import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AutomationAPIController } from './AutomationAPI.controller';
import { AutomationAPIService } from './AutomationAPI.service';
import { Collection, CollectionSchema } from '../Schemas/collection.schema';
import { Company, CompanySchema } from '../Schemas/company.schema';
import { Insect, InsectSchema } from '../Schemas/insect.schema';
import { InsectUsage, InsectUsageSchema } from '../Schemas/insectUsage.schema';
import { Site, SiteSchema } from '../Schemas/site.schema';
import { Kind, KindSchema } from '../Schemas/kind.schema';
import { Users, UsersSchema } from '../Schemas/users.schema';
import { OriginImage, OriginImageSchema } from '../Schemas/originImage.schema';
import { UserIdentifiedCollection, UserIdentifiedCollectionSchema } from '../Schemas/userIdentifiedCollection.schema';
import { TrapEvent, TrapEventSchema } from '../Schemas/trapEvent.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Collection.name, schema: CollectionSchema },
    ]),
    MongooseModule.forFeature([
      { name: Company.name, schema: CompanySchema },
    ]),
    MongooseModule.forFeature([
      { name: Insect.name, schema: InsectSchema },
    ]),
    MongooseModule.forFeature([
      { name: InsectUsage.name, schema: InsectUsageSchema },
    ]),
    MongooseModule.forFeature([
      { name: Site.name, schema: SiteSchema },
    ]),
    MongooseModule.forFeature([
      { name: Kind.name, schema: KindSchema },
    ]),
    MongooseModule.forFeature([
      { name: Users.name, schema: UsersSchema },
    ]),
    MongooseModule.forFeature([
      { name: OriginImage.name, schema: OriginImageSchema },
    ]),
    MongooseModule.forFeature([
      { name: UserIdentifiedCollection.name, schema: UserIdentifiedCollectionSchema },
    ]),
    MongooseModule.forFeature([
      { name: TrapEvent.name, schema: TrapEventSchema },
    ])
  ],
  controllers: [AutomationAPIController],
  providers: [AutomationAPIService],
})
export class AutomationAPIModule { }
