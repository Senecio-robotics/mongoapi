import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Users, UsersDocument } from '../Schemas/users.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import ShortUniqueId from "short-unique-id";
import { compareSync, hashSync } from 'bcrypt';

// This should be a real class/interface representing a user entity
export type User = any;

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(Users.name)
    private readonly usersModel: Model<UsersDocument>,
  ) { }

  async findOne(username: string): Promise<User> {
    return this.usersModel.findOne({ username: username }).exec();
  }

  async changePass(username, old_password, new_password) {
    let user = await this.usersModel.findOne({ username: username }).exec();
    if (compareSync(old_password, user.password.toString())) {
      user.password = new String(hashSync(new_password, 10));
      await user.save();
      return "Password changed successfully"
    } else {
      throw new UnauthorizedException('Wrong old password');
    }
  }

  async resetPass(username, new_password) {
    let user = await this.usersModel.findOne({ username: username }).exec();
    user.password = new String(hashSync(new_password, 10));
    await user.save();
    return "Password changed successfully"
  }
}