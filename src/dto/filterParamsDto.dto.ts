export class FilterParamsDto {
    insectID : String; // {_id : "60dec73adf975161f49f8492" }
    viewUnidentified : Boolean; // True => {identifications:{$size: 0}}, False => { identifications: {$not:{$size: 0}}}
    viewUnverified: Boolean;
    
    // KindTable : { species : "albopictus", genus : "Pipiens"}.(map to id)
    // InsectTable : { $expr : { $in : [ { $arrayElemAt: [ "$identifications.kind_id", -1 ] }, ["mmm","mGLT8y", "asdf"] ]}}
    species : String;
    genus : String;

    taggedBy : String; // { $expr : { $eq : [ { $arrayElemAt: [ "$identifications.tagger_username", -1 ] }, "arik" ]}}
    secondOpinion : Boolean; // { $expr : { $eq : [ { $arrayElemAt: [ "$identifications.second_opinion", -1 ] }, true ]}}
    collections_ids : String[]; // {collection_id: { $in: ["60dec717df975161f49f8491", "60df4cbadf975161f49f8a4f"] }}
    prev_insects : String[]; // {_id: { $nin: ["60dec73adf975161f49f8492", "60dec743df975161f49f8494", "002LkQ"] }}

    /*  AND expressions : 
    {$and :
        [
            { $expr : { $in : [ { $arrayElemAt: [ "$identifications.kind_id", -1 ] }, ["mmm","mGLT8y", "asdf"] ]}}, // kinds
            { $expr : { $eq : [ { $arrayElemAt: [ "$identifications.tagger_username", -1 ] }, "Ruth" ]}} // tagger
        ]
    }
    */
  }