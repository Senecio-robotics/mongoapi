import { Identification } from "../Schemas/identification.schema";
import { Kind } from "../Schemas/kind.schema";
import { NotValid } from "../Schemas/notValid.schema";

export class CreateIdentificationDto {
  species: String;
  genus: String;
  sex: String;
  second_opinion: Boolean;
  cant_identify: Boolean;
  isFed: Boolean;
  valid: Boolean;
  head: Boolean;
  thorax: Boolean;
  abdomen: Boolean;
  noise: Boolean;
  multiple: Boolean;
  tagger_username: String;
  timestamp: Date;

  constructor(identification: Identification, kind: Kind = null, not_valid: NotValid = null) {
    this.second_opinion = identification.second_opinion;
    this.cant_identify = identification.cant_identify;
    this.isFed = identification.is_fed;
    this.tagger_username = identification.tagger_username;
    this.timestamp = identification.timestamp;
    this.valid = (not_valid === null)
    if (kind !== null) {
      this.species = kind.species;
      this.genus = kind.genus;
      this.sex = kind.sex;
    }
    else {
      this.species = "";
      this.genus = "";
      this.sex = "";
    }
    if (not_valid !== null) {
      this.head = not_valid.head;
      this.thorax = not_valid.thorax;
      this.abdomen = not_valid.abdomen;
      this.noise = not_valid.noise;
      this.multiple = not_valid.multiple;
    }
    else {
      this.head = false;
      this.thorax = false;
      this.abdomen = false;
      this.noise = false;
      this.multiple = false;
    }
  }
}