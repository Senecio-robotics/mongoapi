export class TagDto {
  tag_URL: String;
  state: String;
  timestamp: Date;
  tagger_username: String;
}