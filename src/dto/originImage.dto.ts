export class OriginImageDto {
  image_URL?: String;
  boundary_boxes?: Number[][];
  insects_ids?: String[];
  }