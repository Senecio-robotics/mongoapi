export class TypeToTrapDto {
    company_id: String;
    trap_type_id: String;
    trap_id: String;
}