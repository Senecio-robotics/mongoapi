import { VialDto } from "./vial.dto";

export class trapEventDto {
    trap_id: String;
    trap_type_id: String;
    lures_ids: String[];
    collection_id: String;
    failures: String[];
}

export class updateTrapEventDto {
    trapEvent_id: String;
    pickup_date: Date;
    collector_username: String;
    failures: String[];
    vial_report: VialDto[];
    company_id: String;
}