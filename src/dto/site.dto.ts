export class SiteDto {
  name: String;
  longitude: number;
  latitude: number;
  address: String;
  directions: String;
  company_id: String;
  containCollections: Boolean;
  calServ_id: String;
  mapVision_id: String;
}