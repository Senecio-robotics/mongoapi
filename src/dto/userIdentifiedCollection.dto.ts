export class UserIdentifiedCollectionDto {
    username: String;
    collection_id: String;
}