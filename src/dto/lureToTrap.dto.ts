export class LureToTrapDto {
    company_id: String;
    trap_id: String;
    lure_id: String;
}