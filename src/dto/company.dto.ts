export class CompanyDto {
  name: String;
  kinds_ids: String[];
  longitude: number;
  latitude: number;
  lures_ids: String[];
  traps_ids: String[];
  types: String[];
}
