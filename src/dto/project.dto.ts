export class ProjectDto {
  name: String;
  areas: String[];
  sites_ids: String[];
  runs_ids: String[];
}