export class CollectionDto {
  traps: TrapsAndLures[];
  placement_date: Date;
  site_id: String;
  purpose: String;
}

class TrapsAndLures {
  trap_name: String;
  trap_check: Boolean;
  lures: Lures[];
  trap_type: String;
}

class Lures {
  lure_name: String;
  lure_check: Boolean;
}