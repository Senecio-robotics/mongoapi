export class VialDto {
    _id: String;
    kind_id?: String;
    sex?: String;
    species?: String;
    genus?: String;
    amount: number;
    company_id?: String;
}