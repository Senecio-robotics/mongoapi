export class KindDto {
  species: String;
  genus: String;
  sex: String;
}