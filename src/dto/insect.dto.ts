export class InsectDto {
    is_fed: Boolean;
    pool_barcode: String;
    trapEvent_id: String;
    verified_timestamp: Date;
    verifier: String;
    second_opinion: Boolean;
    cant_identify: Boolean;
}