export class IdentificationDto {
  timestamp: Date;
  tagger_username: String;
  kind_id?: String;
  species?: String; 
  genus?: String;
  sex?: String;
  comment: String;
  second_opinion: Boolean;
  cant_identify: Boolean;
  is_fed: Boolean;
  not_valid_id: String;
}