import { Collection } from '../Schemas/collection.schema';
import { CroppedImage } from '../Schemas/croppedImage.schema';
import { Insect } from '../Schemas/insect.schema';
import { Site } from '../Schemas/site.schema';
import { CreateIdentificationDto } from './createIdentification.dto';

class LocalCroppedImage {
    path: String;
    focus_num: Number;
    images_scale: Number;
    constructor(image: CroppedImage) {
        this.path = image.picture_URL;
        this.focus_num = image.focuses_number;
        this.images_scale = image.width_in_mm;
    }
}

export class GetInsectDto {
    insect_id: String;
    site_name: String;
    site_lat: number;
    site_long: number;
    placement_date: Date;
    pickup_date: Date;
    collection_id: String;
    urls: LocalCroppedImage[];
    identifications: CreateIdentificationDto[];
    verified: boolean;

    constructor(insect: Insect, identifications: CreateIdentificationDto[], site: Site, collection: Collection) {
        this.insect_id = insect._id;
        this.site_name = site.name;
        this.site_lat = site.latitude;
        this.site_long = site.longitude;
        this.placement_date = collection.placement_date;
        this.pickup_date = collection.pickup_date;
        this.collection_id = collection._id;
        this.urls = insect.cropped_images.map(image => new LocalCroppedImage(image));
        this.identifications = identifications;
        this.verified = (insect.verified_timestamp !== undefined);
    }
}