export class InsectUsageDto {
    collection_id: String;
    insect_id: String;
    username: String;
    timestamp: Date;
    second_opinion: Boolean;
}