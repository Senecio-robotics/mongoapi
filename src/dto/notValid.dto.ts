export class NotValidDto {
    head: Boolean;
    thorax: Boolean;
    abdomen: Boolean;
    noise: Boolean;
    multiple: Boolean;
}