export class LandmarksDto {
    _id: String;
    lat: Number;
    lng: Number;
    address: String;
    clientName:String;
    email: String;
    cellPhone: String;
    rollsNumber:Number;
    active: Boolean;
    inside:Boolean;
    notes: String;
}