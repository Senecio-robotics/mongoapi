export class CroppedImageDto {
  picture_URL: String;
  width_in_mm: Number;
  length_in_mm: Number;
  focuses_number: Number;
  timestamp: Date;
  position: String;
}