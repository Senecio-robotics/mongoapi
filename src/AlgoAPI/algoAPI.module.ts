import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AlgoAPIController } from "./algoAPI.controller";
import { AlgoAPIService } from './algoAPI.service';
import { Insect, InsectSchema } from "src/Schemas/insect.schema";
import { Kind, KindSchema } from "../Schemas/kind.schema";
import { Company, CompanySchema } from "../Schemas/company.schema";
import { Collection, CollectionSchema } from "../Schemas/collection.schema";
import { TrapEvent, TrapEventSchema } from "../Schemas/trapEvent.schema";
import { OriginImage, OriginImageSchema } from "../Schemas/originImage.schema";
import { NotValid, NotValidSchema } from "src/Schemas/notValid.schema";
import { Site, SiteSchema } from '../Schemas/site.schema';
import { Trap, TrapSchema } from '../Schemas/trap.schema';

@Module({
  imports: [

    MongooseModule.forFeature([
      { name: Insect.name, schema: InsectSchema },
    ]),

    MongooseModule.forFeature([
      { name: Kind.name, schema: KindSchema },
    ]),

    MongooseModule.forFeature([
      { name: Company.name, schema: CompanySchema },
    ]),

    MongooseModule.forFeature([
      { name: Collection.name, schema: CollectionSchema },
    ]),

    MongooseModule.forFeature([
      { name: TrapEvent.name, schema: TrapEventSchema },
    ]),

    MongooseModule.forFeature([
      { name: OriginImage.name, schema: OriginImageSchema }
    ]),
    MongooseModule.forFeature([
      { name: NotValid.name, schema: NotValidSchema }
    ]),
    MongooseModule.forFeature([
      { name: Site.name, schema: SiteSchema },
    ]),
    MongooseModule.forFeature([
      { name: Trap.name, schema: TrapSchema },
    ])

  ],
  controllers: [AlgoAPIController],
  providers: [AlgoAPIService],
})
export class AlgoAPIModule { }