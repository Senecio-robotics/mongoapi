import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Collection, CollectionDocument } from "../Schemas/collection.schema";
import { Company, CompanyDocument } from "../Schemas/company.schema";
import { OriginImage, OriginImageDocument } from "../Schemas/originImage.schema";
import { Insect, InsectDocument } from "../Schemas/insect.schema";
import { Kind, KindDocument } from "../Schemas/kind.schema";
import { TrapEvent, TrapEventDocument } from "../Schemas/trapEvent.schema";
import { NotValid, NotValidDocument } from "src/Schemas/notValid.schema";
import { Site, SiteDocument } from '../Schemas/site.schema';
import { Trap, TrapDocument } from '../Schemas/trap.schema';

@Injectable()
export class AlgoAPIService {
  constructor(

    @InjectModel(Insect.name)
    private readonly insectModel: Model<InsectDocument>,

    @InjectModel(Kind.name)
    private readonly kindModel: Model<KindDocument>,

    @InjectModel(Company.name)
    private readonly companyModel: Model<CompanyDocument>,

    @InjectModel(Collection.name)
    private readonly collectionModel: Model<CollectionDocument>,

    @InjectModel(Site.name)
    private readonly siteModel: Model<SiteDocument>,

    @InjectModel(TrapEvent.name)
    private readonly trapEventModel: Model<TrapEventDocument>,

    @InjectModel(Trap.name)
    private readonly trapModel: Model<TrapDocument>,

    @InjectModel(OriginImage.name)
    private readonly originImageModel: Model<OriginImageDocument>,

    @InjectModel(NotValid.name)
    private readonly notValidModel: Model<NotValidDocument>,

  ) { }

  // Creates the query parametrs for the insects and returns it
  async createQueryOnInsects(params: any) {
    let insectsQuery = [];
    const kindIds = await this.getKindIds(params);
    if (kindIds !== null) {
      var kindsQuery = [];
      for (var i = 0; i < kindIds.length; i++) {
        kindsQuery.push({ $expr: { $eq: [{ $arrayElemAt: ["$identifications.kind_id", -1] }, kindIds[i]] } });
      }
      insectsQuery.push({ $or: kindsQuery });
    }
    if (params.hasOwnProperty("trapEvent_id")) {
      insectsQuery.push({ "trapEvent_id": params.trapEvent_id });
    }
    if (params.hasOwnProperty("tagger_username")) {
      insectsQuery.push({ "identifications.tagger_username": params.tagger_username });
    }
    if (params.hasOwnProperty("identificationStartDate") && params.hasOwnProperty("identificationEndDate")) {
      let startDate = new Date(params.identificationStartDate + "+00:00");
      let endDate = new Date(params.identificationEndDate + "+00:00");
      insectsQuery.push({ "identifications.timestamp": { $gte: startDate } }, { "identifications.timestamp": { $lte: endDate } });
    }
    if (params.hasOwnProperty("verifiedStartDate") && params.hasOwnProperty("verifiedEndDate")) {
      let startDate = new Date(params.verifiedStartDate + "+00:00").toISOString();
      let endDate = new Date(params.verifiedEndDate + "+00:00").toISOString();
      insectsQuery.push({ verified_timestamp: { $gte: startDate } }, { verified_timestamp: { $lte: endDate } });
    }
    if (params.hasOwnProperty("companyName") || params.hasOwnProperty("purpose")) {
      const events_id = await this.getTrapEventIds(params);
      if (events_id !== null) {
        insectsQuery.push({ "trapEvent_id": { $in: events_id } });
      }
    }
    if (params.hasOwnProperty("verifier")) {
      var isTrue = (params.verifier === 'true');
      insectsQuery.push({ "verifier": { $exists: isTrue } });
    }
    if (params.hasOwnProperty("cant_identify")) {
      var isTrue = (params.cant_identify === 'true');
      insectsQuery.push({ "identifications.cant_identify": isTrue });
    }
    if (params.hasOwnProperty("valid") && params.valid === "true") {
      const validIds = await this.getValidInsects();
      insectsQuery.push({ _id: { $in: validIds } });
    }
    if (params.hasOwnProperty("valid") && params.valid === "false") {
      const notValidInsectsIds = await this.getNotValidInsects();
      insectsQuery.push({ _id: { $in: notValidInsectsIds } });
    }
    if (params.hasOwnProperty("collection_id")) {
      const events_id = await this.getCollectionInsects(params);
      insectsQuery.push({ "trapEvent_id": { $in: events_id } });
    }
    if (insectsQuery.length > 1) {
      return { $and: insectsQuery };
    }
    else return insectsQuery[0];

  }

  async getCollectionInsects(params) {
    const trapEventIds = (await this.trapEventModel.find({ collection_id: params.collection_id }, { _id: 1 }).exec()).map(trapEvant => trapEvant._id);
    return trapEventIds;
  }

  async getValidInsects() {
    const validIds = (await this.insectModel.find({ $and: [{ "identifications.not_valid_id": null }, { "identifications.kind_id": { $ne: null } }] }, { _id: 1 }).exec()).map(id => id._id);
    return validIds;
  }

  async getNotValidInsects() {
    const notValidInsects = (await this.insectModel.find({ $and: [{ "identifications.not_valid_id": { $exists: true } }, { $expr: { $ne: [{ $arrayElemAt: ["$identifications.not_valid_id", -1] }, null] } }] }, { _id: 1 }).exec()).map(id => id._id);
    return notValidInsects;
  }

  // Finds the kind ids of the insects according to the given parameters of : genus, sex, and species
  async getKindIds(params: any) {
    let kindQuery = [];
    if (params.hasOwnProperty("genus")) {
      kindQuery.push({ "genus": params.genus });
    }
    if (params.hasOwnProperty("species")) {
      kindQuery.push({ "species": params.species });
    }
    if (params.hasOwnProperty("sex")) {
      kindQuery.push({ "sex": params.sex });
    }
    if (kindQuery.length > 0) {
      const kindIds = (await this.kindModel.find({ $and: kindQuery }, { _id: 1 }).exec()).map(kindId => kindId._id)
      return kindIds;
    }
    return null;
  }

  // Returns the trap event ids if there is a parameter of company name/purpose
  async getTrapEventIds(params: any) {
    var company, collectionIds, trapEventIds;
    if (params.hasOwnProperty("companyName") && params.hasOwnProperty("purpose")) {
      company = await this.companyModel.findOne({ name: params.companyName }).exec();
      collectionIds = (await this.collectionModel.find({ $and: [{ company_id: company._id }, { purpose: params.purpose }] }, { _id: 1 }).exec()).map(collId => collId._id);
    }
    else {
      if (params.hasOwnProperty("purpose")) {
        collectionIds = (await this.collectionModel.find({ purpose: params.purpose }, { _id: 1 }).exec()).map(collId => collId._id);
      }
      else {
        company = await this.companyModel.findOne({ name: params.companyName }).exec();
        collectionIds = (await this.collectionModel.find({ company_id: company._id }, { _id: 1 }).exec()).map(collId => collId._id);
      }
    }
    if (collectionIds === null) {
      return null;
    }
    trapEventIds = (await this.trapEventModel.find({ collection_id: { $in: collectionIds } }, { _id: 1 }).exec()).map(trapEvant => trapEvant._id);
    return trapEventIds;
  }

  async getInsectsPictures(params: any) {
    let fullData = [];
    const insectsQuery = await this.createQueryOnInsects(params);
    let picturesUrl = {};
    const insects = (await this.insectModel.find(insectsQuery, { _id: 1, trapEvent_id: 1, "identifications.kind_id": 1, "identifications.not_valid_id": 1, "cropped_images.timestamp": 1, "cropped_images.position": 1 }).exec());
    const trapEvent = await this.trapEventModel.findById(insects[0].trapEvent_id).exec();
    const collection = await this.collectionModel.findById(trapEvent.collection_id).exec();
    let te, trap, data;
    if (params.hasOwnProperty("trapEvent_id")) {
      te = await this.trapEventModel.find({ _id: params.trapEvent_id }).exec();
    }
    else {
      te = await this.trapEventModel.find({ collection_id: collection._id }).exec();
    }
    let site = await this.siteModel.findOne({ _id: collection.site_id }).exec();
    console.log(te)
    for (let i = 0; i < te.length; i++) {
      trap = await this.trapModel.findOne({ _id: te[i].trap_id }).exec();
      data = { "trapEvent id": te[i]._id, "trap name": trap.name, "pickup date": collection.pickup_date, "placement date": collection.placement_date, "site id": collection.site_id, "site name": site.name, "longitude": site.longitude, "latitude": site.latitude };
      let filteredInsects = insects.filter(insect => insect.trapEvent_id == te[i]._id);
      console.log(filteredInsects.length, insects.length)
      await Promise.all(filteredInsects.map(async insect => {
        let insectId = insect._id;
        let kindId = null;
        let not_valid_id = null;
        if (insect.identifications.length > 0) {
          let lastElement = insect.identifications.length - 1;
          kindId = insect.identifications[lastElement].kind_id;
          not_valid_id = insect.identifications[lastElement].not_valid_id;
        }
        let originImage = await this.originImageModel.findOne({ insects_ids: { $elemMatch: { $eq: insectId } } }).exec();
        if (originImage !== null) {
          let kind, notValid, picture;
          var index = originImage.insects_ids.findIndex(obj => obj == insectId);
          if (kindId !== undefined && kindId !== null) {
            kind = await this.kindModel.find({ _id: kindId }).exec();
            notValid = "False";
          } else {
            kind = [];
            notValid = "True";
          }
          var image_url = originImage.image_URL.toString();
          if (!picturesUrl[image_url]) {
            picturesUrl[image_url] = [];
          }
          if (not_valid_id !== undefined && not_valid_id !== null) {
            notValid = await this.notValidModel.findOne({ _id: not_valid_id }).exec();
          }
          picture = { boundary_boxes: originImage.boundary_boxes[index], kind: kind, notValid: notValid, time_stamp: insect.cropped_images[0].timestamp, position: insect.cropped_images[0].position };
          picturesUrl[image_url].push(picture);
        }
      }));
      var combinedData = { ...data, ...picturesUrl };
      fullData.push(combinedData)
    }
    return fullData;
  }

  async getOriginImages() {
    const originImages = await this.originImageModel.find().exec();
    let originUrls = {};
    await Promise.all(originImages.map(async originImage => {
      if (originImage.insects_ids.length > 0) {
        let insectId = originImage.insects_ids[0];
        if (insectId !== null) {
          let kind_id = await this.insectModel.find({ $and: [{ _id: insectId }, { identifications: { $exists: true } }] }, { _id: 0, "identifications.kind_id": 1 }).exec();
          if (kind_id[0] !== undefined && kind_id[0].identifications.length > 0) {
            let kind = kind_id[0].identifications[0].kind_id;
            if (kind !== undefined && kind !== null) {
              let pictureKind = await this.kindModel.findById({ _id: kind }).exec();
              originUrls[originImage.image_URL.toString()] = pictureKind;
            }
          }
        }
      }
    }));
    return originUrls;
  }

  async getOriginImagesUrls() {
    const originImages = (await this.originImageModel.find({}, { _id: 1, image_URL: 1, insects_ids: 1 }).exec());
    let result: any = [];
    await Promise.all(originImages.map(async image => {
      if (image.insects_ids.length > 0) {
        let insect = await this.insectModel.findOne({ _id: image.insects_ids[0] }).exec();
        let trapEvent = await this.trapEventModel.findOne({ _id: insect.trapEvent_id }).exec();
        let collection = await this.collectionModel.findOne({ _id: trapEvent.collection_id }).exec();
        let site = await this.siteModel.findOne({ _id: collection.site_id }).exec();
        let trap = await this.trapModel.findOne({ _id: trapEvent.trap_id }).exec();
        result.push({ "URL": image.image_URL, "image id": image._id, "time stamp": insect.cropped_images[0].timestamp, "position": insect.cropped_images[0].position, "trap name": trap.name, "pickup date": collection.pickup_date, "placement date": collection.placement_date, "site id": collection.site_id, "site name": site.name, "longitude": site.longitude, "latitude": site.latitude })
      }
    }));
    return result;
  }

}