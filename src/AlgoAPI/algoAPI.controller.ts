import { Body, Controller, Get, Post, Query } from "@nestjs/common";
import { AlgoAPIService } from './algoAPI.service';
import {Request} from '@nestjs/common'
@Controller('algoapi')
export class AlgoAPIController {
  constructor(private readonly algoAPIService: AlgoAPIService) { }

  @Post('getInsectsPictures')
  async getInsectsPictures(
    @Body() body) {
    return this.algoAPIService.getInsectsPictures(body);
  }

  @Get('getOriginImages')
  async getOriginImages(
    @Request() req) {
    return this.algoAPIService.getOriginImages();
  }

  @Get('getOriginImagesUrls')
  async getOriginImagesUrls(
    @Request() req) {
    return this.algoAPIService.getOriginImagesUrls();
  }

}