import { Controller, Body, Post, Param, Put } from '@nestjs/common';
import { UploadScriptAPIService } from './uploadScriptAPI.service';
import { OriginImageDto } from '../dto/originImage.dto';
import { InsectDto } from '../dto/insect.dto';
import { SiteDto } from '../dto/site.dto';
import { CroppedImageDto } from '../dto/croppedImage.dto';
import { IdentificationDto } from '../dto/identification.dto';
import { updateTrapEventDto } from '../dto/trapEvent.dto';
import { CreateIdentificationDto } from 'src/dto/createIdentification.dto';

@Controller('uploadapi')
export class UploadScriptAPIController {
  constructor(private readonly uploadScriptAPIService: UploadScriptAPIService) { }

  @Post('/addOriginImage')
  async createOriginImage(@Body() createOriginImageDto: OriginImageDto) {
    return this.uploadScriptAPIService.createOriginImage(createOriginImageDto);
  }

  @Post('/createInsect')
  async createInsect(
      @Body() createInsectDto: InsectDto) {
      return this.uploadScriptAPIService.createInsect(createInsectDto);
  }

  @Post('/createSite')
  async createSite(
      @Body() createSiteDto: SiteDto) {
      return this.uploadScriptAPIService.createSite(createSiteDto);
  }

  @Post('addInsectImage/:insect_id')
  async createPicture(
      @Param('insect_id') insect_ID: String,
      @Body() createPictureDto: CroppedImageDto) {
      return this.uploadScriptAPIService.createPicture(insect_ID, createPictureDto);
  }

  @Post('/addInsectIdentification/:insect_id')
  async createIdentification(
      @Param('insect_id') insect_ID: String,
      @Body() createIdentificationDto: IdentificationDto) {
      return this.uploadScriptAPIService.createIdentification(insect_ID, createIdentificationDto);
  }

  @Post('/createNotValidIdentification/:insect_id')
  async createNotValidIdentification(
      @Param('insect_id') insect_ID: String,
      @Body() createIdentificationDto: CreateIdentificationDto) {
      return this.uploadScriptAPIService.createNotValidIdentification(insect_ID, createIdentificationDto);
  }

  @Post('/insertToInsectUsage/:insect_id')
  async insertToInsectUsage(
      @Param('insect_id') insect_ID: String) {
      return this.uploadScriptAPIService.insertToInsectUsage(insect_ID);
  }

  // change or add one of the parameters of an existing trap event
  @Put('/updateTrapEvent')
  async updateTrapEvent(
      @Body() updateTrapEvent: updateTrapEventDto) {
      return this.uploadScriptAPIService.updateTrapEvent(updateTrapEvent);
  }

  @Post('/updateKindCounters/:trapevent_id')
  async updateKindCountersForTrapEvent(
      @Param('trapevent_id') trapevent_ID: String) {
      return this.uploadScriptAPIService.updateKindCountersForTrapEvent(trapevent_ID);
  }

  @Post('/updateCollectionState/:trapevent_id')
  async updateCollectionState(
    @Param('trapevent_id') trapevent_ID: String) {
      return this.uploadScriptAPIService.updateCollectionState(trapevent_ID);
  }

  @Post('newCollectionInfo/:trapevent_id')
  async newCollectionInfo(
        @Param('trapevent_id') trapevent_ID: String) {
    return this.uploadScriptAPIService.newCollectionInfo(trapevent_ID);
  }
}