import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UploadScriptAPIController } from './uploadScriptAPI.controller';
import { UploadScriptAPIService } from './uploadScriptAPI.service';
import { Collection, CollectionSchema } from '../Schemas/collection.schema';
import { TrapEvent, TrapEventSchema } from 'src/Schemas/trapEvent.schema';
import { Insect, InsectSchema } from 'src/Schemas/insect.schema';
import { Site, SiteSchema } from 'src/Schemas/site.schema';
import { Kind, KindSchema } from 'src/Schemas/kind.schema';
import { OriginImage, OriginImageSchema } from 'src/Schemas/originImage.schema';
import { InsectUsage, InsectUsageSchema } from 'src/Schemas/insectUsage.schema';
import { NotValid, NotValidSchema } from 'src/Schemas/notValid.schema';
import { Users, UsersSchema } from '../Schemas/users.schema';
import { SocketModule } from '../SocketManager/socketManager.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Collection.name, schema: CollectionSchema },
      { name: TrapEvent.name, schema: TrapEventSchema },
      { name: OriginImage.name, schema: OriginImageSchema },
      { name: Insect.name, schema: InsectSchema },
      { name: Site.name, schema: SiteSchema },
      { name: Kind.name, schema: KindSchema },
      { name: InsectUsage.name, schema: InsectUsageSchema },
      { name: NotValid.name, schema: NotValidSchema },
      { name: Users.name, schema: UsersSchema }
    ]),
    SocketModule
  ],
  controllers: [UploadScriptAPIController],
  providers: [UploadScriptAPIService],
})
export class UploadScriptAPIModule { }
