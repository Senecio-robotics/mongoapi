import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import ShortUniqueId from 'short-unique-id';
import { CroppedImageDto } from 'src/dto/croppedImage.dto';
import { IdentificationDto } from 'src/dto/identification.dto';
import { InsectDto } from 'src/dto/insect.dto';
import { OriginImageDto } from 'src/dto/originImage.dto';
import { trapEventDto, updateTrapEventDto } from 'src/dto/trapEvent.dto';
import { KindCounter } from "../Schemas/kindCounter.schema";
import { CroppedImage } from 'src/Schemas/croppedImage.schema';
import { Identification } from 'src/Schemas/identification.schema';
import { Insect, InsectDocument } from 'src/Schemas/insect.schema';
import { InsectUsage, InsectUsageDocument } from 'src/Schemas/insectUsage.schema';
import { Kind, KindDocument } from 'src/Schemas/kind.schema';
import { OriginImage, OriginImageDocument } from 'src/Schemas/originImage.schema';
import { TrapEvent, TrapEventDocument } from 'src/Schemas/trapEvent.schema';
import { Collection, CollectionDocument } from '../Schemas/collection.schema';
import { CreateIdentificationDto } from 'src/dto/createIdentification.dto';
import { NotValid, NotValidDocument } from 'src/Schemas/notValid.schema';
import { Vial } from 'src/Schemas/vial.schema';
import { SiteDto } from 'src/dto/site.dto';
import { Site, SiteDocument } from 'src/Schemas/site.schema';
import { customAlphabet } from 'nanoid';
import { SocketGateway } from '../SocketManager/socketManager.gateway';
import { Users, UsersDocument } from '../Schemas/users.schema';

@Injectable()
export class UploadScriptAPIService {
  constructor(

    @InjectModel(Collection.name)
    private readonly collectionModel: Model<CollectionDocument>,

    @InjectModel(TrapEvent.name)
    private readonly trapEventModel: Model<TrapEventDocument>,

    @InjectModel(OriginImage.name)
    private readonly originImageModel: Model<OriginImageDocument>,

    @InjectModel(Insect.name)
    private readonly insectModel: Model<InsectDocument>,

    @InjectModel(Site.name)
    private readonly siteModel: Model<SiteDocument>,

    @InjectModel(Kind.name)
    private readonly kindModel: Model<KindDocument>,

    // allows actions on the notValid table
    @InjectModel(NotValid.name)
    private readonly notValidModel: Model<NotValidDocument>,

    // allows actions on the insect usage table
    @InjectModel(InsectUsage.name)
    private readonly insectUsageModel: Model<InsectUsageDocument>,

    // allows actions on the users table
    @InjectModel(Users.name)
    private readonly usersModel: Model<UsersDocument>,
    private readonly socketManager: SocketGateway

  ) { }

  customAlphabetChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
  generateRandomID = customAlphabet(this.customAlphabetChars, 6);

  async createOriginImage(createOriginImageDto: OriginImageDto): Promise<OriginImage> {
    const createdOriginImage = new this.originImageModel(createOriginImageDto);
    createdOriginImage._id = (new ShortUniqueId()).randomUUID();
    await createdOriginImage.save();
    return createdOriginImage._id;
  }

  async createInsect(createInsectDto: InsectDto) {
    const createdInsect = new this.insectModel(createInsectDto);
    createdInsect._id = (new ShortUniqueId()).randomUUID();
    await createdInsect.save();
    return createdInsect._id
  }

  async createSite(createSiteDto: SiteDto) {
    const createdSite = new this.siteModel(createSiteDto);
    let site_id = this.generateRandomID();
    let exist = await this.siteModel.findOne({ _id: site_id }).exec();
    while (exist) {
      site_id = this.generateRandomID();
      exist = await this.siteModel.findOne({ _id: site_id }).exec();
    }
    createdSite._id = site_id;
    await createdSite.save();
    return createdSite._id
  }

  async createPicture(insect_ID: String, createPictureDto: CroppedImageDto) {
    let p: CroppedImage = Object.assign({ _id: (new ShortUniqueId()).randomUUID() }, createPictureDto);
    const firstUnderscoreIndex = createPictureDto.picture_URL.indexOf('_');
    const dateString = createPictureDto.picture_URL.substr(firstUnderscoreIndex + 1).split('.')[0];
    const [date, time] = dateString.split('__');
    const [day, month, year] = date.split('_').map(num => parseInt(num));
    const [hour, minute, second, milsec] = time.split('_').map(num => parseInt(num));
    const dateTime = new Date(year + 2000, month - 1, day, hour, minute, second);
    p.timestamp = dateTime;
    let insect_obj = await this.insectModel.findById(insect_ID).exec();
    insect_obj.cropped_images.push(p);

    // Alert the system that we updated an internal array
    insect_obj.markModified("cropped_images");
    await insect_obj.save();

    return p._id;
  }

  async createNotValidIdentification(insect_ID: String, createIdentificationDto: CreateIdentificationDto) {
    let i: Identification = new Identification();
    console.log(createIdentificationDto);
    i._id = (new ShortUniqueId()).randomUUID();
    i.is_fed = false;
    i.kind_id = null;
    i.tagger_username = "Machine";
    i.timestamp = new Date();
    i.timestamp.setTime(i.timestamp.getTime() - new Date().getTimezoneOffset() * 60 * 1000);
    i.comment = '';
    i.cant_identify = false;
    i.second_opinion = false;
    i.not_valid_id = null;

    // not valid info
    const head = createIdentificationDto.head;
    const thorax = createIdentificationDto.thorax;
    const abdomen = createIdentificationDto.abdomen;
    const noise = createIdentificationDto.noise;
    const multiple = createIdentificationDto.multiple;

    let not_valid: NotValidDocument = await this.notValidModel.findOne({ head: head, thorax: thorax, abdomen: abdomen, noise: noise, multiple: multiple }, { _id: 1 }).exec();
    if (not_valid === null) {
      not_valid = new this.notValidModel({ _id: (new ShortUniqueId()).randomUUID(), head: createIdentificationDto.head, thorax: createIdentificationDto.thorax, abdomen: createIdentificationDto.abdomen, noise: createIdentificationDto.noise, multiple: createIdentificationDto.multiple });
      await not_valid.save();
    }
    i.not_valid_id = not_valid._id;
    let insect_obj = await this.insectModel.findById(insect_ID).exec();
    insect_obj.identifications.push(i);

    // Alert the system that we updated an internal array
    insect_obj.markModified("identifications");
    await insect_obj.save();

    return i._id;
  }

  async createIdentification(insect_ID: String, createIdentificationDto: IdentificationDto) {
    let kind = await this.kindModel.findOne({
      species: createIdentificationDto.species,
      genus: createIdentificationDto.genus,
      sex: createIdentificationDto.sex
    }).exec()

    // if the kind doesn't exist, create it in the DB (Male, Female or Unidentified)
    if (kind === null) {
      let kind_dto = { species: createIdentificationDto.species, genus: createIdentificationDto.genus, sex: createIdentificationDto.sex };
      let kind = new this.kindModel(kind_dto);
      kind._id = (new ShortUniqueId()).randomUUID();
      await kind.save();
    }
    let i: Identification = Object.assign({ _id: (new ShortUniqueId()).randomUUID(), timestamp: new Date(), kind_id: kind._id }, createIdentificationDto);
    let insect_obj = await this.insectModel.findById(insect_ID).exec();
    insect_obj.identifications.push(i);

    // Alert the system that we updated an internal array
    insect_obj.markModified("identifications");
    await insect_obj.save();

    return i._id;
  }

  async updateTrapEvent(updateTrapEvent: updateTrapEventDto) {
    let trapevent_obj = await this.trapEventModel.findById(updateTrapEvent.trapEvent_id).exec();
    let coll_obj = await this.collectionModel.findById(trapevent_obj.collection_id).exec();
    coll_obj.pickup_date = updateTrapEvent.pickup_date;
    coll_obj.collector_username = updateTrapEvent.collector_username;
    const save_col_p = coll_obj.save();
    trapevent_obj.failures = updateTrapEvent.failures;
    for (let i = 0; i < updateTrapEvent.vial_report.length; i++) {
      let genus = updateTrapEvent.vial_report[i]['genus'];
      let species = updateTrapEvent.vial_report[i]['species'];
      let kind_id = (await this.kindModel.findOne({ species: species, genus: genus, sex: 'Female' }).exec())._id;
      let vial: Vial;
      vial = new Vial();
      vial._id = updateTrapEvent.vial_report[i]['barcode'];
      vial.kind_id = kind_id;
      vial.amount = updateTrapEvent.vial_report[i]['amount'];
      vial.company_id = coll_obj.company_id;
      vial.tests = [];
      trapevent_obj.vials.push(vial);
    }
    trapevent_obj.markModified("vials");
    await trapevent_obj.save();
    await save_col_p;
  }

  async insertToInsectUsage(insect_id: String) {
    const insect = await this.insectModel.findById(insect_id).exec();
    const trapEvent = await this.trapEventModel.findById(insect.trapEvent_id).exec();
    let createdInsectUsage;
    createdInsectUsage = new this.insectUsageModel({ collection_id: trapEvent.collection_id, insect_id: insect._id, username: null, timestamp: null, second_opinion: false });
    createdInsectUsage._id = (new ShortUniqueId()).randomUUID();
    await createdInsectUsage.save();
  }

  async updateKindCountersForTrapEvent(trapevent_id: String) {
    const insects: Insect[] = await this.insectModel.find({ trapEvent_id: trapevent_id }).exec();
    let counters: KindCounter[] = [];
    for (let j = 0; j < insects.length; j++) {
      if (!insects[j].cant_identify && insects[j].identifications.length > 0) {
        const kind_id = insects[j].identifications[insects[j].identifications.length - 1].kind_id;
        if (kind_id === null)
          continue;
        // return KindCounter id according to kind_id
        const new_kind_counter_index = counters.findIndex((kC) => kC.kind_id === kind_id);

        // check if the new kindCounter exists 
        if (new_kind_counter_index != -1)
          counters[new_kind_counter_index].amount += 1;
        else
          counters.push({ _id: (new ShortUniqueId()).randomUUID(), kind_id: kind_id, amount: 1 });
      }
    }
    let trapEvent = await this.trapEventModel.findById(trapevent_id).exec();
    trapEvent.kindsCounter = counters;
    trapEvent.markModified("kindsCounter");
    await trapEvent.save();
  }

  async updateCollectionState(trapevent_id: String) {
    const trapEvent = await this.trapEventModel.findById(trapevent_id).exec();
    let collection = await this.collectionModel.findById(trapEvent.collection_id).exec();
    let site = await this.siteModel.findById(collection.site_id).exec();
    site.containCollections = true;
    const trapEventIDs = (await this.trapEventModel.find({ collection_id: collection._id }, { _id: 1 }).exec()).map(te => te._id);
    let identificationExpr: any = [];
    identificationExpr.push({ identifications: { $size: 0 } });
    identificationExpr.push({ $expr: { $eq: [{ $arrayElemAt: ["$identifications.sex", -1] }, "Unidentified"] } });
    identificationExpr.push({ $expr: { $eq: [{ $arrayElemAt: ["$identifications.species", -1] }, "Unidentified"] } });
    identificationExpr.push({ $expr: { $eq: [{ $arrayElemAt: ["$identifications.second_opinion", -1] }, true] } });
    const undefined_insects = await this.insectModel.countDocuments({ trapEvent_id: { $in: trapEventIDs }, $or: identificationExpr }).exec();
    if (undefined_insects > 0) {
      collection.state = 1;
    }
    else {
      collection.state = 2;
    }
    collection.save();
    site.save();
  }

  async newCollectionInfo(trapevent_ID: String) {
    let kinds = {};
    const te = await this.trapEventModel.findById(trapevent_ID).exec();
    const collection = await this.collectionModel.findById(te.collection_id).exec();
    const traps = await this.trapEventModel.find({ collection_id: collection._id }).exec();
    const totalInsects = await this.insectModel.countDocuments({ trapEvent_id: { $in: (traps.map(t => t._id)) } }).exec();
    for (let i = 0; i < traps.length; i++) {
      const kindsCounter = traps[i].kindsCounter;
      for (let j = 0; j < kindsCounter.length; j++) {
        const kind = (await this.kindModel.findById(kindsCounter[j].kind_id).exec());
        const name = kind.genus + " " + kind.species;
        const amount = kindsCounter[j].amount;
        if (kinds[name]) {
          kinds[name] += amount;
        } else {
          kinds[name] = amount;
        }
      }
    }
    console.log('working');
    kinds["company_id"] = collection.company_id;
    kinds["collection_id"] = collection._id;
    kinds["site_id"] = collection.site_id;
    kinds["total_insects"] = totalInsects;
    await this.checkUsersRules(kinds);

  }

  async checkUsersRules(kinds) {
    const users = await this.usersModel.find({ company_id: kinds["company_id"] }).exec();
    for (const user of users) {
      for (const rule of user.rules) {
        const { site_id, amount, kindName } = rule;
        let message;
        if (site_id.length === 0) {
          if (kinds[String(kindName)] && kinds[String(kindName)] > amount) {
            message = "In site " + kinds['site_id'] + " discovered " + kinds[String(kindName)] + " " + kindName;
          }
          else {
            if (kindName === 'All') {
              message = "In site " + kinds["site_id"] + " discovered " + kinds["total_insects"] + " insects";
            }
          }
        }
        else {
          if (site_id.includes(kinds['site_id'])) {
            if (kindName !== 'All') {
              if (kinds[String(kindName)] && kinds[String(kindName)] > amount) {
                message = "In site " + kinds["site_id"] + " discovered " + kinds[String(kindName)] + " " + kindName;
              }
            }
            else {
              message = "In site " + kinds["site_id"] + " discovered " + kinds["total_insects"] + " insects";
            }
          }
        }
        if (message !== undefined) {
          user.messages.push(message);
          user.markModified("messages");
          await user.save();
          let data = {};
          data['company_id'] = kinds['company_id'];
          data['userName'] = user.username;
          data['message'] = message;
          this.socketManager.emitNewMessage(data);
        }
      }
    }
  }
}