import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type SiteDocument = Site & Document;

@Schema()
export class Site {
  @Prop()
  _id: String;

  @Prop()
  name: String;

  @Prop()
  longitude: number;

  @Prop()
  latitude: number;

  @Prop()
  address: String;

  @Prop()
  directions: String;

  @Prop()
  company_id: String;

  @Prop()
  containCollections: Boolean;

  @Prop()
  calServ_id: String;
  
  @Prop()
  mapVision_id: String;
}

export const SiteSchema = SchemaFactory.createForClass(Site);