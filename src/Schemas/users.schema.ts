import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Rule } from './rule.schema';

export type UsersDocument = Users & Document;

@Schema()
export class Users {

  @Prop()
  _id: String;

  @Prop()
  username: String;

  @Prop()
  password: String;

  @Prop()
  collector: Boolean;

  @Prop()
  salt: String;

  @Prop()
  company_id: String;

  @Prop()
  rules: Rule[];

  @Prop()
  messages: String[];
}

export const UsersSchema = SchemaFactory.createForClass(Users);