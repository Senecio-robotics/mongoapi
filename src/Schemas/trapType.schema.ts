import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type TrapTypeDocument = TrapType & Document;

@Schema()
export class TrapType {
  @Prop()
  _id: String;

  @Prop()
  name: String;
}

export const TrapTypeSchema = SchemaFactory.createForClass(TrapType);