import { Prop, Schema } from '@nestjs/mongoose';

@Schema()
export class Tag {
  @Prop()
  _id: String;

  @Prop()
  tag_URL: String;

  @Prop()
  state: String;

  @Prop()
  timestamp: Date;

  @Prop()
  tagger_username: String;
}
