import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type KindDocument = Kind & Document;

@Schema()
export class Kind {
  @Prop()
  _id: String;

  @Prop()
  species: String;

  @Prop()
  genus: String;

  @Prop()
  sex: String;
}

export const KindSchema = SchemaFactory.createForClass(Kind);