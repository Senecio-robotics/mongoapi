import { Prop, Schema } from '@nestjs/mongoose';

@Schema()
export class Project {
  
  @Prop()
  _id: String;

  @Prop()
  name: String;

  @Prop()
  areas: String[];

  @Prop()
  sites_ids: String[];

  @Prop()
  runs_ids: String[];
}
