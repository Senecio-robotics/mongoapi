import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Identification } from './identification.schema';
import { CroppedImage } from './croppedImage.schema';
import { Document } from 'mongoose';

export type InsectDocument = Insect & Document;

@Schema()
export class Insect {
  
  @Prop()
  _id: String;

  @Prop()
  is_fed: Boolean;

  @Prop()
  pool_barcode: String;

  @Prop()
  trapEvent_id: String;

  @Prop()
  verified_timestamp: Date;

  @Prop()
  verifier: String;

  @Prop()
  second_opinion: Boolean;

  @Prop()
  cant_identify: Boolean;

  @Prop()
  cropped_images: CroppedImage[];

  @Prop()
  identifications: Identification[];
}

export const InsectSchema = SchemaFactory.createForClass(Insect);
