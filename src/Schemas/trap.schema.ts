import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type TrapDocument = Trap & Document;

@Schema()
export class Trap {
  @Prop()
  _id: String;

  @Prop()
  name: String;
}

export const TrapSchema = SchemaFactory.createForClass(Trap);