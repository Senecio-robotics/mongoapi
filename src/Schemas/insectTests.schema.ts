import { Prop, Schema } from '@nestjs/mongoose';

@Schema()
export class InsectTests {
  
  @Prop()
  _id: String;
  
  @Prop()
  kind_id: String;

  @Prop()
  viruses_tests: String[];
}