import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type CollectionDocument = Collection & Document;

export enum CollectionState {
  OnField,
  InProgress,
  Completed
}
@Schema()
export class Collection {
  
  @Prop()
  _id: String;

  @Prop()
  company_id: String;

  @Prop()
  state?: CollectionState;

  @Prop()
  collector_username: String;

  @Prop()
  pickup_date: Date;

  @Prop()
  placement_date: Date;

  @Prop()
  purpose: String;
  
  @Prop()
  site_id: String;

  @Prop()
  machine_id: String;

  @Prop()
  camera_type: String;
}

export const CollectionSchema = SchemaFactory.createForClass(Collection);