import { Prop, Schema } from '@nestjs/mongoose';
@Schema()
export class CroppedImage {
  
  @Prop()
  _id: String;

  @Prop()
  picture_URL: String;

  @Prop()
  width_in_mm: Number;

  @Prop()
  length_in_mm: Number;

  @Prop()
  focuses_number: Number;

  @Prop()
  timestamp: Date;

  @Prop()
  position: String;
}
