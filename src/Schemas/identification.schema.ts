import { Prop, Schema } from '@nestjs/mongoose';

@Schema()
export class Identification {
  @Prop()
  _id: String;

  @Prop()
  timestamp: Date;

  @Prop()
  tagger_username: String;

  @Prop()
  kind_id: String;

  @Prop()
  comment: String;

  @Prop()
  second_opinion: Boolean;

  @Prop()
  cant_identify: Boolean;

  @Prop()
  is_fed: Boolean;

  @Prop()
  not_valid_id: String;
}