import { Prop, Schema } from '@nestjs/mongoose';
import { Spreads } from './spreads.schema';


@Schema()
export class Landmarks {

    @Prop()
    _id: String;

    @Prop()
    lat: Number;

    @Prop()
    lng: Number;

    @Prop()
    address: String;

    @Prop()
    clientName: String;

    @Prop()
    email: String;

    @Prop()
    cellPhone: String;

    @Prop()
    rollsNumber: Number;

    @Prop()
    active: Boolean;

    @Prop()
    inside:Boolean;

    @Prop()
    notes: String;

    @Prop()
    startTime: String;

    @Prop()
    endTime: String;

    @Prop()
    spreads: Spreads[];

    @Prop()
    surveyResult: Object[];
}

