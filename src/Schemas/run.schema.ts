import { Prop, Schema } from '@nestjs/mongoose';

@Schema()
export class Run {
  
  @Prop()
  _id: String;

  @Prop()
  name: String;

  @Prop()
  sites_ids: String[];
}
