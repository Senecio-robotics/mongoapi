import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type NotValidDocument = NotValid & Document;

@Schema()
export class NotValid {

  @Prop()
  _id: String;

  @Prop()
  head: Boolean;

  @Prop()
  thorax: Boolean;

  @Prop()
  abdomen: Boolean;

  @Prop()
  noise: Boolean;

  @Prop()
  multiple: Boolean;
}

export const NotValidSchema = SchemaFactory.createForClass(NotValid);
