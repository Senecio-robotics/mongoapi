import { Prop, Schema } from '@nestjs/mongoose';

@Schema()
export class userSpreads {
    @Prop()
    _id: String;

    @Prop()
    userName: String;

    @Prop()
    areaName: String;

    @Prop()
    date: String;

    @Prop()
    spreadsArray: String[];
}

