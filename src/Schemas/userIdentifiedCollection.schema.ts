import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserIdentifiedCollectionDocument = UserIdentifiedCollection & Document;

@Schema()
export class UserIdentifiedCollection {

  @Prop()
  _id: String;
  
  @Prop()
  username: String;

  @Prop()
  collection_id: String;
}

export const UserIdentifiedCollectionSchema = SchemaFactory.createForClass(UserIdentifiedCollection);
