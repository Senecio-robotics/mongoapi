import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Project } from './project.schema';
import { Run } from './run.schema';
import { Document } from 'mongoose';
import { InsectTests } from './insectTests.schema';
import { SpreadingAreas } from './spreadingAreas.schema';

export type CompanyDocument = Company & Document;

@Schema()
export class Company {
  @Prop()
  _id: String;

  @Prop()
  name: String;

  @Prop()
  kinds_ids: String[];

  @Prop()
  longitude: number;

  @Prop()
  latitude: number;

  @Prop()
  lures_ids: String[];

  @Prop()
  traps_ids: String[];

  @Prop()
  types: String[];

  @Prop()
  runs: Run[];

  @Prop()
  insects_tests: InsectTests[];

  @Prop()
  spreading_areas: SpreadingAreas[];
}

export const CompanySchema = SchemaFactory.createForClass(Company);
