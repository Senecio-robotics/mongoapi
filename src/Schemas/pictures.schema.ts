import { Prop, Schema } from '@nestjs/mongoose';

@Schema()
export class Pictures {

    @Prop()
    description: String;

    @Prop()
    url: String;

}