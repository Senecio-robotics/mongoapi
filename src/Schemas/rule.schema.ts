import { Prop, Schema } from '@nestjs/mongoose';

@Schema()
export class Rule {
    @Prop()
    site_id: String[];

    @Prop()
    name: String;

    @Prop()
    amount: Number;

    @Prop()
    kindName: String;
}