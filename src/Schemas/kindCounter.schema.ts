import { Prop, Schema } from '@nestjs/mongoose';

@Schema()
export class KindCounter {
  
  @Prop()
  _id: String;

  @Prop()
  kind_id: String;

  @Prop()
  amount: number;
}