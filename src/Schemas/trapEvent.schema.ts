import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { KindCounter } from './kindCounter.schema';
import { Vial } from './vial.schema';

export type TrapEventDocument = TrapEvent & Document;

@Schema()
export class TrapEvent {
  @Prop()
  _id: String;

  @Prop()
  trap_id: String;

  @Prop()
  trap_type_id: String;

  @Prop()
  lures_ids: String[];

  @Prop()
  collection_id: String;

  @Prop()
  kindsCounter: KindCounter[];

  @Prop()
  vials: Vial[];

  @Prop()
  failures: String[];
}

export const TrapEventSchema = SchemaFactory.createForClass(TrapEvent);