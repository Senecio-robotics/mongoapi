import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type OriginImageDocument = OriginImage & Document;

@Schema()
export class OriginImage {

  @Prop()
  _id: String;

  @Prop()
  image_URL: String;

  @Prop()
  boundary_boxes: Number[][];

  @Prop()
  insects_ids: String[];
}

export const OriginImageSchema = SchemaFactory.createForClass(OriginImage);
