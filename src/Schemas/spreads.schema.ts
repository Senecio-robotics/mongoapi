import { Prop, Schema } from '@nestjs/mongoose';

@Schema()
export class Spreads {
    @Prop()
    date: String;

    @Prop()
    amount: Number;

}