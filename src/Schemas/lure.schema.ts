import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type LureDocument = Lure & Document;

@Schema()
export class Lure {
  @Prop()
  _id: String;

  @Prop()
  name: String;
}

export const LureSchema = SchemaFactory.createForClass(Lure);