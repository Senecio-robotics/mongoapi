import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type FailureDocument = Failure & Document;

@Schema()
export class Failure {
  @Prop()
  _id: String;

  @Prop()
  failure: String;
}

export const FailureSchema = SchemaFactory.createForClass(Failure);
