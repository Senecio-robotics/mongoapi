import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type FailureCompanyDocument = FailureCompany & Document;

@Schema()
export class FailureCompany {
  @Prop()
  _id: String;

  @Prop()
  failure_id: String;

  @Prop()
  company_id: String;
}

export const FailureCompanySchema = SchemaFactory.createForClass(FailureCompany);
