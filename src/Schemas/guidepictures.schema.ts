import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Pictures } from './pictures.schema'

export type GuidePicturesDocument = GuidePictures & Document;

@Schema()
export class GuidePictures {

  @Prop()
  _id: String;

  @Prop()
  insect_name:  String;

  @Prop()
  pictures:  Pictures[];
}

export const GuidePicturesSchema = SchemaFactory.createForClass(GuidePictures);
