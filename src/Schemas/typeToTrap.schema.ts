import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type TypeToTrapDocument = TypeToTrap & Document;

@Schema()
export class TypeToTrap {
  @Prop()
  _id: String;

  @Prop()
  company_id: String;

  @Prop()
  trap_type_id: String;

  @Prop()
  trap_id: String;
}

export const TypeToTrapSchema = SchemaFactory.createForClass(TypeToTrap);