import { Prop, Schema } from '@nestjs/mongoose';
import { Test } from './test.schema';

@Schema()
export class Vial {
  
  @Prop()
  _id: String;

  @Prop()
  kind_id: String;

  @Prop()
  amount: number;

  @Prop()
  company_id: String;

  @Prop()
  tests: Test[];
}