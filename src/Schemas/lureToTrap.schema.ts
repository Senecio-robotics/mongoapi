import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type LureToTrapDocument = LureToTrap & Document;

@Schema()
export class LureToTrap {
  @Prop()
  _id: String;

  @Prop()
  company_id: String;

  @Prop()
  trap_id: String;

  @Prop()
  lure_id: String;
}

export const LureToTrapSchema = SchemaFactory.createForClass(LureToTrap);