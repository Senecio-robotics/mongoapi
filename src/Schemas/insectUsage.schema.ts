import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type InsectUsageDocument = InsectUsage & Document;

@Schema()
export class InsectUsage {
  
  @Prop()
  _id: String;

  @Prop()
  collection_id: String;

  @Prop()
  insect_id: String;

  @Prop()
  username: String;

  @Prop()
  timestamp: Date;

  @Prop()
  second_opinion: Boolean;
}

export const InsectUsageSchema = SchemaFactory.createForClass(InsectUsage);
