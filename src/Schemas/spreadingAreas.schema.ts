import { Prop, Schema } from '@nestjs/mongoose';
import { Landmarks } from './landmarks.schema';
import { userSpreads } from './userSpreads.schema';

@Schema()
export class SpreadingAreas {
    @Prop()
    _id: String;

    @Prop()
    name: String;

    @Prop()
    landMarks: Landmarks[];

    @Prop()
    userSpreads: userSpreads[];
}

