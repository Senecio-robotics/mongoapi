import { Prop, Schema } from '@nestjs/mongoose';

@Schema()
export class Test {
  
  @Prop()
  _id: String;

  @Prop()
  virus_test: String;

  @Prop()
  result: Boolean;
 
}