import { Module } from '@nestjs/common';
import { AuthService } from '../auth/auth.service';
import { SecurityController } from './security.controller';

@Module({
  controllers: [SecurityController],
  providers: [AuthService],
})
export class SecurityModule {}
