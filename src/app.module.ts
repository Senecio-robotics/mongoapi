import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppAPIModule } from './AppAPI/appAPI.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { updateIfCurrentPlugin } from 'mongoose-update-if-current';
import { AutomationAPIModule } from './AutomationAPI/AutomationAPI.module';
import { MachineAPIModule } from './MachineAPI/machineAPI.module';
import { ScheduleModule } from '@nestjs/schedule';
import { UploadScriptAPIModule } from './UploadScriptAPI/uploadScriptAPI.module';
import { AlgoAPIModule } from './AlgoAPI/algoAPI.module';
import { join } from 'path';
import { ServeStaticModule } from '@nestjs/serve-static';
import { SocketModule } from './SocketManager/socketManager.module';

const config = require('config');
const conn_str = config.get('mongoConStr');
const db_name = config.get('db');
@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),  
    MongooseModule.forRoot(
      conn_str[0] + db_name + conn_str[1], {
        connectionFactory: (connection) => {
          connection.plugin(updateIfCurrentPlugin);
          return connection;
        }
      }),
    ScheduleModule.forRoot(),
    AppAPIModule,
    AlgoAPIModule,
    MachineAPIModule,
    AuthModule,
    UsersModule,
    AutomationAPIModule,
    UploadScriptAPIModule,
    SocketModule,
  ],
})
export class AppModule {}