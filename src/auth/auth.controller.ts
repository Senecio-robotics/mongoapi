import { Controller, Get, Request, Post, UseGuards, Body } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { LocalAuthGuard } from '../auth/local-auth.guard';
import { AuthService } from '../auth/auth.service';

@Controller()
export class AuthController {
  constructor(private authService: AuthService) { }

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(
    @Request() req,
    @Body() body) {
    let user = {
      username: body.username,
      company_id: req.user ? req.user.company_id : body.company_id,
      socket_id: body.socket_id
    }
    return this.authService.login(user);
  }

  @UseGuards(JwtAuthGuard)
  @Post('auth/logout')
  async logout(
    @Request() req,
    @Body() body) {
    return this.authService.logout(req.user.company_id, body.socket_id);
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(
    @Request() req) {
    return req.user;
  }

  @UseGuards(JwtAuthGuard)
  @Post('auth/changepass')
  async changePass(
    @Body() body) {
    return this.authService.changePass(body.username, body.old_password, body.new_password);
  }

  @UseGuards(JwtAuthGuard)
  @Post('auth/resetpass')
  async resetPass(
    @Body() body) {
    return this.authService.resetPass(body.username, body.new_password);
  }
}