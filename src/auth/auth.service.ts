import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { compareSync } from 'bcrypt';
import { SocketGateway } from '../SocketManager/socketManager.gateway';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private socketManager: SocketGateway
  ) { }

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne(username);
    if (user && compareSync(pass, user.password)) {
      return { username: user.username, company_id: user.company_id };
    }
    return null;
  }

  async login(user: any) {
    this.socketManager.registerSocket(user.socket_id, user.company_id, user.username);
    const payload = { username: user.username, sub: user.company_id };
    return {
      access_token: this.jwtService.sign(payload),
      company_id: user.company_id
    };
  }

  async logout(company_id, socket_id) {
    this.socketManager.removeSocket(socket_id, company_id);
  }

  async changePass(username, old_password, new_password) {
    return this.usersService.changePass(username, old_password, new_password);
  }

  async resetPass(username, new_password) {
    return this.usersService.resetPass(username, new_password);
  }
}
